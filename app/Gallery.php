<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Gallery extends Model {
	protected $table = 'gallery';
	protected $fillable = ['userid', 'image', 'file_type'];
	public $timestamps = FALSE;

	function getTypeAttribute() {
		return pathinfo($this->filename, PATHINFO_EXTENSION);
	}
}
