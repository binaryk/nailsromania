<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UsersModel extends Model {
	protected $table = 'users_profiles';
	protected $fillable
		= [
			'name',
			'name_slug',
			'category',
			'description',
			'specialities',
			'gender',
			'qtitles',
			'qualifications',
			'photo',
			'phone',
			'fax',
			'email',
			'address',
			'city',
			'website',
			'featured',
			'status',
			'lat',
			'lng',
			'map',
			'video',
			'company',
		];
	static $withoutAppends = FALSE;

	function getQualificationsAttribute($qualifications) {
		if ($qualifications != "") {
			return explode(',', $qualifications);
		}

		return $qualifications;
	}

	function getQtitlesAttribute($qtitles) {
		if ($qtitles != "") {
			return explode(',', $qtitles);
		}

		return $qtitles;
	}

	function getSpecialitiesAttribute($specialities) {
		if (self::$withoutAppends) {
			return $specialities;
		}

		return explode(',', $specialities);
	}
}
