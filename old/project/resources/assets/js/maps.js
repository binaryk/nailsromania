map = null, center = {
    lat: 45.91784833230805,
    lng: 25.78926100000001
},
    function (a) {
        "use strict";
        window.azl = a.extend({}, window.azl), window.azl.init_maps = function () {
            var isDraggable = $(document).width() > 480 ? true : false;
            window.azl = a.extend({}, {
                mapStyles: [{
                    featureType: "administrative",
                    elementType: "labels.text.fill",
                    stylers: [{
                        color: "#333333"
                    }]
                }, {
                    featureType: "landscape",
                    elementType: "all",
                    stylers: [{
                        color: "#f5f5f5"
                    }]
                }, {
                    featureType: "poi",
                    elementType: "all",
                    stylers: [{
                        visibility: "off"
                    }]
                }, {
                    featureType: "road",
                    elementType: "all",
                    stylers: [{
                        saturation: -100
                    }, {
                        lightness: 45
                    }]
                }, {
                    featureType: "road.highway",
                    elementType: "all",
                    stylers: [{
                        visibility: "simplified"
                    }]
                }, {
                    featureType: "road.arterial",
                    elementType: "labels.icon",
                    stylers: [{
                        visibility: "off"
                    }]
                }, {
                    featureType: "transit",
                    elementType: "all",
                    stylers: [{
                        visibility: "off"
                    }]
                }, {
                    featureType: "water",
                    elementType: "all",
                    stylers: [{
                        color: "#ffffff"
                    }, {
                        visibility: "on"
                    }]
                }],
                clusterStyles: [{
                    url: azl.directory + "/img/cluster.png",
                    height: 38,
                    width: 42,
                    textColor: "#ffffff"
                }],
                markerContent: (azl.counties) ? '<div class="map-marker"><div class="icon"><img class="marker" src="' + ("markerImage" in azl ? azl.markerImage : azl.directory + "/img/cluster.png") + '"><div class="cat" style="background-image: url(\'{{marker_image}}\');background-position: 0px 0px; height: 38px; line-height: 38px; width: 42px; text-align: center; top:0;color: rgb(255, 255, 255); position: absolute; font-size: 11px; font-family: Arial,sans-serif; font-weight: bold;">{{count}}</div></div></div>' : '<div class="map-marker"><div class="icon"><img class="marker" src="' + ("markerImage" in azl ? azl.markerImage : azl.directory + "/img/marker.png") + '"><div class="cat" style="background-image: url(\'{{marker_image}}\')"></div></div></div>',
                infoboxOptions: {
                    disableAutoPan: !1,
                    pixelOffset: new google.maps.Size(0, 0),
                    zIndex: null,
                    alignBottom: !0,
                    boxClass: "infobox-wrapper",
                    enableEventPropagation: !0,
                    closeBoxMargin: "0px 0px -8px 0px",
                    closeBoxURL: azl.directory + "/img/close-btn.png",
                    infoBoxClearance: new google.maps.Size(1, 1)
                },
                infoboxTemplate: '<div class="entry azl-location"><div class="entry-thumbnail">{{{image}}}{{{thumbnail}}}</div><div class="entry-data"><div class="entry-header"><div class="entry-extra">{{{extra}}}</div><div class="entry-title"><a href="{{url}}">{{title}}</a></div><div class="entry-meta">{{{meta}}}</div>{{{header}}}</div><div class="entry-content">{{{description}}}</div><div class="entry-footer">{{{footer}}}</div>{{{data}}}</div></div>'
            }, window.azl), Mustache.parse(azl.markerContent), Mustache.parse(azl.infoboxTemplate), a(".azl-map-wrapper").each(function () {
                function b(a) {
                    var b = a.clusters_;
                    return !(1 == b.length && b[0].markers_.length > 1)
                }

                var c = this;
                if (a(c).find(".controls .fullscreen").off("click").on("click", function () {
                        a(c).find(".azl-map").toggleClass("fullscreen"), google.maps.event.trigger(map, "resize"), window.scrollTo(0, 0), a(c).find(".azl-map").is(".fullscreen") ? a("html, body").css("overflow", "hidden") : a("html, body").css("overflow", "auto")
                    }), a(c).find(".controls .locate").off("click").on("click", function () {
                        navigator.geolocation && navigator.geolocation.getCurrentPosition(function (a) {
                            var b = new google.maps.LatLng(parseFloat(a.coords.latitude), parseFloat(a.coords.longitude));
                            map.setCenter(b), map.load_locations()
                        })
                    }), a(c).find(".controls .zoom-in").off("click").on("click", function () {
                        map.setZoom(map.getZoom() + 1)
                    }), a(c).find(".controls .zoom-out").off("click").on("click", function () {
                        map.setZoom(map.getZoom() - 1), map.load_locations()
                    }), void 0 !== a(c).find(".azl-map").data("markers")) {
                    var d = a(c).find(".azl-map").data("markers");
                    if (!azl.counties) {
                        void 0 !== a(c).find(".azl-map").data("markerClusterer") && a(c).find(".azl-map").data("markerClusterer").removeMarkers(d);
                        for (var e = 0; e < d.length; e++) d[e].setMap(null);
                    }
                }
                if (void 0 === a(c).find(".azl-map").data("map") ? map = new google.maps.Map(a(c).find(".azl-map").get(0), {
                        scrollwheel: !1,
                        draggable: isDraggable,
                        disableDefaultUI: !0,
                        styles: azl.mapStyles,
                        mapTypeId: google.maps.MapTypeId.ROADMAP
                    }) : map = a(c).find(".azl-map").data("map"), map.refresh = function () {
                    }, a(c).find(".azl-map").data("map", map), a(c).data("latitude") && a(c).data("longitude")) {
                    var f = new google.maps.LatLng(parseFloat(a(c).data("latitude")), parseFloat(a(c).data("longitude"))),
                        g = document.createElement("DIV");
                    g.innerHTML = Mustache.render(azl.markerContent, {
                        marker_image: a(c).data("marker_image")
                    });
                    var h = new RichMarker({
                        position: f,
                        map: map,
                        content: g,
                        flat: !0
                    });
                    a(c).find(".azl-map").data("markers", [h]), map.refresh = function () {
                        map.setZoom(5), map.setCenter(f), google.maps.event.trigger(map, "resize")
                    }, map.refresh()
                } else if ("locations" in azl) {
                    var i = new google.maps.LatLngBounds,
                        d = [],
                        j = !1,
                        k = !1;
                    for (var l in azl.locations) {
                        var f = new google.maps.LatLng(parseFloat(azl.locations[l].latitude), parseFloat(azl.locations[l].longitude));
                        i.extend(f);
                        var g = document.createElement("DIV");
                        g.innerHTML = Mustache.render(azl.markerContent, azl.locations[l]);
                        var h = new RichMarker({
                            position: f,
                            map: map,
                            content: g,
                            flat: !0
                        });
                        h.post_id = l, d.push(h),
                            function (b) {
                                google.maps.event.addDomListener(b.content, "click", function (c) {
                                    var county = false;
                                    if (azl.counties) {
                                        county = true;
                                    }

                                    function e(b, c) {
                                        function d() {
                                            if (county) {
                                                jQuery.post(azl.ajaxurl, {
                                                    action: "get_location",
                                                    post_id: b.post_id,
                                                    county: true,
                                                    location: window.location.pathname.split('/')
                                                }).done(function (data) {
                                                    data = JSON.parse(data);
                                                    window.location.href = data.url;
                                                    return false;
                                                });

                                            } else {
                                                var a = document.createElement("div");
                                                a.innerHTML = Mustache.render(azl.infoboxTemplate, azl.locations[b.post_id]), azl.infoboxOptions.content = a, b.infobox = new InfoBox(azl.infoboxOptions), google.maps.event.addListener(b.infobox, "closeclick", function () {
                                                    k = 0
                                                })
                                            }
                                        }

                                        function e() {
                                            a(b.infobox.content_).find('.image[src=""]').each(function () {
                                                a(this).attr("src", a(this).data("src")).on("load", function () {
                                                    b.infobox.open(map, b)
                                                })
                                            }), b.infobox.open(map, b)
                                        }

                                        "infobox" in b ? (e(), c()) : !(b.post_id in azl.locations) || Object.keys(azl.locations[b.post_id]).length <= 4 ? a.post(azl.ajaxurl, {
                                            action: "get_location",
                                            post_id: b.post_id
                                        }, function (a) {
                                            a && "" != a && (azl.locations[b.post_id] = JSON.parse(a), d(), e(), c())
                                        }) : (d(), e(), c())
                                    }

                                    if ((j = b) != k) {
                                        for (var f = 0; f < d.length; f++) "infobox" in d[f] && d[f].infobox.close();
                                        e(b, function () {
                                            setTimeout(function () {
                                                0 == b.infobox.pixelOffset_.width && (b.infobox.setOptions({
                                                    pixelOffset: new google.maps.Size(-a(b.infobox.content_).width() / 2, 0)
                                                }), e(b, function () {
                                                }))
                                            }, 0)
                                        })
                                    }
                                    k = b, c.stopPropagation && c.stopPropagation(), c.returnValue = !1
                                })
                            }(h)
                    }
                    a(c).find(".azl-map").data("markers", d), map.refresh = function () {
                        google.maps.event.trigger(map, "resize"), map.fitBounds(i)
                    }, map.refresh(), google.maps.event.addDomListener(window, "resize", function () {
                        map.setCenter(center)
                    });
                    var m = a(window).width();
                    a(window).resize(function () {
                        m != a(window).width() && (m = a(window).width(), map.setCenter(center), google.maps.event.trigger(map, "resize"))
                    }), map.load_locations = function () {
                        a.post(azl.ajaxurl, {
                            action: "locations",
                            southwest_latitude: map.getBounds().getSouthWest().lat(),
                            southwest_longitude: map.getBounds().getSouthWest().lng(),
                            northeast_latitude: map.getBounds().getNorthEast().lat(),
                            northeast_longitude: map.getBounds().getNorthEast().lng(),
                            query: a(c).find(".azl-map").data("query")
                        }, function (b) {
                            if (b && "" != b) {
                                Object.keys(azl.locations).length;
                                azl.locations = a.extend(JSON.parse(b), azl.locations), Object.keys(azl.locations).length
                            }
                        })
                    }, google.maps.event.clearListeners(map, "dragend"), google.maps.event.addListener(map, "dragend", function (a) {
                        map.load_locations()
                    }), google.maps.event.clearListeners(map, "click"), google.maps.event.addListener(map, "click", function (a) {
                        0 != j && setTimeout(function () {
                            "infobox" in j && j.infobox.close(), k = 0
                        }, 0)
                    }), google.maps.event.clearListeners(map, "bounds_changed"), google.maps.event.addListenerOnce(map, "bounds_changed", function (a) {
                        1 == d.length && this.getZoom() && this.setZoom(8)
                    });
                    if (!azl.counties) {
                        var n = new MarkerClusterer(map, d, {
                            styles: azl.clusterStyles,
                            maxZoom: 15
                        });
                        a(c).find(".azl-map").data("markerClusterer", n), n.onClick = function (a, c, d) {
                            return b(c, d)
                        }
                    }
                }
            })
        }, window.azl.list_and_map = function () {
            a("#content > ul.products").length && (a("#colophon").hide(), a("#tertiary > *").hide(), a(function () {
                setTimeout(function () {
                    a("#tertiary .sidebar-inner").trigger("sticky_kit:recalc"), a("#tertiary .sidebar-inner").trigger("sticky_kit:detach")
                }, 0)
            }), setTimeout(function () {
                var b = a(".azl-map-wrapper.all").detach();
                if (a("#tertiary").append(b), a("#main").css("margin", "0"), a("#main").css("top", a("#main").offset().top + "px"), a("#main").addClass("fixed"), a("#main > .container").removeClass("container"), a(b).find(".azl-map").data("map").refresh(), "infinitescroll" in a.fn) {
                    var c = a("#content");
                    a.infinitescroll.prototype._nearbottom = function () {
                        var b = this.options,
                            d = a(c).scrollTop() - a(c).height();
                        return a(c).get(0).scrollHeight == a(c).get(0).clientHeight && (d = 0 + a(document).height() - b.binder.scrollTop() - a(window).height()), this._debug("math:", d, b.pixelsFromNavToBottom), d - b.bufferPx < b.pixelsFromNavToBottom
                    }, a("#content").on("smartscroll.azl", function () {
                        a("#content.infinite-scroll > ul.products").data("infinitescroll") && a("#content.infinite-scroll > ul.products").data("infinitescroll").scroll()
                    }), a("#content").trigger("smartscroll.azl"), "resizable" in a.fn && a("#primary").resizable({
                        handles: "e",
                        resize: function (c, d) {
                            a("#tertiary").width(a("#main").width() - d.size.width + "px"), a("#tertiary").css("left", "auto"), a(b).find(".azl-map").data("map").refresh()
                        }
                    }), a(document).on("triggered.azexo", function () {
                        a(b).find(".azl-map").data("map").refresh()
                    })
                }
            }, 0))
        }, window.azl.validate_form = function (b) {
            var c = !0;
            return b.find(".validation-error").remove(), b.find("select, input, textarea").each(function () {
                var b = this,
                    d = !0;
                if (a(b).hasAttr("data-validation") && (!a(b).hasAttr("data-optional") || "" != a(b).val())) {
                    for (var e = a(b).data("validation").split("|"), f = 0; f < e.length; f++) switch (e[f]) {
                        case "length_conditional":
                            if ("" !== a(b).val()) {
                                var g = parseInt(a(a(b).data("field_number_val")).val());
                                a(b).val().split(/\r*\n/).length != g && (d = !1)
                            }
                            break;
                        case "conditional":
                            "" == a(b).val() && "" == a("#" + a(b).data("conditional-field")).val() && (d = !1);
                            break;
                        case "required":
                            "" == a(b).val() && (d = !1);
                            break;
                        case "int":
                            isNaN(parseInt(a(b).val())) && (d = !1);
                            break;
                        case "float":
                            isNaN(parseFloat(a(b).val())) && (d = !1);
                            break;
                        case "email":
                            /\S+@\S+\.\S+/.test(a(b).val()) || (d = !1);
                            break;
                        case "match":
                            a(b).val() !== a('input[name="' + a(b).data("match") + '"]').val() && (d = !1);
                            break;
                        case "checked":
                            a(b).prop("checked") || (d = !1)
                    }
                    if (!d) {
                        for (a(b).parent().prepend('<div class="validation-error">' + a(b).data("error") + "</div>"); a(b).parent().find(".validation-error ~ .validation-error").length;) a(b).parent().find(".validation-error ~ .validation-error").remove();
                        c = !1
                    }
                }
            }), c
        }, a.QueryString = function (a) {
            if ("" == a) return {};
            for (var b = {}, c = 0; c < a.length; ++c) {
                var d = a[c].split("=");
                2 == d.length && (b[d[0]] = decodeURIComponent(d[1].replace(/\+/g, " ")))
            }
            return b
        }(window.location.search.substr(1).split("&")), a(function () {
            if (a("input[multiple]").each(function () {
                    a(this).attr("name", a(this).attr("name") + "[]")
                }), a.fn.hasAttr = function (a) {
                    return void 0 !== this.attr(a)
                }, a("form.cmb-form").length && "tabs" in a.fn) {
                var b = a("form.cmb-form"),
                    c = !1;
                if (a(b).find(".cmb-field-list > .cmb-type-title").length) {
                    for (c = {}; a(b).find(".cmb-field-list > .cmb-type-title").length;) {
                        var d = a(b).find(".cmb-field-list > .cmb-type-title").get(0),
                            e = /cmb2-id-([\w_-]+)/.exec(a(d).attr("class"))[1],
                            f = a(d).find(".cmb-td > h5").attr("class");
                        c[e] = {
                            title: a(d).find(".cmb-td > h5").text(),
                            desc: a(d).find(".cmb2-metabox-description").text()
                        };
                        var g = [],
                            h = d;
                        do {
                            (h = a(h).next().get(0)) && !a(h).is(".cmb-type-title") && g.push(h)
                        } while (h && !a(h).is(".cmb-type-title"));
                        a(g).wrapAll('<div id="' + e + '" class="tab-content ' + f + '" />'), a(d).remove()
                    }
                    var i = a("<ul></ul>").prependTo(a(b).find(".cmb-field-list"));
                    for (var e in c) a(i).append('<li><a href="#' + e + '">' + c[e].title + "</a><p>" + c[e].desc + "</p></li>")
                }
                for (; a(b).find(".cmb-type-wrapper").length;) {
                    var g = [],
                        j = a(b).find(".cmb-type-wrapper").get(0),
                        h = j;
                    do {
                        (h = a(h).next().get(0)) && !a(h).is(".cmb-type-wrapper") && g.push(h)
                    } while (h && !a(h).is(".cmb-type-wrapper"));
                    a(g).wrapAll('<div class="' + a(j).find(".wrapper").data("class") + '" />'), a(j).remove()
                }
                var k = a(b).find('[name="submit-cmb"]');
                a(k).wrap('<div class="buttons" />');
                var l = "azl-frontend-object-id" == a(b).find('input[name="object_id"]').val();
                if (c) {
                    var m = a('<input type="button" name="prev" value="' + azl_translate.previous + '" class="button prev">').insertBefore(k),
                        n = a('<input type="button" name="next" value="' + azl_translate.next + '" class="button next">').insertBefore(k);
                    a(b).find(".cmb-field-list").tabs({
                        activate: function (c, d) {
                            l && (d.newTab.is(":first-child") ? m.hide() : m.show(), d.newTab.is(":last-child") ? (k.show(), n.hide()) : (n.show(), k.hide())), a(b).find(".pw-map").each(function () {
                                var b = a(this).data("map").getCenter();
                                google.maps.event.trigger(a(this).data("map"), "resize"), a(this).data("map").setCenter(b)
                            })
                        },
                        beforeActivate: function (a, b) {
                            azl.validate_form(b.oldPanel) || a.preventDefault()
                        }
                    }), m.on("click", function () {
                        return a(b).find(".ui-tabs-nav .ui-tabs-active").prev().find("a").click(), a("html, body").animate({
                            scrollTop: a(b).offset().top - 100
                        }, 500), !1
                    }), n.on("click", function () {
                        return a(b).find(".ui-tabs-nav .ui-tabs-active").next().find("a").click(), a("html, body").animate({
                            scrollTop: a(b).offset().top - 100
                        }, 500), !1
                    }), l ? 1 == Object.keys(c).length ? (m.hide(), n.hide()) : (m.hide(), k.hide()) : (m.hide(), n.hide())
                }
                k.on("click", function (c) {
                    b.find("select[required], input[required], textarea[required]").each(function () {
                        a(this).is(":visible") || a(this).removeAttr("required")
                    })
                }), b.on("submit", function (a) {
                    azl.validate_form(b) || a.preventDefault()
                })
            }
            a(".azl-delete a").on("click", function () {
                if (!confirm(azl_translate.delete)) return !1
            }), "google" in window && (azl.init_maps(), a(document).on("infinitescroll", function (b, c) {
                a(c.new_elems).each(function () {
                    var b = a(this).find("script[data-post]").data("post");
                    azl.locations[b] = JSON.parse(a(this).find("script[data-post]").html())
                }), azl.init_maps()
            }))
        })
    }(jQuery);