<?php
$rating = (float) \App\Review::where('userid', $profiledata->id)->avg('rating');
$count = \App\Review::where('userid', $profiledata->id)->count();
$total = '(' . $count . ' Recenzii';
$trophy = '';
if ($count > 10 && $count < 20) {
	$trophy = '<i class="fa fa-trophy" aria-hidden="true">Gold Nail Artist</i>';
} elseif ($count > 20 && $count < 50) {
	$trophy = '<i class="fa fa-trophy" aria-hidden="true" style="color:silver"></i>';
} elseif ($count > 50) {
	$trophy = '<i class="fa fa-trophy" aria-hidden="true" style="color:gold"></i>';
}
?>
<section style="background: url({{url('/')}}/assets/images/{{$settings[0]->background}}) no-repeat center center; background-size: auto; background-attachment:scroll;">
    <div class="row">
        <div style="margin: 4% 0px 4% 0px;">
            <div class="text-center" style="color: #FFF;padding: 20px;">
                <h1>{{$profiledata->name}}</h1>
                <div>{{$profiledata->category}}</div>
                <div class="ratings">
                    <div class="empty-stars"></div>
                    <div class="full-stars" style="width:{{number_format($rating, 1, '.', '')*20}}%"></div>
                </div>
                <span class="rating-number">({{number_format($rating, 1, '.', '')}}/5)</span>
                <div class="rating-number">{{$total}} <?php echo $trophy; ?></div>

            </div>
        </div>
    </div>
</section>