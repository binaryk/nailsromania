@extends('includes.master')

@section('content')
    <div class="container">
        <section class="azl-map-wrapper all">
			<?php if(count($mapMarkers) < 1) { ?>
            <div id="map-results-overlay">
                No matching results found. Please modify your search criteria and try searching again.
            </div>
			<?php } ?>
            <input id="azl-map" type="checkbox" style="position: absolute; clip: rect(0, 0, 0, 0);">
            <div class="controls">
                <div class="toggle">
                    <label for="azl-map"></label>
                </div>
                <div class="locate"></div>
                <div class="zoom-in"></div>
                <div class="zoom-out"></div>
            </div>
            <div class="azl-map" data-query='{"post_type":"product", "nopaging":true, "posts_per_page":-1}' style="position: relative; overflow: hidden;"></div>

            <div class="row" style="background-color:rgba(0,0,0,0.7);">
                <form action="{{action('FrontEndController@search')}}" method="post">
                    {{csrf_field()}}
                    <div class="form-group">
                        <div class="col-md-3 opt-form" style="padding-left: 15px;">
                            <input id="color" name="city" class="form-control" placeholder="Alege un oras" list="cities" autocomplete="off">
                            <datalist id="cities">
                                @foreach($cities as $city)
                                    <option value="{{$city->city}}">
                                @endforeach
                            </datalist>
                        </div>
                        <div class="col-md-5 opt-form">
                            <input type="text" placeholder="Alege o categorie" list="categories" name="category" class="form-control" autocomplete="off">
                            <datalist id="categories">
                                @foreach($categories as $category)
                                    <option value="{{$category->name}}">
                                @endforeach
                            </datalist>
                        </div>
                        <div class="col-md-2 opt-form">
                            <label>
                                <input type="checkbox" name="starred" class="form-control" <?php echo (isset($_REQUEST['starred']) && $_REQUEST['starred'] == 'on') ? 'checked="checked"' : ''; ?> />
                                <div>Doar 5 stele</div>
                            </label>
                        </div>
                        <div class="col-md-2 opt-form">
                            <button class="btn btn-ocean btn-block" type="submit">Cauta</button>
                        </div>
                    </div>
                </form>
            </div>
        </section>
    </div>
	<?php
	if(count($mapMarkers) > 0) {
	foreach ($mapMarkers as $marker) {
		$json[ $marker->id ]['latitude']  = (float) $marker->latitude;
		$json[ $marker->id ]['longitude'] = (float) $marker->longitude;
		$json[ $marker->id ]['featured']  = (int) $marker->map;
		$json[ $marker->id ]['company']   = (int) $marker->company;
	}
	?>
    <script type="text/javascript">azl.locations = <?php echo json_encode($json); ?>;</script>
	<?php } ?>

    <div id="wrapper" class="go-section">
        <div class="row">
            <div class="container">
                <h2 class="text-center">{{$pagename}}</h2>
                <hr>
                <div class="gocover"></div>
                <div id="alldocs">
                    @foreach($allusers as $alluser)
                        <div class="col-md-3 col-xs-12 col-sm-6 single">
                            <div class="group<?php echo ($alluser['featured'] == 1) ? " featured" : ""; ?>">
                                <a href="{{url('/')}}/profile/{{$alluser->id}}/{{$alluser->name_slug}}">
                                    @if($alluser->photo != "")
                                        <img src="{{imageUrl(url('/').'/assets/images/profile_photo/' . $alluser->photo, 250, 250)}}" class="profile-image" alt="{{$alluser->name}}">
                                    @else
                                        <img src="{{url('/')}}/assets/images/profile_photo/avatar.jpg" style="max-width: 100%;" class="profile-image" alt="{{$alluser->name}}">
                                    @endif
                                    @if($alluser->company == 1)
                                        <span class="company">COMPANIE</span>
                                    @endif
                                    @if($alluser->featured ==1)
                                        <span class="premium">PREMIUM</span>
                                    @endif
                                    <div class="text-center listing">
                                        <h3 class="no-margin go-bold">{{$alluser->name}}</h3>
                                        <p class="no-margin">{{$alluser->category}} - {{$alluser->city}}</p>
                                    </div>
                                </a>
                            </div>
                        </div>
                    @endforeach
                    <div class='col-md-12 margintop'></div>
                </div>

            </div>
        </div>
    </div>

@stop

@section('footer')

@stop