<!DOCTYPE html>
<!--[if lt IE 7]>
<html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>
<html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>
<html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if IE 9]>
<html class="no-js ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js" lang=""> <!--<![endif]-->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">

    <meta name="description" content="">
    <meta name="author" content="">

    <title>Nails Romania - Bine ai venit</title>

    <link href="//fonts.googleapis.com/css?family=Quicksand:300,400,500,700&amp;subset=latin-ext" rel="stylesheet">
    <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" type="text/css">
    <link rel="icon" type="image/png" href="{{url('/')}}/assets/images/{{$settings[0]->favicon}}"/>
    <link rel="stylesheet" href="{{ URL::asset('assets/css/bootstrap.min.css')}}" type="text/css">
    <link rel="stylesheet" href="{{ URL::asset('assets/css/material.min.css')}}" type="text/css">
    <link rel="stylesheet" href="{{ URL::asset('assets/css/welcome.css')}}" type="text/css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="//oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="//oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    <script>if (/Android|webOS|iPhone|iPad|iPod|BlackBerry/i.test(navigator.userAgent)) {
            window.location = "/mobile";
        }</script>
</head>
<body>
<!-- Top Navigation Bar Start -->
<nav id="topNav2" class="navbar">
    <div class="container">
        <!-- Logo Start -->
        <a class="navbar-brand" href="{{ url('/') }}">
            <img src="{{ URL::asset('assets/images/logo')}}/{{$settings[0]->logo}}" alt="">
        </a>
        <!-- Logo End -->
    </div>
</nav>
<!-- Top Navigation Bar End -->

<div id="banner" data-img-src="{{ URL::asset('http://makeupromania.ro/wallpaper_intro.jpg')}}">
    <div class="vc-parent">
        <div class="vc-child">
            <div class="banner-content">
                <div class="container">
                    <div class="row" id="textSlider">
                        <div class="col-md-12">
                            <div class="slider-2-content text-center">
                                <div class="item-1">
                                    <h2 class="mdl-typography--display-2 mdl-typography--text-capitalize">
                                        PRIMUL SITE DIN ROMANIA
                                    </h2>
                                    <h4 class="mdl-typography--text-capitalize">
                                        Special pentru Tine, Nail Artist-ul creativ
                                    </h4>
                                </div>
                                <div class="item-2">
                                    <h2 class="mdl-typography--display-2 mdl-typography--text-capitalize">
                                        Clientii Tai au nevoie de Tine !
                                    </h2>
                                    <h4 class="mdl-typography--text-capitalize">
                                        Acum sunteti exclusiv cu totii intr-o singura platforma simpla
                                    </h4>
                                </div>
                                <div class="item-3">
                                    <h2 class="mdl-typography--display-2 mdl-typography--text-capitalize">
                                        1 Euro
                                    </h2>
                                    <h4 class="mdl-typography--text-capitalize">
                                        Pentru 1 Euro, beneficiezi de toate capabilitatile platformei si vei fi <b>promovat</b> permanent
                                    </h4>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row" id="buttons">
                        <div class="col-md-12 text-center">
                            <a href="/user/login" class="btn btn-block mdl-button mdl-button--raised mdl-button--accent">
                                <i class="fa fa-user"></i> Logheaza-te
                            </a>
                            <a href="/user/registration" class="btn btn-block mdl-button mdl-button--raised mdl-button--accent btn-register">
                                <i class="fa fa-user-plus"></i> Inregistreaza-te
                            </a>
                            <a href="/user/forgot" class="btn mdl-button mdl-button--raised mdl-button--accent btn-forgot">
                                <i class="fa fa-lock"></i> Ai uitat parola ?
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div id="footer">

</div>

<script type='text/javascript' src="{{ URL::asset('assets/js/jquery.js')}}"></script>
<script type='text/javascript' src="{{ URL::asset('assets/js/bootstrap.min.js')}}"></script>
<script type='text/javascript' src="{{ URL::asset('assets/js/material.min.js')}}"></script>
</body>
</html>