<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, shrink-to-fit=no, initial-scale=1">
    <meta name="keywords" content="{{$code[0]->meta_keys}}">
    <meta name="author" content="">
    <meta name="_token" content="{!! csrf_token() !!}"/>
    <link rel="icon" type="image/png" href="{{url('/')}}/assets/images/{{$settings[0]->favicon}}"/>
    <title>{{$settings[0]->title}}</title>
    <!-- Bootstrap Core CSS -->
    <link rel="stylesheet" href="{{ URL::asset('assets/css/bootstrap.min.css')}}" type="text/css">
    <link rel="stylesheet" href="{{ URL::asset('assets/css/bootstrap-datepicker.css')}}" type="text/css">
    <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" type="text/css">
    <link href="//fonts.googleapis.com/css?family=Quicksand:300,400,500,700&amp;subset=latin-ext" rel="stylesheet">
    <!-- Custom CSS -->
    <link rel="stylesheet" href="{{ URL::asset('assets/css/genius1.css')}}" type="text/css">
    <link rel="stylesheet" href="{{ URL::asset('assets/css/maps.css')}}" type="text/css">
    <link rel="stylesheet" href="//vjs.zencdn.net/6.2.8/video-js.css">
    <meta property="fb:app_id" content="{{FB_APPID}}"/>
    <meta property="og:url" content="{{url()->current()}}"/>
    <meta property="og:description" content="Viziteaza profilul meu pe www.makeupromania.ro"/>
    @if(request()->route()->getName() === 'user.profile')
        <meta property="og:type" content="profile"/>
        <meta property="og:title" content="Nails Romania: {{$profiledata->name}}"/>
        @if($profiledata->photo != "")
            <meta property="og:image" content="{{url('/')}}/assets/images/profile_photo/{{$profiledata->photo}}">
        @else
            <meta property="og:image" content="{{url('/')}}/assets/images/profile_photo/avatar.jpg"/>
        @endif
        <meta property="og:image:height" content="300"/>
        <meta property="og:image:width" content="600"/>
@endif
    <!--[if lt IE 9]>
    <script src="//oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="//oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    <!--[if lt IE 8]>
    <link rel='stylesheet' href='{{ URL::asset(' assets/css/bootstrap-ie7.css')}}' type='text/css'>
    <![endif]-->
    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/fancybox/3.1.25/jquery.fancybox.min.css"/>
    <!-- jQuery -->
    <script src="{{ URL::asset('assets/js/jquery.js')}}"></script>
    <script type="text/javascript">
        window.azl = {};
        azl.ajaxurl = "/map_query";
        azl.directory = "/assets/images";
        azl.mapStyles = [{
            "featureType": "administrative",
            "elementType": "labels.text.fill",
            "stylers": [{
                "color": "#444444"
            }]
        }, {
            "featureType": "landscape",
            "elementType": "all",
            "stylers": [{
                "color": "#f2f2f2"
            }]
        }, {
            "featureType": "poi",
            "elementType": "all",
            "stylers": [{
                "visibility": "off"
            }]
        }, {
            "featureType": "road",
            "elementType": "all",
            "stylers": [{
                "saturation": -100
            }, {
                "lightness": 45
            }]
        }, {
            "featureType": "road.highway",
            "elementType": "all",
            "stylers": [{
                "visibility": "simplified"
            }]
        }, {
            "featureType": "road.arterial",
            "elementType": "labels.icon",
            "stylers": [{
                "visibility": "off"
            }]
        }, {
            "featureType": "transit",
            "elementType": "all",
            "stylers": [{
                "visibility": "off"
            }]
        }, {
            "featureType": "water",
            "elementType": "all",
            "stylers": [{
                "color": "#771186"
            }, {
                "visibility": "on"
            }]
        }, {
            "featureType": "water",
            "elementType": "geometry.fill",
            "stylers": [{
                "color": "#771186"
            }]
        }];
        azl.markerImage = azl.directory + "/dailybooth-logo.png";
        azl.markerFeatured = azl.directory + "/marker_featured.png";
        azl.markerCompany = azl.directory + "/marker_company.png";
        azl.clusterStyles = [{
            height: 38,
            width: 42,
            textColor: "#ffffff"
        }];
        azl.clusterStyles[0].url = azl.directory + "/cluster.png";
    </script>
    <script src="//vjs.zencdn.net/ie8/1.1.2/videojs-ie8.min.js"></script>
</head>
<body class="<?php echo (isset($style)) ? $style : '' ?>">
<div id="fb-root"></div>
<script>(function (d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return;
        js = d.createElement(s);
        js.id = id;
        js.src = '//connect.facebook.net/ro_RO/sdk.js#xfbml=1&version=v2.11&appId={{FB_APPID}}';
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));
    <!-- Facebook Pixel Code -->
    !function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
        n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
        n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
        t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
        document,'script','https://connect.facebook.net/en_US/fbevents.js');
    fbq('init', '105207486932553'); // Insert your pixel ID here.
    fbq('track', 'PageView');
</script>
<noscript><img height="1" width="1" style="display:none" src="//www.facebook.com/tr?id=105207486932553&ev=PageView&noscript=1" /></noscript>
<!-- DO NOT MODIFY -->
<!-- End Facebook Pixel Code -->
<div id="cover"></div>
<div class="theme2">
	<?php
	if ( ! $mobile) {
		$class = 'container';
	} else {
		$class = 'row';
	}
	?>
    <div class="{{$class}}">
        @if(!$mobile)
            <div class="row">
                <div class="pull-left gologo">
                    <a href="{{ url('/') }}">
                        <img class="img-responsive" src="{{ URL::asset('assets/images/logo')}}/{{$settings[0]->logo}}" alt="">
                    </a>
                </div>
                @include('includes.header-ad')
            </div>
        @endif

        <nav class="navbar navbar-bootsnipp " role="navigation" id="nav_bar">
            <div class="container">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    @if($mobile)
                        <div class="animbrand">
                            <a class="navbar-brand" href="{{url('/')}}">Nails Romania</a>
                        </div>
                    @endif
                </div>

                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                    <ul class="nav navbar-nav navbar-right">
                        @if(!$mobile)
                            <li class="pull-left">
                                <div class="socicon text-center">
                                    @if($sociallinks[0]->f_status == "enable")
                                        <a href="{{$sociallinks[0]->facebook}}" class="facebook"><i class="fa fa-facebook"></i></a>
                                    @endif
                                </div>
                            </li>
                        @endif
                        <li><a href="{{url('/')}}" class="">ACASA</a></li>
                        <li><a href="{{url('/listfeatured')}}" class="">Artisti Recomandati de Website</a></li>
                        <li><a href="{{url('/liststarred')}}" class="">Artisti recomandati de Utilizatori</a></li>
                        <li><a href="{{url('/listall')}}" class="">Toti Artistii</a></li>


                        @if($pagesettings[0]->a_status == 1)
                            <li><a href="{{url('/about')}}" class="">Despre Noi</a></li>
                        @endif
                        @if(Auth::guard('profile')->guest())
                            <li><a href="{{url('/user/login')}}" class="">Login</a></li>
                            <li><a href="{{url('/user/registration')}}" class="">Inregistrare</a></li>
                        @else
                            <li><a href="{{url('/user/dashboard')}}" class="">Contul meu</a></li>
                        @endif
                    </ul>
                </div>
            </div>
        </nav>
    </div>

    @yield('content')
    @include('includes.footer')
</div>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-110190615-1"></script>
<script>
    window.dataLayer = window.dataLayer || [];

    function gtag() {
        dataLayer.push(arguments);
    }

    gtag('js', new Date());
    gtag('config', 'UA-110190615-1');
</script>
<script type='text/javascript' src="{{URL::asset('assets/js/jquery.smooth-scroll.js')}}"></script>
<!-- Bootstrap Core JavaScript -->
<script type='text/javascript' src="{{URL::asset('assets/js/bootstrap.min.js')}}"></script>
<script type='text/javascript' src="{{URL::asset('assets/js/genius.js')}}"></script>
<script type="text/javascript" src="//maps.google.com/maps/api/js?libraries=places&key={{GMAPS_KEY}}"></script>
<script type='text/javascript' src="{{URL::asset('assets/js/maps.min.js')}}"></script>
<script type='text/javascript' src="{{URL::asset('assets/js/infobox.js')}}"></script>
<script type='text/javascript' src="{{URL::asset('assets/js/markerclusterer.js')}}"></script>
<script type='text/javascript' src="{{URL::asset('assets/js/richmarker.js')}}"></script>
<script type='text/javascript' src="{{URL::asset('assets/js/mustache.js')}}"></script>
<script type='text/javascript' src="{{URL::asset('assets/js/moment.min.js')}}"></script>
<script type='text/javascript' src="{{URL::asset('assets/js/transition.js')}}"></script>
<script type='text/javascript' src="{{URL::asset('assets/js/collapse.js')}}"></script>
<script type='text/javascript' src="{{URL::asset('assets/js/bootstrap-datepicker.js')}}"></script>
<script type='text/javascript' src="//cdnjs.cloudflare.com/ajax/libs/fancybox/3.1.25/jquery.fancybox.min.js"></script>
<script type='text/javascript' src="{{URL::asset('assets/js/video.js')}}"></script>
<script type="text/javascript">
    window.HELP_IMPROVE_VIDEOJS = false;
    $(window).load(function () {
        setTimeout(function () {
            $('#cover').fadeOut(300);
        }, 300)
    });
</script>

{!! $code[0]->google_analytics !!}

@yield('footer')
<script type="text/javascript">$.ajaxSetup({headers: {'X-CSRF-Token': $('meta[name=_token]').attr('content')}});$.ajaxSetup({type:'POST',headers:{"cache-control":"no-cache"}});</script>
<script>
    function gtag_report_conversion(url) {
        var callback = function () {
            if (typeof(url) != 'undefined') {
                window.location = url;
            }
        };
        gtag('event', 'conversion', {
            'send_to': 'AW-862644121/lKGWCJDt4HkQmc-rmwM',
            'event_callback': callback
        });
        return false;
    }
</script>
</body>
</html>