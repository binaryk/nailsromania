<div class="pull-right headad">
    <div class="desktop-advert">
        @if(!empty($ads728x90))
            @if($ads728x90->type == "banner")
                <a class="ads" href="{{$ads728x90->redirect_url}}" target="_blank">
                    <img class="banner-728x90" src="{{url('/')}}/assets/images/ads/{{$ads728x90->banner_file}}" alt="Advertisement">
                </a>
            @else
                {!! $ads728x90->script !!}
            @endif
        @endif
    </div>
</div>