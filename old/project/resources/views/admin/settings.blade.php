@extends('admin.includes.master-admin')

@section('content')

    <div id="page-wrapper">
        <div class="container-fluid">
            <div class="row" id="main">

                <!-- Page Heading -->
                <div class="go-title">
                    <h3>General Settings</h3>
                    <div class="go-line"></div>
                </div>
                <!-- Page Content -->
                <div class="panel panel-default">
                    <div class="panel-body">
                        <div id="res">
                            @if(Session::has('message'))
                                <div class="alert alert-success alert-dismissable">
                                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                    {{ Session::get('message') }}
                                </div>
                            @endif
                        </div>
                        <!-- /.start -->
                        <div class="col-md-12">
                            <ul class="nav nav-tabs tabs-left">
                                <li class="active"><a href="#logo" data-toggle="tab" aria-expanded="true">Logo</a></li>
                                <li class=""><a href="#favicon" data-toggle="tab" aria-expanded="true">Favicon</a></li>
                                <li class=""><a href="#website" data-toggle="tab" aria-expanded="false">Website Contents</a></li>
                                <li class=""><a href="#payment" data-toggle="tab" aria-expanded="false">Payment Informations</a></li>
                                <li class=""><a href="#background" data-toggle="tab" aria-expanded="false">Background Image</a></li>
                                <li class=""><a href="#about" data-toggle="tab" aria-expanded="false">Despre Noi</a></li>
                                <li class=""><a href="#address" data-toggle="tab" aria-expanded="false">Office Adresa</a></li>
                                <li class=""><a href="#footer" data-toggle="tab" aria-expanded="false">Footer</a></li>
                                <li class=""><a href="#config" data-toggle="tab" aria-expanded="false">Configuration</a></li>
                            </ul>
                        </div>

                        <div class="col-xs-9">
                            <!-- Tab panes -->
                            <div class="tab-content">
                                @include('admin.parts.logo')
                                @include('admin.parts.favicon')
                                @include('admin.parts.website')
                                @include('admin.parts.payment')
                                @include('admin.parts.background')
                                @include('admin.parts.about')
                                @include('admin.parts.address')
                                @include('admin.parts.footer')
                                @include('admin.parts.config')
                            </div>
                        </div>
                    </div>
                    <!-- /.end -->
                </div>
            </div>
        </div>
        <!-- /.row -->
    </div>
    <!-- /.container-fluid -->
    </div>
    <!-- /#page-wrapper -->

@stop

@section('footer')
    <script type="text/javascript">
        bkLib.onDomLoaded(function () {
            new nicEditor().panelInstance('aboutpnael');
            new nicEditor().panelInstance('footerpnael');
        });
    </script>
@stop