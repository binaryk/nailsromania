<div class="tab-pane active" id="logo">
    <p class="lead">Website Logo</p>
    <div class="ln_solid"></div>
    <form method="POST" action="settings/logo" class="form-horizontal form-label-left" enctype="multipart/form-data">
        {{csrf_field()}}
        <div class="item form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Current Logo
            </label>
            <div class="col-md-6 col-sm-6 col-xs-12">
                <img class="col-md-6" src="../assets/images/logo/{{$setting[0]->logo}}">
            </div>

        </div>
        <br>

        <div class="item form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Setup New Logo <span class="required">*</span>
            </label>
            <div class="col-md-6 col-sm-6 col-xs-12">
                <input type="file" name="logo" required/>
            </div>

        </div>

        <div class="ln_solid"></div>
        <div class="form-group">
            <div class="col-md-6 col-md-offset-3">
                <!--  <button type="submit" class="btn btn-primary">Cancel</button> -->
                <button type="submit" class="btn btn-success btn-block">Update Settings</button>
            </div>
        </div>
    </form>
</div>