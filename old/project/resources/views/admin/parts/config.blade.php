<div class="tab-pane" id="config">
    <p class="lead">Various website configurations</p>
    <div class="ln_solid"></div>
    <form method="POST" action="settings/config" class="form-horizontal form-label-left" id="about_form">
        {{csrf_field()}}
        <div class="item form-group">
            <label class="control-label col-md-4 col-sm-4 col-xs-12" for="title"> Default Profile Images <span class="required">*</span>
            </label>
            <div class="col-md-6 col-sm-6 col-xs-12">
                <input class="form-control col-md-7 col-xs-12" name="profile_images" placeholder="Default Profile Images" required="required" type="text" value="{{$setting[0]->profile_images}}">
            </div>
        </div>

        <div class="ln_solid"></div>
        <div class="form-group">
            <div class="col-md-6 col-md-offset-3">
                <!--  <button type="submit" class="btn btn-primary">Cancel</button> -->
                <button type="submit" id="about_update" class="btn btn-success btn-block">Update Settings</button>
            </div>
        </div>
    </form>
</div>