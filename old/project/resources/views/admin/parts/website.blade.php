<div class="tab-pane" id="website">
    <p class="lead">Website Contents</p>

    <div class="ln_solid"></div>
    <form method="POST" action="settings/title" class="form-horizontal form-label-left" id="website_form">
        {{csrf_field()}}
        <div class="item form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="title"> Website Title <span class="required">*</span>
            </label>
            <div class="col-md-6 col-sm-6 col-xs-12">
                <input class="form-control col-md-7 col-xs-12" data-validate-length-range="6" name="title" placeholder="Website Title" required="required" type="text" value="{{$setting[0]->title}}">
            </div>
        </div>

        <div class="ln_solid"></div>
        <div class="form-group">
            <div class="col-md-6 col-md-offset-3">
                <!--  <button type="submit" class="btn btn-primary">Cancel</button> -->
                <button type="submit" id="website_update" class="btn btn-success btn-block">Update Settings</button>
            </div>
        </div>
    </form>
</div>