<div class="tab-pane" id="favicon">
    <p class="lead">Website Favicon</p>
    <div class="ln_solid"></div>
    <form method="POST" action="settings/favicon" class="form-horizontal form-label-left" enctype="multipart/form-data">
        {{csrf_field()}}
        <div class="item form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Current Favicon
            </label>
            <div class="col-md-6 col-sm-6 col-xs-12">
                <img class="col-md-3" src="../assets/images/{{$setting[0]->favicon}}">
            </div>

        </div>
        <br>
        <!-- <input type="hidden" name="id" value="1"> -->
        <div class="item form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Setup New Favicon <span class="required">*</span>
            </label>
            <div class="col-md-6 col-sm-6 col-xs-12">
                <input type="file" name="favicon" required/>
            </div>
        </div>

        <div class="ln_solid"></div>
        <div class="form-group">
            <div class="col-md-6 col-md-offset-3">
                <!--  <button type="submit" class="btn btn-primary">Cancel</button> -->
                <button type="submit" class="btn btn-success btn-block">Update Settings</button>
            </div>
        </div>
    </form>
</div>