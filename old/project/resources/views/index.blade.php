@extends('includes.master')

@section('content')
	<?php
	if ( ! $mobile) {
		$class = 'container';
	} else {
		$class = 'row';
	}
	?>
    <div class="{{$class}}">
        <section class="azl-map-wrapper all">
            @if(!$mobile)
                <input id="azl-map" type="checkbox" style="position: absolute; clip: rect(0, 0, 0, 0);">
                <div class="controls">
                    <div class="toggle">
                        <label for="azl-map"></label>
                    </div>
                    <div class="locate"></div>
                    <div class="zoom-in"></div>
                    <div class="zoom-out"></div>
                </div>
            @endif
            <div class="azl-map" data-query='{"post_type":"product", "nopaging":true, "posts_per_page":-1}' style="position: relative; overflow: hidden;"></div>

            <div class="row" style="background-image: url('http://makeupromania.ro/assets/images/Fotolia_111751750_Subscription_Monthly_XXL.jpg');">
                <form action="{{action('FrontEndController@search')}}" method="post">
                    {{csrf_field()}}
                    <div class="form-group" style="padding-left: 15px;">
                        <div class="col-md-4 opt-form">
                            <input id="color" name="city" class="form-control" placeholder="Alege un oras" list="cities" autocomplete="off">
                            <datalist id="cities">
                                @foreach($cities as $city)
                                    <option value="{{$city->city}}">
                                @endforeach
                            </datalist>
                        </div>
                        <div class="col-md-6 opt-form">
                            <input type="text" placeholder="Alege o categorie" list="categories" name="category" class="datalist form-control" autocomplete="off">
                            <datalist id="categories">
                                @foreach($categories as $category)
                                    <option value="{{$category->name}}">
                                @endforeach
                            </datalist>
                        </div>
                        @if(!$mobile)
                            <div class="col-md-2 opt-form">
                                <label>
                                    <input type="checkbox" name="starred" class="form-control">
                                    <div>Doar 5 stele</div>
                                </label>
                            </div>
                        @endif
                        <div class="submit" style="padding-right:15px;">
                            <input class="btn btn-ocean btn-block" type="submit" value="Cauta"/>
                        </div>
                    </div>
                </form>
            </div>
        </section>
    </div>

	<?php
	if(count($mapMarkers) > 0) {
	foreach ($mapMarkers as $marker) {
		$json[ $marker->id ]['latitude']  = (float) $marker->latitude;
		$json[ $marker->id ]['longitude'] = (float) $marker->longitude;
		$json[ $marker->id ]['featured']  = (int) $marker->map;
		$json[ $marker->id ]['company']   = (int) $marker->company;
	}
	?>
    <script type="text/javascript">azl.locations = <?php echo json_encode($json); ?></script>
	<?php } ?>

    <section class="go-section" style="padding-bottom:0;">
        <div class="row">
            <div class="container">
                <h2 class="text-center">Toate categoriile</h2>
                <hr>
                @foreach($categories as $category)
                    <div class="col-md-4 cats" id="{{$category->slug}}">
                        <a href="category/{{$category->slug}}" class="btn btn-genius btn-block">
                            <strong> {{$category->name}}</strong>
                        </a>
                    </div>
                @endforeach
            </div>
            @if ( ! $mobile) {
            <div class="container">
                <img class="img-responsive" src="{{url('/')}}/assets/images/banner_categorii.jpeg" style="margin:0 auto;"/>
            </div>
            @endif
        </div>
    </section>

    <div id="wrapper" class="go-section" style="padding-top:0;">
        <div class="container">
            <h2 class="text-center">Nail Artisti PREMIUM</h2>
            <hr>
            @foreach($featured as $feature)
				<?php
				if ($feature->id == $lastPayer->user_id) {
					$class = 'lastPayer';
				} else {
					$class = '';
				}
				?>
                <div class="col-md-3 col-xs-12 col-sm-6 single <?php echo $class; ?>">
                    <div class="group featured">
                        <a href="{{url('/')}}/profile/{{$feature->id}}/{{$feature->name_slug}}">
                            @if($feature->photo != "")
                                <img src="{{imageUrl(url('/').'/assets/images/profile_photo/' . $feature->photo, 250, 250)}}" class="profile-image" alt="{{$feature->name}}" width="250" height="250">
                            @else
                                <img src="{{url('/')}}/assets/images/profile_photo/avatar.jpg" style="max-width: 100%;" class="profile-image" alt="{{$feature->name}}">
                            @endif
                            @if($feature->company == 1)
                                <span class="company">COMPANIE</span>
                            @endif
                            <span class="premium">PREMIUM</span>
                            <div class="text-center listing">
                                <h3 class="no-margin go-bold">{{$feature->name}}</h3>
                                <p class="no-margin">{{$feature->category}} - {{$feature->city}}</p>
                            </div>
                        </a>
                    </div>
                </div>
            @endforeach
        </div>
        <div class="container">
            <hr style="margin-top:15px;">
            <h2 class="text-center">Nail Artistii</h2>
            <hr>
            @foreach($allUsers as $user)
                <div class="col-md-3 col-xs-12 col-sm-6 single">
                    <div class="group featured">
                        <a href="{{url('/')}}/profile/{{$user->id}}/{{$user->name_slug}}">
                            @if($user->photo != "")
                                <img src="{{imageUrl(url('/').'/assets/images/profile_photo/' . $user->photo, 250, 250)}}" class="profile-image" alt="{{$feature->name}}">
                            @else
                                <img src="{{url('/')}}/assets/images/profile_photo/avatar.jpg" style="max-width: 100%;" class="profile-image" alt="{{$user->name}}">
                            @endif
                            @if($user->company == 1)
                                <span class="company">COMPANIE</span>
                            @endif
                            <div class="text-center listing">
                                <h3 class="no-margin go-bold">{{$user->name}}</h3>
                                <p class="no-margin">{{$user->category}} - {{$user->city}}</p>
                            </div>
                        </a>
                    </div>
                </div>
            @endforeach
        </div>
    </div>
@stop

@section('footer')
    <!-- begin olark code -->
    <script type="text/javascript" async>
        ;(function (o, l, a, r, k, y) {
            if (o.olark) return;
            r = "script";
            y = l.createElement(r);
            r = l.getElementsByTagName(r)[0];
            y.async = 1;
            y.src = "//" + a;
            r.parentNode.insertBefore(y, r);
            y = o.olark = function () {
                k.s.push(arguments);
                k.t.push(+new Date)
            };
            y.extend = function (i, j) {
                y("extend", i, j)
            };
            y.identify = function (i) {
                y("identify", k.i = i)
            };
            y.configure = function (i, j) {
                y("configure", i, j);
                k.c[i] = j
            };
            k = y._ = {s: [], t: [+new Date], c: {}, l: a};
        })(window, document, "static.olark.com/jsclient/loader.js");
        /* Add configuration calls below this comment */
        olark.identify('6892-872-10-5248');
        <!-- end olark code -->
        window.SGPMPopupLoader = window.SGPMPopupLoader || {
            ids: [], popups: {}, call: function (w, d, s, l, id) {
                w['sgp'] = w['sgp'] || function () {
                    (w['sgp'].q = w['sgp'].q || []).push(arguments[0]);
                };
                var sg1 = d.createElement(s), sg0 = d.getElementsByTagName(s)[0];
                if (SGPMPopupLoader && SGPMPopupLoader.ids && SGPMPopupLoader.ids.length > 0) {
                    SGPMPopupLoader.ids.push(id);
                    return;
                }
                SGPMPopupLoader.ids.push(id);
                sg1.onload = function () {
                    SGPMPopup.openSGPMPopup();
                };
                sg1.async = true;
                sg1.src = l;
                sg0.parentNode.insertBefore(sg1, sg0);
                return {};
            }
        };
        SGPMPopupLoader.call(window, document, 'script', 'https://popupmaker.com/assets/lib/SGPMPopup.min.js', 'dd00807f');

        <!-- Begin Inspectlet Embed Code -->
        (function () {
            window.__insp = window.__insp || [];
            __insp.push(['wid', 1760245627]);
            var ldinsp = function () {
                if (typeof window.__inspld != "undefined") return;
                window.__inspld = 1;
                var insp = document.createElement('script');
                insp.type = 'text/javascript';
                insp.async = true;
                insp.id = "inspsync";
                insp.src = ('https:' == document.location.protocol ? 'https' : 'http') + '://cdn.inspectlet.com/inspectlet.js?wid=1760245627&r=' + Math.floor(new Date().getTime() / 3600000);
                var x = document.getElementsByTagName('script')[0];
                x.parentNode.insertBefore(insp, x);
            };
            setTimeout(ldinsp, 0);
        })();
    </script>
    <!-- End Inspectlet Embed Code -->
@stop