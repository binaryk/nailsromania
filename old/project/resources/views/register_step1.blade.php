<?php
$style = 'register';
?>

@extends('includes.master')

@section('content')
    @if(!$mobile)
        @include('parts.header', ['background'=>'background_register.jpg'])
    @endif
    <div id="wrapper" class="go-section">
        <div class="row">
            <div class="container">
                <div class="row text-center">
                    <img src="{{url('/')}}/assets/images/PASUL1.png" alt="pasul1" class="img-responsive" style="margin:0 auto;"/>
                </div>
                <hr>
                <div id="resp" class="col-md-6 col-md-offset-3">
                    @if ($errors->has('name'))
                        <span class="help-block">
							<strong>* {{ $errors->first('name') }}</strong>
						</span>
                    @endif

                    @if ($errors->has('email'))
                        <span class="help-block">
							<strong>* {{ $errors->first('email') }}</strong>
						</span>
                    @endif

                    @if ($errors->has('phone'))
                        <span class="help-block">
							<strong>* {{ $errors->first('phone') }}</strong>
						</span>
                    @endif
                </div>
                <form id="registerForm" action="{{route('user.registration.submit')}}" method="post">
                    {{csrf_field()}}
                    <div class="row">
                        <div class="col-md-6 col-md-offset-3">
                            <div class="form-group">
                                <input @if(old('name') != '') value="{{old('name')}}" @endif name="name" placeholder="Numele complet" class="form-control" type="text" required>
                                <p id="nameError" class="errorMsg"></p>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-6 col-md-offset-3">
                            <div class="form-group">
                                <input @if(old('email') != '') value="{{old('email')}}" @endif name="email" placeholder="Email" class="form-control" type="email" required>
                                <p id="emailError" class="errorMsg"></p>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-6 col-md-offset-3">
                            <div class="form-group">
                                <input @if(old('phone') != '') value="{{old('phone')}}" @endif name="phone" placeholder="Telefon" class="form-control" type="text" required>
                                <p id="nameError" class="errorMsg"></p>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-md-10 col-md-offset-3"></div>

                        <label class="col-md-5 control-label"></label>
                        <div class="col-md-2">
                            <button type="submit" id="RegButton" class="btn btn-ocean btn-block">
                                <strong>Mergi la pasul 2</strong>
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@stop

@section('footer')
<script type="text/javascript">
    $('#RegButton').click(function (e) {
        e.preventDefault();
        gtag_report_conversion('{{url()->current()}}');
        var dataStr = $('#registerForm').serialize();

        $.ajax({
            type: "POST",
            url: "{{route('user.registration.submit')}}",
            data: dataStr,
            success: function (r) {
                r = JSON.parse(r);
                console.log(r);
                window.location.href = r.login_url;
            },
            error: function (e) {

            }
        });
    });
</script>
@stop