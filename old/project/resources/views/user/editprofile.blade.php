@extends('user.includes.master-user')

@section('content')
    @include('parts.header', ['background'=>'background_login.jpg'])
    <div id="page-wrapper">
        <div class="container">
            @include('user.parts.profile')
        </div>
        <!-- /.row -->
    </div>
    <!-- /.container-fluid -->
    </div>
    <!-- /#page-wrapper -->
@stop

@section('footer')
    <script type="text/javascript">
        $("#uploadTrigger").click(function () {
            $("#uploadFile").click();
            $("#uploadFile").change(function (event) {
                $("#uploadTrigger").html($("#uploadFile").val());
            });
        });

        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('#docphoto').attr('src', e.target.result);
                };

                reader.readAsDataURL(input.files[0]);
            }
        }

        bkLib.onDomLoaded(function () {
            new nicEditor().panelInstance('profiledesc');
        });

        $('#addField').click(function () {
            $('#qualiTies').append('<div class="item form-group qfield"><div class="col-md-5 col-sm-5 col-xs-5"><input class="form-control col-md-12 col-xs-12" name="q_titles[]" placeholder="Title" required="required" type="text"></div><div class="col-md-5 col-sm-5 col-xs-12"><input class="form-control col-md-12 col-xs-12" name="qualities[]" placeholder="Text / Details" required="required" type="text"></div><div class="col-md-2 col-sm-2 col-xs-12"><span class="removeField"><i class="fa fa-times-circle fa-2x"></i></span></div></div>');
            $('.removeField').click(function () {
                $(this).parent().parent().remove();
            });
        });

        $('.removeField').click(function () {
            $(this).parent().parent().remove();
        });

        //Google maps details
        var componentForm = {
            address: 'long_name',
            locality: 'short_name'
        };
        var input_load = document.getElementById('address_input');
        var options = {
            types: ['geocode']
        };
        address = new google.maps.places.Autocomplete(input_load, options);
        address.addListener('place_changed', complete_address_fields);

        function complete_address_fields() {
            var place = address.getPlace();
            console.log(place);
            document.getElementById('lat_input').value = place.geometry.location.lat();
            document.getElementById('lng_input').value = place.geometry.location.lng();

            for (var i = 0; i < place.address_components.length; i++) {
                var addressType = place.address_components[i].types[0];
                if (componentForm[addressType]) {
                    var val = place.address_components[i][componentForm[addressType]];
                    document.getElementById(addressType + '_input').value = val;
                }
            }
        }
    </script>
@stop