<!-- Page Heading -->
<div class="go-title">
    <div class="pull-right">
        <a href="{!! url('/user/dashboard') !!}" class="btn btn-default btn-back"><i class="fa fa-arrow-left"></i> Inapoi la pagina de profil</a>
    </div>
    <h3>Editeaza profilul</h3>
    <div class="go-line"></div>
</div>
<!-- Page Content -->
<div class="panel panel-default">
    <div class="panel-body">
        <div id="response">
            @if(Session::has('message'))
                <div class="alert alert-success alert-dismissable">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                    {{ Session::get('message') }}
                </div>
            @endif
        </div>
        <form method="POST" action="{{ action('UserProfileController@update', ['id' => $user->id]) }}" class="form-horizontal form-label-left" enctype="multipart/form-data">
            {{csrf_field()}}
            <div class="item form-group">
                <label class="control-label col-md-2 col-sm-2 col-xs-12" for="name">Nume complet<span class="required">*</span>
                </label>
                <div class="col-md-10 col-sm-10 col-xs-12">
                    <input class="form-control col-md-7 col-xs-12" value="{{$user->name}}" name="name" placeholder="Nume Artist" required="required" type="text">
                </div>
            </div>
            <div class="item form-group">
                <label class="control-label col-md-2 col-sm-2 col-xs-12" for="name">Selecteaza Categoria<span class="required">*</span>
                </label>
                <div class="col-md-10 col-sm-10 col-xs-12">
                    <select name="category" class="form-control" required>
                        <option value="">Selecteaza Categoria</option>
                        @foreach($categories as $category)
                            @if($category->name == $user->category)
                                <option value="{{$category->name}}" selected>{{$category->name}}</option>
                            @else
                                <option value="{{$category->name}}">{{$category->name}}</option>
                            @endif
                        @endforeach
                    </select>
                </div>
            </div>

            <div class="item form-group">
                <label class="control-label col-md-2 col-sm-2 col-xs-12" for="name">Fotografie actuala<span class="required">*</span>
                </label>
                <div class="col-md-10 col-sm-10 col-xs-12">
                    <img src="{!! url('assets/images/profile_photo') !!}/{{$user->photo}}" style="max-width: 200px;" alt="No Photo Added" id="docphoto">
                </div>
            </div>

            <div class="item form-group">
                <label class="control-label col-md-2 col-sm-2 col-xs-12" for="number"> Imagine profil <span class="required">*</span>
                </label>
                <div class="col-md-10 col-sm-10 col-xs-12">
                    <input type="file" accept="image/*" name="photo" class="hidden" onchange="readURL(this)" id="uploadFile"/>
                    <div id="uploadTrigger" class="form-control btn btn-default"><i class="fa fa-upload"></i> Add Profile Photo</div>
                    <br><br>
                    <p class="small-label">Prefered Size: (600x600) or Square Sized Image</p>
                </div>
            </div>

            <div class="item form-group">
                <label class="control-label col-md-2 col-sm-2 col-xs-12" for="name">Desriere profil<span class="required">*</span>
                </label>
                <div class="col-md-10 col-sm-10 col-xs-12">
                    <textarea id="profiledesc" name="description">{!! $user->description !!}</textarea>
                </div>
            </div>

            <div class="form-line"></div>

            <div class="item form-group">
                <label class="control-label col-md-2 col-sm-2 col-xs-12" for="name">Produse folosite <span class="required">*</span>
                    <p class="small-label">Separate de virgula</p>
                </label>
                <div class="col-md-10 col-sm-10 col-xs-12">
                    <input class="form-control col-md-10 col-xs-12" name="specialities" value="{{$user->specialities}}" placeholder="Practice Areas" type="text">
                </div>
            </div>

            <div class="item form-group">
                <label class="control-label col-md-2 col-sm-2 col-xs-12" for="name">Gender<span class="required">*</span>
                </label>
                <div class="col-md-10 col-sm-10 col-xs-12">
                    <select class="form-control" name="gender" required>
                        <option value="">Selecteaza sex</option>
                        @if($user->gender == "Male")
                            <option value="Male" selected>Barbat</option>
                        @else
                            <option value="Male">Barbat</option>
                        @endif
                        @if($user->gender == "Female")
                            <option value="Female" selected>Femeie</option>
                        @else
                            <option value="Female">Femeie</option>
                        @endif

                    </select>
                </div>
            </div>
            <div class="item form-group">
                <h4 class="col-md-offset-2 col-md-2 col-sm-6 col-xs-12" style="margin-bottom: 0">Calificari</h4>
            </div>
            <div class="form-line"></div>
            <div class="col-md-offset-2 col-md-10" style="padding-left: 5px;" id="qualiTies">
                @if(!empty($user->qtitles))
                    @foreach(array_combine($user->qtitles, $user->qualifications) as $title => $quality)
                        <div class="item form-group qfield">
                            <div class="col-md-6 col-sm-6 col-xs-6">
                                <input class="form-control col-md-12 col-xs-12" name="q_titles[]" value="{{$title}}" placeholder="Numele Scolii/Institutiei" type="text">
                            </div>
                            <div class="col-md-5 col-sm-5 col-xs-5">
                                <input class="form-control col-md-12 col-xs-12" name="qualities[]" value="{{$quality}}" placeholder="Specializare" type="text">
                            </div>
                            <div class="col-md-1 col-sm-1 col-xs-1">
                                <span class="removeField"><i class="fa fa-times-circle fa-2x"></i></span>
                            </div>
                        </div>
                    @endforeach
                @else
                    <div class="item form-group qfield">
                        <div class="col-md-6 col-sm-6 col-xs-6">
                            <input class="form-control col-md-12 col-xs-12" name="q_titles[]" placeholder="Numele Scolii/Institutiei" type="text">
                        </div>
                        <div class="col-md-5 col-sm-5 col-xs-5">
                            <input class="form-control col-md-12 col-xs-12" name="qualities[]" placeholder="Specializare" type="text">
                        </div>
                        <div class="col-md-1 col-sm-1 col-xs-1">
                            <span class="removeField"><i class="fa fa-times-circle fa-2x"></i></span>
                        </div>
                    </div>
                @endif
            </div>
            <div class="item form-group">
                <div class="col-md-offset-2 col-md-10">
                    @if(count($docs) > 0)
                        <a href="{!! url('/user/gallery', ['type'=>'document']) !!}" class="add-image" style="float:right; width:auto; padding: 10px 15px;margin-left:15px;">
                            <i class="fa fa-pencil"></i> Editeaza diplome
                        </a>
                    @endif
                    <button class="btn btn-default" type="button" id="addField"><i class="fa fa-plus fa-fw"></i>Adauga</button>
                    <a href="{!! url('/user/add_document') !!}" class="add-image" style="float:right; width:auto; padding: 10px 15px;">
                        <i class="fa fa-plus"></i> Adauga diplome
                    </a>
                </div>
            </div>

            <div class="form-line"></div>
            <div class="item form-group">
                <label class="control-label col-md-2 col-sm-2 col-xs-12" for="slug">Adresa<span class="required">*</span>
                </label>
                <div class="col-md-10 col-sm-10 col-xs-12">
                    <input class="form-control col-md-7 col-xs-12" value="{{$user->address}}" id="address_input" name="address" placeholder="Adresa" required="required" type="text">
                    <input value="{{$user->city}}" id="locality_input" name="city" type="hidden">
                    <input type="hidden" name="latitude" id="lat_input" value="{{$user->latitude}}"/>
                    <input type="hidden" name="longitude" id="lng_input" value="{{$user->longitude}}"/>
                </div>
            </div>
            <div class="item form-group">
                <label class="control-label col-md-2 col-sm-2 col-xs-12" for="slug">Telefon<span class="required">*</span>
                </label>
                <div class="col-md-10 col-sm-10 col-xs-12">
                    <input class="form-control col-md-7 col-xs-12" value="{{$user->phone}}" name="phone" placeholder="Phone" type="text">
                </div>
            </div>
            <div class="item form-group">
                <label class="control-label col-md-2 col-sm-2 col-xs-12" for="slug">Fax<span class="required">*</span>
                </label>
                <div class="col-md-10 col-sm-10 col-xs-12">
                    <input class="form-control col-md-7 col-xs-12" value="{{$user->fax}}" name="fax" placeholder="Fax" type="text">
                </div>
            </div>
            <div class="item form-group">
                <label class="control-label col-md-2 col-sm-2 col-xs-12" for="slug">Email<span class="required">*</span>
                </label>
                <div class="col-md-10 col-sm-10 col-xs-12">
                    <input class="form-control col-md-7 col-xs-12" value="{{$user->email}}" name="email" placeholder="Email" required="required" type="text" disabled>
                </div>
            </div>
            <div class="item form-group">
                <label class="control-label col-md-2 col-sm-2 col-xs-12" for="slug">Website<span class="required">*</span>
                </label>
                <div class="col-md-10 col-sm-10 col-xs-12">
                    <input class="form-control col-md-7 col-xs-12" value="{{$user->website}}" name="website" placeholder="Website" type="text">
                </div>
            </div>

            <div class="ln_solid"></div>
            <div class="form-group">
                <div class="col-md-offset-3 col-md-10">
                    <p class="small-label" style="font-weight:900;">*Nota: Artistii isi asuma responsabilitatea validitatii acestor date.</p>
                </div>
                <div class="col-md-6 col-md-offset-3">
                    <button id="add_ads" type="submit" class="btn btn-success btn-block">Actualizeaza profil</button>
                </div>
            </div>
        </form>
    </div>
</div>