<!-- Page Heading -->
<div class="go-title">
    <div class="pull-right">
        <a href="{!! route('user.dashboard') !!}" class="btn btn-default btn-back"><i class="fa fa-arrow-left"></i> Back</a>
    </div>
    <h3>Images</h3>
    <div class="go-line"></div>
</div>
<!-- Page Content -->
<div class="panel panel-default">
    <div class="panel-body">
        <table class="table table-striped table-bordered" cellspacing="0" id="example" width="100%">
            <thead>
            <tr>
                <th>Photo</th>
                <th>Caption</th>
                <th>Actions</th>
            </tr>
            </thead>
            <tbody>
            @foreach($gallerydata as $image)
                <tr>
                    <td><img src="/assets/images/gallery/{{$image->image}}" class="doctorimg" alt="Shaon"></td>
                    <td>{{$image->caption}}</td>
                    <td>
                        <form method="POST" action="{!! action('GalleryController@getDelete', ['id' => $image->id]) !!}">
                            {{csrf_field()}}
                            <input type="hidden" name="_method" value="DELETE">
                            <a href="/images_delete/{{$image->id}}" class="btn btn-danger btn-lg"><i class="fa fa-trash"></i> Delete </a>
                        </form>
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
</div>