@extends('user.includes.master-user')



@section('content')

    @include('parts.header', ['background'=>'background_login.jpg'])

    <div id="page-wrapper">

        <div class="container">

            <div class="row" id="main">

                <!-- Page Heading -->

                <div class="go-title">

                    <div class="pull-right">

                        <a href="{!! url('/user/dashboard') !!}" class="btn btn-default btn-back">

                            <i class="fa fa-arrow-left"></i> Inapoi la pagina de profil

                        </a>

                    </div>

                    <h3>&nbsp;</h3>

                    <div class="go-line"></div>

                </div>

                <!-- Page Content -->

                <div class="panel panel-default">

                    <div class="panel-body">

                        <div id="response"></div>

                        <span class="btn fileinput-button btn-block" style="padding:25px; background-color:#9800a1;color:#fff; text-transform:uppercase;">

                            <i class="fa fa-plus"></i>

                            <span>Selecteaza fisiere...</span>

                            <input id="fileupload" type="file" name="files[]" multiple/>

                            <input type="hidden" name="file_type" value="image"/>

                        </span>

                        <br>

                        <br>

                        <!-- The global progress bar -->

                        <div id="progress" class="progress">

                            <div class="progress-bar progress-bar-success"></div>

                        </div>

                        <br>

                        <p class="small-label text-center" style="color:#9800a1;"><b>*Nota: </b></p>

                    </div>

                </div>

            </div>

        </div>

        @stop



        @section('footer')

            <script type="text/javascript">

                $(function () {

                    'use strict';

                    $('#fileupload').fileupload({

                        url: '/user/upload/document',

                        dataType: 'json',

                        done: function (e, data) {

                            var message = '<div class="alert alert-success alert-dismissable"><a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>' + data.result.message + '</div>';

                            $('#response').html(message);

                        }

                    }).prop('disabled', !$.support.fileInput)

                        .parent().addClass($.support.fileInput ? undefined : 'disabled');

                });

            </script>

@stop

