@extends('user.includes.master-user')

@section('header')
    <script type="text/javascript">
        @if (Request::has('new_user'))
        fbq('track', 'CompleteRegistration');
        @endif
            (function (p, u, s, h) {
                p._pcq = p._pcq || [];
                p._pcq.push(['_currentTime', Date.now()]);
                s = u.createElement('script');
                s.type = 'text/javascript';
                s.async = true;
                s.src = 'https://cdn.pushcrew.com/js/ee99f11878310a97fc0a7681445e4b16.js';
                h = u.getElementsByTagName('script')[0];
                h.parentNode.insertBefore(s, h);
            })(window, document);
    </script>
@stop

@section('content')
    @include('parts.header', ['background'=>'background_login.jpg'])

    <div id="page-wrapper">
        <div class="container">
            <div class="row" id="main">
                <div class="container">
                    <div class="col-md-12 text-center">
                        <div style="display: inline-block; margin:auto;">
                            <p><font size="3" color="red">Completeaza-ti adresa pentru a fi vazut pe harta din prima pagina alaturi de ceilalti Makeup-Artisti.<br/>Apasa pe butonul <b>Editeaza-ti profilul</b> de mai jos. Succes.</font></p>
                            <div class="col-md-12 text-center">
                                <div class="row">
                                    <a href="{{route('user.profile.edit')}}" class="edit-profile"><i class="fa fa-pencil"></i> Editeaza-ti profilul</a>
                                </div>

                                <div class="row top-buffer text-center">
                                    @if(count($gallerydata) < 20)
                                        <a href="{!! url('/user/add_image') !!}" class="add-image" style="padding: 10px 15px;"><i class="fa fa-plus"></i> Adauga galerie foto</a>
                                    @else
                                        <a href="{!! url('/user/gallery', ['type'=>'image']) !!}" class="add-image" style="float:right; width:auto; padding: 10px 15px;margin-left:15px;"><i class="fa fa-pencil"></i> Editeaza galeria foto</a>
                                    @endif
                                </div>

                                <div class="row top-buffer">
                                    @if(count($videodata) < 1)
                                        <a href="{!! url('/user/add_video') !!}" class="add-image"><i class="fa fa-plus"></i> Adauga video</a>
                                    @else
                                        <a href="{!! url('/user/gallery', ['type'=>'video']) !!}" class="add-image"><i class="fa fa-plus"></i> Editeaza galeria video</a>
                                    @endif
                                </div>

                                <div class="row top-buffer">
	                                <?php $style = ''; ?>
                                    @if($user->featured != 1)
		                                <?php $style = ' style="float:right;margin-right:15%;width:33%;"'; ?>
                                        <a href="javascript:" data-toggle="modal" data-target="#ModalFeatured" class="edit-profile" style="float:left;margin-left:15%;width:33%;">
                                            <i class="fa fa-money"></i> Activeaza-ti profilul premium
                                        </a>
                                    @endif
                                    <a href="/premium" class="edit-profile"<?php echo $style;?>>
                                        <i class="fa fa-money"></i> Activeaza banner PREMIUM pe site
                                    </a>
                                </div>

                                <div class="row top-buffer">
                                    <a class="view-profile" href="{!! url('/profile/'. Auth::user()->id .'/'. Auth::user()->name_slug) !!}" target="_blank"><b class="fa fa-user"></b> Vezi profilul tau</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- /#page-wrapper -->

    @if($user->status != 1)
        @include('user.parts.modal_activate')
    @endif

	<?php
	$item_amount = $settings[0]->normal_price;

	if ($user->status == '1') {
		$item_amount = 0.00;
	}

	if ($user->featured != 1) {
	$item_amount = $item_amount + $settings[0]->featured_price;
	$feature_price_text = 'Cost abonament: ';
	$feature_price_text .= ($user->status != 1) ? $settings[0]->normal_price . ' + ' . $settings[0]->featured_price : $settings[0]->featured_price;
	$feature_price_text .= ' EUR';
	?>

    @include('user.parts.modal', [$modal_id = 'ModalFeatured', $modal_title = 'Activeaza profil Premium', $modal_subject = 'Vei fi afisat pe prima pagina la sectiunea Nail Artisti PREMIUM.<br />Vei avea un simbol distinct fata de ceilalti Nail Artisti pe harta din prima pagina.<br />Vei putea sa iti urci un material video pe pagina de profil, pe langa portofoliul foto.', $feature_price = $feature_price_text, $feature = 'featured'])

	<?php } ?>
@stop

@section('footer')
    <script type="text/javascript">
        $("#opt").change(function () {
            var opt = $("#opt").val();
            if (opt == "anual") {
                $("#cost").html("Cost abonament: {{$settings[0]->anual_price}} EUR");
            } else {
                $("#cost").html("Cost abonament: {{$settings[0]->monthly_price}} EUR");
            }
        });
    </script>
@stop