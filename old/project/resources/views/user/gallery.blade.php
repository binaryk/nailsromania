@extends('user.includes.master-user')



@section('content')

    @include('parts.header', ['background'=>'background_login.jpg'])

    <div id="page-wrapper">
        <div class="container">
            <div class="row" id="main">
                <div class="container">
                    <!-- Page Heading -->
                    <div class="go-title">
                        <div class="pull-right">
                            <a href="{!! route('user.dashboard') !!}" class="btn btn-default btn-back"><i class="fa fa-arrow-left"></i> Inapoi la pagina de profil</a>
                        </div>
                        <h3>&nbsp;</h3>
                        <div class="go-line"></div>
                    </div>

                    <!-- Page Content -->

                    <div class="panel panel-default">
                        <div class="panel-body">
                            <table class="table table-striped table-bordered" cellspacing="0" id="example" width="100%">
                                <thead>
                                <tr>
                                    <th>File</th>
                                    <th>Actions</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($gallerydata as $item)

                                    <tr>
                                        <td><img src="/assets/images/gallery/{{$item->image}}" class="doctorimg"></td>
                                        <td>
                                            <form method="POST" action="{!! action('GalleryController@getDelete', [ 'id' => $item->id, 'type' => $item->file_type ]) !!}">
                                                {{csrf_field()}}

                                                <input type="hidden" name="_method" value="DELETE">
                                                <a href="/user/delete/{{$item->id}}/{{$item->file_type}}" class="btn btn-delete btn-block"><i class="fa fa-trash"></i> Sterge</a>

                                            </form>

                                        </td>

                                    </tr>

                                @endforeach

                                </tbody>

                            </table>

                        </div>

                    </div>

                </div>

            </div>

        </div>

    </div>

@stop



@section('footer')

@stop

