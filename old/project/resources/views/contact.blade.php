@extends('includes.master')

@section('content')


    <section style="background: url({{url('/')}}/assets/images/{{$settings[0]->background}}) no-repeat center center; background-size: cover;">
        <div class="row" style="background-color:rgba(0,0,0,0.7);">

            <div style="margin: 3% 0px 3% 0px;">
                <div class="text-center" style="color: #FFF;padding: 20px;">
                    <h1>Contact</h1>
                </div>
            </div>

        </div>
    </section>

    <div id="wrapper" class="go-section">
        <div class="row">
            <div class="container">
                <div class="col-md-12">
                    <div class="container">
                        <!-- Form Name -->
                        <h3>Contacteaza-ne</h3>
                        <hr>

                        <form action="{{action('FrontEndController@contactmail')}}" method="post">
                            {{csrf_field()}}
                            <input type="hidden" name="to" value="{{$pagedata->contact_email}}">
                            <!-- Success message -->
                            <div style="display: none;" class="alert alert-success" role="alert" id="success_message">
                                @if(Session::has('cmail'))
                                    {{ Session::get('cmail') }}
                                @endif
                            </div>
                            <!-- Text input-->
                            <div class="row">
                                <div class="form-group col-md-6">
                                    <input name="name" placeholder="Nume" class="form-control" type="text" required>
                                    <p id="nameError" class="errorMsg"></p>
                                </div>

                                <div class="form-group col-md-6">
                                    <input name="email" placeholder="Email" class="form-control" type="text" required>
                                    <p id="emailError" class="errorMsg"></p>
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group col-md-6">
                                    <input name="phone" placeholder="Telefon" class="form-control" type="text" required>
                                    <p id="phoneError" class="errorMsg"></p>
                                </div>

                                <div class="form-group col-md-6">
                                    <select name="department" class="form-control selectpicker" required>
                                        <option value="">Sex</option>
                                        <option value="Male">Barbat</option>
                                        <option value="Female">Femeie</option>
                                    </select>
                                    <p id="departmentError" class="errorMsg"></p>
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group col-md-12">
                                    <textarea class="form-control" placeholder="Message" name="message" rows="8" required></textarea>
                                    <p id="messageError" class="errorMsg"></p>
                                </div>
                            </div>

                            <div id="resp"></div>
                            <!-- Button -->
                            <div class="form-group">
                                <label class="col-md-5 control-label"></label>
                                <div class="col-md-2">
                                    <button type="submit" class="btn btn-primary btn-block">TRIMITE</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>


@stop

@section('footer')

@stop