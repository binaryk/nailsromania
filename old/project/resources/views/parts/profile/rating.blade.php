<?php
$rating = (float) \App\Review::where('userid', $profiledata->id)->avg('rating');
$count = \App\Review::where('userid', $profiledata->id)->count();
$total = '' . $count . ' Recenzii';
$trophy = '';
if ($count > 10 && $count < 20) {
	$trophy = 'Acest utilizator este:<br /><i class="fa fa-trophy" aria-hidden="true" style="font-size: 30px;"></i><br /><span style="font-weight:900;text-transform:uppercase;">Membru novice</span>';
} elseif ($count > 20 && $count < 50) {
	$trophy = 'Acest utilizator este:<br /><i class="fa fa-trophy" aria-hidden="true" style="color:silver; font-size: 30px;"></i><br /><span style="font-weight:900;text-transform:uppercase;">Silver member</span>';
} elseif ($count > 50) {
	$trophy = 'Acest utilizator este:<br /><i class="fa fa-trophy" aria-hidden="true" style="color:gold; font-size: 30px;"></i><br /><span style="font-weight:900;text-transform:uppercase;">Gold member</span>';
}
?>
<section style="background: url({{url('/')}}/assets/images/{{$settings[0]->background}}) no-repeat center center; background-size: auto; background-attachment:scroll;">
    <div class="row">
        <div style="margin: 3% 0px 3% 0px;">
            <div class="text-center" style="color: #FFF; padding: 20px;">
                <h1>{{$profiledata->name}}</h1>
                <div>Nail Artist din categoria: <span style="font-weight:900;">{{$profiledata->category}}</span></div>
                <div class="ratings">
                    <div class="empty-stars"></div>
                    <div class="full-stars" style="width:{{number_format($rating, 1, '.', '')*20}}%"></div>
                </div>
                <div class="rating-number">({{number_format($rating, 1, '.', '')}}/5)</div>
                <div class="rating-number">{{$total}}<br/><?php echo $trophy; ?></div>
            </div>
        </div>
    </div>
</section>