<div class="profile-section">
    <h3 class="no-margin">Teaser</h3>
    <hr>
    <div class="videocontent">
        <video id="my-video" class="video-js vjs-default-skin vjs-big-play-centered" controls preload="auto" width="640" height="264" data-setup='{"fluid":true}'>
            <source src="/assets/images/gallery/{{$videos->image}}" type='video/mp4'>
            <p class="vjs-no-js">
                To view this video please enable JavaScript, and consider upgrading to a web browser that
            </p>
        </video>
    </div>
</div>