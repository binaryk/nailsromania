<div class="col-md-4 contact-info">
    @if($profiledata->photo != "")
        <img src="{{url('/')}}/assets/images/profile_photo/{{$profiledata->photo}}" alt="" style="max-width: 100%;" class="profile-image">
    @else
        <img src="{{url('/')}}/assets/images/profile_photo/avatar.jpg" class="profile-image" alt="{{$profiledata->name}}">
    @endif
    <div>
        <h3>Contact Info</h3>
        <hr>
        <div class="profile-group">
            @if($profiledata->address != '')
                <p class="profile-contact">
                    <i class="fa fa-home fa-1x"></i>
                    <a target="_blank" rel="nofollow" href="https://www.google.com/maps/dir/Current+Location/{{$profiledata->latitude}},{{$profiledata->longitude}}/">{{$profiledata->address}}</a>
                </p>
            @endif
            @if($profiledata->city != '')
                <p class="profile-contact"><i class="fa fa-building fa-1x"></i> {{$profiledata->city}}</p>
            @endif
            @if($profiledata->fax != NULL)
                <p class="profile-contact">
                    <i class="fa fa-fax fa-1x"></i> {{$profiledata->fax}}
                </p>
            @endif
            @if($profiledata->phone != NULL)
                <p class="profile-contact">
                    <i class="fa fa-phone fa-1x"></i>
                    <a href="tel:{{$profiledata->phone}}">{{$profiledata->phone}}</a>
                </p>
            @endif
            <p class="profile-contact">
                <i class="fa fa-envelope fa-1x"></i>
                {!! obfuscateEmail($profiledata->email) !!}
            </p>
            @if($profiledata->website != NULL)
                <p class="profile-contact">
                    <i class="fa fa-globe fa-1x"></i>
                    <a href="{{$profiledata->website}}" target="_blank">{{$profiledata->website}}</a>
                </p>
            @endif
            @if($profiledata->latitude != NULL && $profiledata->longitude != NULL)
                <a target="_blank" rel="nofollow" href="https://www.google.com/maps/dir/Current+Location/{{$profiledata->latitude}},{{$profiledata->longitude}}/" class="map_static_zoom show_map map_static_hover jq_tooltip"
                   data-source="map_thumbnail">
                    <img src="//maps.googleapis.com/maps/api/staticmap?&zoom=13&center={{$profiledata->latitude}},{{$profiledata->longitude}}&size=360x300&scale=1&maptype=roadmap&format=png&markers=size:mid%7Ccolor:blue%7Clabel:%7C{{$profiledata->latitude}},{{$profiledata->longitude}}&key={{GMAPS_KEY}}" width="360" height="300"/>
                    <span class="map-thumb__marker--current"></span>
                </a>
            @endif
        </div>
        <h3>Distribuie acest Profil!</h3>
        <hr>
        <div class="profile-group">
            <!-- AddToAny BEGIN -->
            <div class="a2a_kit a2a_kit_size_32 a2a_default_style">
                <a class="a2a_button_facebook"></a>
            </div>
            <script async src="//static.addtoany.com/menu/page.js"></script>
            <!-- AddToAny END -->
        </div>
    </div>
    <div class="text-center">
        <div class="desktop-advert">
            @if(!empty($ads300x250))
                @if($ads300x250->type == "banner")
                    <a class="ads" href="{{$ads300x250->redirect_url}}" target="_blank">
                        <img class="banner-300x250" src="{{url('/')}}/assets/images/ads/{{$ads300x250->banner_file}}" alt="Advertisement">
                    </a>
                @else
                    {!! $ads300x250->script !!}
                @endif
            @endif
        </div>
    </div>
</div>