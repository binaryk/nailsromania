<div class="tab-pane fade in active" id="contact">
    <div class="col-md-12">
        <h3>Trimite o cerere de colaborare</h3>
        <hr>
        <form action="{{action('FrontEndController@usermail')}}" method="post">
            {{csrf_field()}}
            <input type="hidden" name="to" value="{{$profiledata->email}}">
            <input type="hidden" name="to_name" value="{{$profiledata->name}}">
            <div class="form-group">
                <label>Nume:</label>
                <input type="text" id="name" name="name" class="form-control" required>
                <p id="vname" style="color:red;"></p>
            </div>
            <div class="form-group">
                <label>Email:</label>
                <input type="text" id="email" name="email" class="form-control" required>
                <p id="vemail" style="color:red;"></p>
            </div>
            <div class="form-group">
                <label>Telefon:</label>
                <input type="text" id="phone" name="phone" class="form-control" required>
                <p id="vphone" style="color:red;"></p>
            </div>
            <div class="form-group">
                <label>Programare:</label>
                <div class='input-group date' id='datetimepicker2'>
                    <input type='text' class="form-control" name="programare"/>
                    <span class="input-group-addon">
                        <span class="glyphicon glyphicon-calendar"></span>
                    </span>
                </div>
            </div>
            <div class="form-group">
                <label>Mesaj:</label>
                <textarea name="message" rows="8" id="message" class="form-control" required></textarea>
                <p id="vmessage" style="color:red;"></p>
            </div>
            <div id="load" style="display: none;" class="text-center"><img src="/assets/images/loader.gif" style="width: 50px;padding-bottom: 5px; "></div>
            <div class="form-group" id="submt">
                <label class="col-md-4 control-label"></label>
                <div class="col-md-4">
                    <button type="submit" class="btn btn-ocean btn-block">TRIMITE PROGRAMARE</button>
                </div>
            </div>

        </form>
    </div>
</div>
<script type="text/javascript">
    $(function () {
        $('#datetimepicker2').datetimepicker({
            locale: 'ro',
            sideBySide: true,
            allowInputToggle: true,
            stepping: 30,
            defaultDate: ''
        });
    });
</script>