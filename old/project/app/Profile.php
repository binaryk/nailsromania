<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Profile extends Authenticatable {
	use Notifiable;
	public $table = "users_profiles";
	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable
		= [
			'name',
			'name_slug',
			'category',
			'description',
			'specialities',
			'gender',
			'qualifications',
			'photo',
			'phone',
			'fax',
			'email',
			'password',
			'address',
			'city',
			'latitude',
			'longitude',
			'website',
			'featured',
			'status',
			'remember_token',
			'company',
		];

	/**
	 * The attributes that should be hidden for arrays.
	 *
	 * @var array
	 */
	protected $hidden
		= [
			'password',
			'remember_token',
		];
}
