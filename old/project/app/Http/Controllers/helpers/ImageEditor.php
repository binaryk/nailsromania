<?php

namespace App\Http\Controllers\helpers;

class ImageEditor {
	private $img;
	private $src;
	private $type = 'jpg';
	private $width = 0;
	private $height = 0;

	/**
	 * Create a new editor
	 *
	 * @param string $src the path to the image
	 */
	function __construct($src) {
		$this->setTypeFromFilename($src);
		$this->img    = imagecreatefromstring(file_get_contents($src));
		$this->width  = imagesx($this->img);
		$this->height = imagesy($this->img);
		$this->src    = $src;
	}

	/**
	 * Set the type of the image
	 *
	 * @param string $type jpg, gif or png are allowed
	 */
	function setType($type) {
		if ( ! in_array($type, ['jpg', 'gif', 'png'])) {
			return;
		}
		$this->type = $type;
		// if($this->type == 'gif') {
		//   imagesavealpha($this->img, true);
		//   imagecolortransparent($this->img, 127<<24);
		// }
		return $this;
	}

	/**
	 * Sets the type of the image from a filename
	 */
	function setTypeFromFilename($filename) {
		$ext = strtolower(pathinfo($filename, PATHINFO_EXTENSION));

		if ($ext == "jpg" || $ext == "jpeg") {
			$this->setType("jpg");
		} elseif ($ext == "gif") {
			$this->setType("gif");
		} elseif ($ext == "png") {
			$this->setType("png");
		}
	}

	/**
	 * Resize the image to fit the given width and height
	 */
	function resizeToFit($width, $height, $enlarge = FALSE) {
		// Get current width & height
		$currentWidth  = $this->width;
		$currentHeight = $this->height;

		// Compute the new width & height
		$newWidth  = $currentWidth;
		$newHeight = $currentHeight;
		if ($newWidth > $width || ($enlarge && $newWidth < $width)) {
			$newWidth  = $width;
			$newHeight = ceil(($newWidth / $currentWidth) * $currentHeight);
		}
		if ($newHeight > $height || ($enlarge && $newHeight < $height)) {
			$newHeight = $height;
			$newWidth  = ceil(($newHeight / $currentHeight) * $currentWidth);
		}

		// Create a new temporary image
		$temp = imagecreatetruecolor($newWidth, $newHeight);
		// If the type is set to png, save the png's transparency
		if ($this->type == 'png') {
			imagealphablending($temp, FALSE);
			imagesavealpha($temp, TRUE);
		}
		// If the type is set to gif, save the gif's transparecy
		if ($this->type == 'gif') {
			imagesavealpha($temp, TRUE);
			imagecolortransparent($temp, 127 << 24);
			$background = imagecolorallocate($temp, 0, 0, 0);
			imagecolortransparent($temp, $background);
		}
		// Resize the image
		imagecopyresampled($temp, $this->img, 0, 0, 0, 0, $newWidth, $newHeight, $currentWidth, $currentHeight);
		imagedestroy($this->img);
		$this->img = $temp;
		// Update the width & height of the image
		$this->width  = $newWidth;
		$this->height = $newHeight;

		return $this;
	}

	/**
	 * Crop a thumbnail from the center
	 */
	function cropThumbnail($width, $height, $enlarge = FALSE) {
		// Compute the ratio
		$ratioWidth  = $this->width / $width;
		$ratioHeight = $this->height / $height;

		// Crop in the desired ratio
		$this->crop(($this->width - $width * $ratioWidth) / 2, ($this->height - $height * $ratioHeight) / 2, $width * $ratioWidth, $height * $ratioHeight);

		// Resize to fit the thumb dimensions
		$this->resizeToFit($width, $height, $enlarge);

		return $this;
	}

	/**
	 * Crop the image
	 *
	 * @param  integer $x      left offset in pixels
	 * @param  integer $y      top offset in pixels
	 * @param  integer $width  crop width in pixels
	 * @param  integer $height crop height in pixels
	 */
	function crop($x, $y, $width, $height) {
		// Get current width & height
		$currentWidth  = $this->width;
		$currentHeight = $this->height;

		// Create a new temporary image
		$temp = imagecreatetruecolor($width, $height);

		// If the type is set to png, save the png's transparency
		if ($this->type == 'png') {
			imagealphablending($temp, FALSE);
			imagesavealpha($temp, TRUE);
		}

		// If the type is set to gif, save the gif's transparecy
		if ($this->type == 'gif') {
			imagesavealpha($temp, TRUE);
			imagecolortransparent($temp, 127 << 24);
			$background = imagecolorallocate($temp, 0, 0, 0);
			imagecolortransparent($temp, $background);
		}

		// Crop the image
		imagecopyresampled($temp, $this->img, 0, 0, $x, $y, $currentWidth, $currentHeight, $currentWidth, $currentHeight);
		imagedestroy($this->img);
		$this->img = $temp;
		// Update the width & height of the image
		$this->width  = $width;
		$this->height = $height;

		return $this;
	}

	/**
	 * Get the width of the image
	 */
	function getWidth() {
		return $this->width;
	}

	/**
	 * Get the height of the iamge
	 */
	function getHeight() {
		return $this->height;
	}

	/**
	 * Print the image in the browser
	 *
	 * @param int $quality 0 < $quality < 100. applicable for png and jpg
	 */
	function show($quality = 100) {
		header('Content-type: image/' . $this->type);

		if ($this->type == 'jpg') {
			imagejpeg($this->img, NULL, $quality);
		} elseif ($this->type == 'png') {
			imagepng($this->img, NULL, ($quality / 100) * 9);
		} else {
			imagegif($this->img);
		}

		return $this;
	}

	/**
	 * Write the image to file
	 *
	 * @param string $filename
	 * @param int    $quality 0 < $quality < 100. applicable for png and jpg
	 */
	function write($filename, $quality = 100) {
		if ($this->type == 'jpg') {
			imagejpeg($this->img, $filename, $quality);
		} elseif ($this->type == 'png') {
			imagepng($this->img, $filename, ($quality / 100) * 9);
		} else {
			imagegif($this->img, $filename);
		}

		return $this;
	}
}
