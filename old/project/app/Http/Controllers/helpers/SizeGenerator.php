<?php

namespace App\Http\Controllers\helpers;

use App\Models\User;
use DB;

class SizeGenerator {
	public function __construct() {
	}

	public function processImage($path, $save_path, $data = []) {
		_pre('processing image ' . $path);

		$default_data = [
			'resize' => TRUE,
			'width'  => 150,
			'height' => 150,

			'crop'           => FALSE,
			'crop_x'         => 0,
			'crop_y'         => 0,
			'crop_width'     => 150,
			'crop_height'    => 150,
			'crop_ratio'     => 1,
			'crop_thumbnail' => FALSE,
		];

		$data = array_merge($default_data, $data);

		$img = new ImageEditor($path);

		if ($data['crop_thumbnail'] === TRUE) {
			$img->cropThumbnail($data['crop_width'], $data['crop_height']);

			$data['crop']   = FALSE;
			$data['resize'] = FALSE;
		}

		if ($data['crop'] === TRUE) {
			$img->crop(
				$data['crop_x'] * $data['crop_ratio'],
				$data['crop_y'] * $data['crop_ratio'],
				$data['crop_width'] * $data['crop_ratio'],
				$data['crop_height'] * $data['crop_ratio']
			);
		}

		if ($data['resize'] === TRUE) {
			$img->resizeToFit($data['width'], $data['height'], TRUE);
		}

		_pre('saving to ' . $save_path);

		$img->write($save_path);

		$this->compress_image($save_path);

		return $img;
	}

	public function check_file($file) {
		$file_headers = @get_headers($file);

		if ($file_headers[0] == 'HTTP/1.1 404 Not Found') {
			return FALSE;
		} else {
			return TRUE;
		}
	}

	public function generateSizes($path, $rootdir, $filename, $sizes, $data = []) {
		_pre('generateSizes()');

		if ( ! $this->check_file($path)) {
			_pre('generateSizes() failed! Public path not found: ' . $path);

			return FALSE;
		}

		if (xcount($sizes) <= 0) {
			return FALSE;
		}

		$path_extension = $this->getExtension($path);

		if ($path_extension === FALSE) {
			return FALSE;
		}

		if ($this->getExtension($filename) === FALSE) {
			$filename .= '.' . $path_extension;
		}

		if (substr($rootdir, - 1, 1) != '/') {
			$rootdir .= '/';
		}

		if (substr($rootdir, 0, strlen(public_path())) !== public_path()) {
			$rootdir = public_path() . $rootdir;
		}

		foreach ($sizes as $size => $wh) {
			$sizeDir = $rootdir . strtolower($size);

			if ( ! is_dir($sizeDir)) {
				_pre($sizeDir . ' does not exist, creating...');
				mkdir($sizeDir, 0777, TRUE);
			}

			$data['resize'] = TRUE;
			$data['width']  = $wh[0];
			$data['height'] = $wh[1];

			$this->processImage($path, $sizeDir . '/' . $filename, $data);
		}

		return TRUE;
	}

	private function getExtension($file) {
		if (strpos($file, '.') === FALSE) {
			return FALSE;
		}

		return @end(explode('.', $file));
	}

	public function compress_image($path, $max_quality = 90, $min_quality = 60) {
		if ( ! $this->check_file($path)) {
			_pre('does not exist');

			return FALSE;
		}

		$xpath     = escapeshellarg($path);
		$extension = strtolower($this->getExtension($path));

		switch ($extension) {
			case 'jpg':
			case 'jpeg':
				$cmd = sprintf('jpegoptim -m%s %s 2>&1', $max_quality, $xpath);
				break;

			case 'png':
				//$cmd = sprintf("pngquant --quality=%s-%s - < %s 2>&1", $min_quality, $max_quality, $xpath);
				$cmd = sprintf("pngquant --ext '-new.png' --quality=%s-%s %s", $min_quality, $max_quality, $xpath);
				break;

			case 'gif':

				break;

			case 'bmp':

				break;

			default:
				_pre('unknown extension');

				return FALSE;
		}

		_pre('compressing image: ' . $path . ' with cmd: ' . $cmd);

		$content = '';
		$content = shell_exec($cmd);

		if ($extension == 'png') {
			shell_exec('mv -f ' . escapeshellarg(str_replace('.png', '-new.png', $path)) . ' ' . $xpath);
		}

		if ( ! $content) {
			_pre('empty content');

			return FALSE;
		}

		_pre($content);

		return $content;
	}

	private function compress_png($path_to_png_file, $max_quality = 90) {
		if ( ! file_exists($path_to_png_file)) {
			throw new Exception("File does not exist: $path_to_png_file");
		}

		$min_quality = 60;

		$compressed_png_content = shell_exec("pngquant --quality=$min_quality-$max_quality - < " . escapeshellarg($path_to_png_file));

		if ( ! $compressed_png_content) {
			throw new Exception("Conversion to compressed PNG failed. Is pngquant 1.8+ installed on the server?");
		}

		return $compressed_png_content;
	}

	public function avatars() {
		$users = User::get();

		$sizes = [
			'20'  => [20, 20],
			'30'  => [30, 30],
			'40'  => [40, 40],
			'50'  => [50, 50],
			'75'  => [75, 75],
			'150' => [150, 150],
		];

		$k = 0;

		if (xcount($users) > 0) {
			foreach ($users as $key => $user) {
				if ( ! $user->are_avatar) {
					continue;
				}

				$k ++;

				_pre($user->avatar);
				$x = $this->generateSizes($user->avatar, $user->dir_absolute, $user->user . '.jpg', $sizes);

				if ($x) {
					_pre('generated for ' . $user->avatar);
				} else {
					_pre('   -   failed for ' . $user->avatar);
				}
			}
		}

		_pre('total users: ' . xcount($users));
		_pre('total optimized: ' . $k);
	}

	public function avatarDefault() {
		$sizes = [
			'20'  => [20, 20],
			'30'  => [30, 30],
			'40'  => [40, 40],
			'50'  => [50, 50],
			'150' => [150, 150],
		];

		$sizegen = new SizeGenerator();

		$sizegen->generateSizes(public_path() . '/media/useri/default/default_avatar.jpg', public_path() . '/media/useri/default', 'default_avatar.jpg', $sizes);
	}

	public function forumAttachments() {
		$r = ForumAtasament::limit(10)->join('forum_postari', 'forum_postari.id', '=', 'forum_atasamente.post_id')
		                   ->where('forum_postari.nr_atasamente', '>', 0)->where('forum_atasamente.post_id', 1346511)->get();

		$sizes = [
			'thumbnail' => [96, 96],
		];

		$nr = xcount($r);

		if ($nr > 0) {
			_pre('Processing ' . $nr . ' attachments');

			foreach ($r as $k => $v) {
				_pre('path: ' . $v->url);
				_pre('dir: ' . $v->dir);
				_pre('nume: ' . $v->nume);

				$this->generateSizes($v->thumb, $v->dir, $v->nume, $sizes);
			}
		}

		return;

		$sizes = [
			'20'  => [20, 20],
			'30'  => [30, 30],
			'40'  => [40, 40],
			'50'  => [50, 50],
			'150' => [150, 150],
		];

		$k = 0;

		if (xcount($users) > 0) {
			foreach ($users as $key => $user) {
				if ( ! $user->are_avatar) {
					continue;
				}

				$k ++;

				_pre($user->avatar);
				#$x = $this -> generateSizes($user -> avatar, $user -> dir_absolute, $user -> user . '.jpg', $sizes);

				if ($x) {
					_pre('generated for ' . $user->avatar);
				} else {
					_pre('          -    failed for ' . $user->avatar);
				}
			}
		}

		_pre('total users: ' . xcount($users));
		_pre('total optimized: ' . $k);
	}

	public function articol_main_img() {
		return;
		$r = DB::table('articole')->where('id', 12824)->get();

		$sizes = [
			'300x170' => [300, 170],
			'160x90'  => [160, 90],
		];

		$nr = xcount($r);

		if ($nr > 0) {
			_pre('Processing ' . $nr . ' attachments');

			foreach ($r as $k => $v) {
				_pre('path: ' . $v->url);
				_pre('dir: ' . $v->dir);
				_pre('nume: ' . $v->nume);

				$this->generateSizes($v->thumb, $v->dir, $v->nume, $sizes);
			}
		}
	}
}

?>