<?php

namespace App\Http\Controllers\Auth;

use App\Category;
use App\Http\Controllers\Controller;
use App\Profile;
use App\Settings;
use App\UsersModel;
use Illuminate\Auth\Events\Registered;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class ProfileRegistrationController extends Controller {
	protected $redirectTo = '/dashboard';

	public function __construct() {
		$this->middleware('guest:profile');
	}

	function showStep1() {
		$detect = new \Mobile_Detect();
		$mobile = $detect->isMobile();

		return view('register_step1', compact('mobile'));
	}

	function registerStep1(Request $request) {
		$data      = $request->all();
		$validator = Validator::make($data,
			[
				'name'  => 'required|min:5|max:255',
				'email' => 'required|email|max:255|unique:users_profiles',
				'phone' => 'required|min:9|max:15',
			]);
		if ($validator->fails()) {
			return redirect(route('user.registration'))->withInput()->withErrors($validator);
		} else {
			$user = Profile::create([
				'name'      => $data['name'],
				'name_slug' => slug($data['name']),
				'email'     => $data['email'],
				'phone'     => $data['phone'],
				'status'    => 0,
			]);

			$post['action']  = 'register';
			$post['message'] = 'mesajul';
			$post['url']     = url('/user/register/' . $user->id);

			return view('json', compact('post'));
		}
	}

	function showRegistrationForm($id) {
		$categories = Category::all();
		$user       = UsersModel::findOrFail($id);
		$user       = $user->id;

		$detect = new \Mobile_Detect();
		$mobile = $detect->isMobile();

		return view('registeruser', compact('categories', 'mobile', 'user'));
	}

	function register(Request $request) {
		$validator = Validator::make($request->all(),
			[
				'lat_input' => 'required',
				'lng_input' => 'required',
				'password'  => 'required|min:6|confirmed',
			]);
		if ($validator->fails()) {
			return redirect(route('user.register', ['id' => $request->user_id]))->withInput()->withErrors($validator);
		} else {
			$userOld           = UsersModel::findOrFail($request->user_id);
			$data['category']  = $request->category;
			$data['company']   = $request->company;
			$data['address']   = $request->address;
			$data['city']      = $request->city;
			$data['latitude']  = (float) $request->lat_input;
			$data['longitude'] = (float) $request->lng_input;
			$data['password']  = Hash::make($request->password);
			$data['status']    = 1;

			$user = UsersModel::where('id', $request->user_id)
			                  ->update($data);

			//Send email to user
			$website = Settings::find(1)->value('title');
			$subject = 'Bun venit pe ' . $website;
			$to      = $userOld->name . ' <' . $userOld->email . '>';

			$headers[] = 'MIME-Version: 1.0';
			$headers[] = 'Content-type: text/html; charset=iso-8859-1';
			$headers[] = 'From: Nails Romania <no-reply@makeupromania.ro>';
			$headers[] = 'To: ' . $to;
			$headers[] = 'Bcc: catalin@dinbacau.net, contact@mikhail.ro';

			$msg = 'Salut <b>' . $userOld->name . '</b>,<br /> Bine ai venit pe ' . $website . '. In noul profil iti poti configura portofoliul si datele personale usor si rapid. Grabeste-te, clientii asteapta sa te vada 
pe harta.<br /><br /> Acceseaza profilul tau personal <a href="http://makeupromania.ro/user/login">aici</a>.<br />';
			$msg .= '<b>Date conectare:</b> <br />';
			$msg .= '<b>Utilizator:</b> ' . $userOld->email . '<br />';
			$msg .= '<b>Parola:</b> ' . $request->password;

			mail($to, $subject, $msg, implode("\r\n", $headers));

			$post['action']  = 'register';
			$post['message'] = 'mesajul';
			$post['url']     = url(route('user.login'));

			return view('json', compact('post'));
		}
	}

	/**
	 * Get the guard to be used during registration.
	 *
	 * @return \Illuminate\Contracts\Auth\StatefulGuard
	 */
	protected function guard() {
		return Auth::guard('profile');
	}

	protected function registered(Request $request, $user) {
		//
	}
}
