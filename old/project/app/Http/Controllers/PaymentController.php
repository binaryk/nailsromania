<?php

namespace App\Http\Controllers;

use App\Payment;
use App\Settings;
use App\UsersModel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class PaymentController extends Controller {
	function store(Request $request) {
		$payment  = new Payment;
		$settings = Settings::findOrFail(1);
		$user     = UsersModel::findOrFail($request->userid);

		$paymentold = Payment::where('user_id', $request->userid)
		                     ->where('payment_status', "Pending")
		                     ->where('custom', $request->custom);
		$paymentold->delete();

		$item_name   = 'Premium Profile Feature: ' . ucfirst($request->custom);
		$item_number = str_random(2) . time();
		$item_amount = $settings->normal_price;

		if ($user->status == '1' || $request->custom == 'anual' || $request->custom == 'monthly') {
			$item_amount = 0.00;
		}

		if ($request->banner && $request->banner == 1) {
			switch ($request->custom) {
				case 'banner_week' :
					$item_amount = 30;
					$perioada    = '1 Saptamana';
					break;
				case 'banner_month':
					$item_amount = 80;
					$perioada    = '1 Luna';
					break;
				case 'banner_quarter':
					$item_amount = 170;
					$perioada    = '3 Luni';
					break;
				case 'banner_half':
					$item_amount = 250;
					$perioada    = '6 Luni';
					break;
				case 'banner_year':
					$item_amount = 550;
					$perioada    = '1 An';
					break;
			}
			$item_name = 'Banner PREMIUM pe site pentru ' . $perioada . ' de zile';
		} else {
			$price       = $request->custom . '_price';
			$item_amount = $item_amount + $settings->$price;
		}

		$paypal_email = $settings->paypal_business;
		$return_url   = action('PaymentController@payreturn');
		$cancel_url   = action('PaymentController@paycancle');
		$notify_url   = action('PaymentController@notify');

		$querystring = '';

		// Firstly Append paypal account to querystring
		$querystring .= "?business=" . urlencode($paypal_email) . "&";

		//The item name and amount can be brought in dynamically by querying the $_POST['item_number'] variable.
		$querystring .= "item_name=" . urlencode($item_name) . "&";
		$querystring .= "amount=" . urlencode($item_amount) . "&";
		$querystring .= "item_number=" . urlencode($item_number) . "&";

		$querystring .= "cmd=" . urlencode(stripslashes($request->cmd)) . "&";
		$querystring .= "bn=" . urlencode(stripslashes($request->bn)) . "&";
		$querystring .= "lc=" . urlencode(stripslashes($request->lc)) . "&";
		$querystring .= "currency_code=" . urlencode(stripslashes($request->currency_code)) . "&";

		// Append paypal return addresses
		$querystring .= "return=" . urlencode(stripslashes($return_url)) . "&";
		$querystring .= "cancel_return=" . urlencode(stripslashes($cancel_url)) . "&";
		$querystring .= "notify_url=" . urlencode($notify_url) . "&";

		$querystring .= "custom=" . $request->custom;

		$payment['user_id']        = $request->userid;
		$payment['custom']         = $request->custom;
		$payment['paid_amount']    = $item_amount;
		$payment['method']         = "Paypal";
		$payment['payment_status'] = "Pending";
		$payment['payment_id']     = $item_number;
		$payment['process_time']   = date('Y-m-d H:i:s');
		$payment['query']          = $querystring;
		$payment->save();

		// Redirect to paypal IPN
		$paypal_website = ($settings->paypal_sandbox == 1) ? 'sandbox.paypal' : 'paypal';
		header('location:https://www.' . $paypal_website . '.com/cgi-bin/webscr' . $querystring);
		exit();
	}

	function paycancle() {
		return redirect()->back();
	}

	function payreturn() {
		$detect = new \Mobile_Detect();
		$mobile = $detect->isMobile();

		return view('user.paymentreturn', compact('mobile'));
	}

	function notify(Request $request) {
		$settings       = Settings::findOrFail(1);
		$raw_post_data  = file_get_contents('php://input');
		$raw_post_array = explode('&', $raw_post_data);
		$myPost         = [];
		foreach ($raw_post_array as $keyval) {
			$keyval = explode('=', $keyval);
			if (count($keyval) == 2) {
				$myPost[ $keyval[0] ] = urldecode($keyval[1]);
			}
		}

		// Read the post from PayPal system and add 'cmd'
		$req = 'cmd=_notify-validate';
		if (function_exists('get_magic_quotes_gpc')) {
			$get_magic_quotes_exists = TRUE;
		}
		foreach ($myPost as $key => $value) {
			if ($get_magic_quotes_exists == TRUE && get_magic_quotes_gpc() == 1) {
				$value = urlencode(stripslashes($value));
			} else {
				$value = urlencode($value);
			}
			$req .= "&$key=$value";
		}
		/*
		 * Post IPN data back to PayPal to validate the IPN data is genuine
		 * Without this step anyone can fake IPN data
		 */
		$paypal_website = ($settings->paypal_sandbox == 1) ? 'sandbox.paypal' : 'paypal';
		$paypalURL      = "https://www." . $paypal_website . ".com/cgi-bin/webscr";
		$ch             = curl_init($paypalURL);
		if ($ch == FALSE) {
			return FALSE;
		}
		curl_setopt($ch, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $req);
		curl_setopt($ch, CURLOPT_SSLVERSION, 6);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 1);
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);
		curl_setopt($ch, CURLOPT_FORBID_REUSE, 1);

		// Set TCP timeout to 30 seconds
		curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 30);
		curl_setopt($ch, CURLOPT_HTTPHEADER, ['Connection: Close', 'User-Agent: company-name']);
		$res = curl_exec($ch);

		/*
		 * Inspect IPN validation result and act accordingly
		 * Split response headers and payload, a better way for strcmp
		 */
		$tokens = explode("\r\n\r\n", trim($res));
		$res    = trim(end($tokens));
		$post   = $_POST;
		if (strcmp($res, "VERIFIED") == 0 || strcasecmp($res, "VERIFIED") == 0) {
			$payment                = Payment::where('custom', $post['custom'])->where('payment_id', $post['item_number']);
			$data['txnid']          = $post['txn_id'];
			$data['payment_status'] = $post['payment_status'];
			$data['custom_request'] = serialize($post);
			$period                 = ($post['custom'] == 'anual') ? '1 year' : '1 month';
			$data['date_start']     = date('Y-m-d');
			$data['date_end']       = date('Y-m-d', strtotime('+' . $period));
			$payment->update($data);

			$user_id = $payment->pluck('user_id');
			$feature = ($post['custom'] === 'monthly' || $post['custom'] === 'anual') ? 'status' : trim($post['custom']);
			DB::table('users_profiles')->where('id', $user_id)->update(['featured' => 1, 'map' => 1, 'video' => 1]);
			//DB::table('users_profiles')->where('id', $user_id)->update([$feature => 1]);
		} else {
			$payment = Payment::where('custom', $post['custom'])->where('payment_id', $post['item_number']);
			$payment->delete();
		}
	}
}
