<?php

namespace App\Http\Controllers;

use App\Gallery;
use App\UsersModel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;

class GalleryController extends Controller {
	function __construct() {
		$this->middleware('auth:profile');
	}

	function addImageForm() {
		$user = UsersModel::findOrFail(Auth::user()->id);

		$detect = new \Mobile_Detect();
		$mobile = $detect->isMobile();

		return view('user.upload_image', compact('user', 'mobile'));
	}

	function addVideoForm() {
		$user = UsersModel::findOrFail(Auth::user()->id);

		$detect = new \Mobile_Detect();
		$mobile = $detect->isMobile();

		return view('user.upload_video', compact('user', 'mobile'));
	}

	function addDocForm() {
		$user = UsersModel::findOrFail(Auth::user()->id);

		$detect = new \Mobile_Detect();
		$mobile = $detect->isMobile();

		return view('user.upload_docs', compact('user', 'mobile'));
	}

	function postUpload(Request $request, $type) {
		$user = UsersModel::findOrFail(Auth::user()->id);

		foreach ($request->files as $photo) {
			$photo      = $photo[0];
			$image_name = str_random(2) . time() . '.' . $photo->getClientOriginalExtension();
			$photo->move('assets/images/gallery/', $image_name);

			Gallery::create([
				'image'     => $image_name,
				'userid'    => $user['id'],
				'file_type' => $type,
			]);

			if ($request->type = 'image') {
				$message = 'Galeria foto a fost actualizata cu succes';
			} else {
				$message = 'Fisierul video a fost incarcat cu succes';
			}
		}

		return response()->json(['message' => $message], 200);
	}

	function getDelete($id, $type) {
		$image = Gallery::findOrFail($id);
		$image->delete();

		return redirect('user/gallery/' . $type);
	}
}