<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;

class SettingsController extends Controller {
	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	function __construct() {
		$this->middleware('auth');
	}

	function index() {
		// //
		$setting = DB::select('SELECT * FROM settings WHERE id=?', [1]);

		return view('admin.settings', compact('setting'));
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request $request
	 *
	 * @return \Illuminate\Http\Response
	 */
	function store(Request $request) {
		//return $request->all();
		DB::table('settings')
		  ->where('id', 1)
		  ->update(['title' => $request->title]);
		Session::flash('message', 'Title Updated Successfully.');

		return redirect('admin/settings');
	}

	function title(Request $request) {
		//return $request->all();
		DB::table('settings')
		  ->where('id', 1)
		  ->update(['title' => $request->title]);
		Session::flash('message', 'Title Updated Successfully.');

		return redirect('admin/settings');
	}

	function paymentinfo(Request $request) {
		DB::table('settings')
		  ->where('id', 1)
		  ->update([
			  'paypal_business' => $request->paypal_business,
			  'paypal_sandbox'  => $request->paypal_sandbox,
			  'normal_price'    => $request->normal_price,
			  'featured_price'  => $request->featured_price,
			  'monthly_price'   => $request->monthly_price,
			  'anual_price'     => $request->anual_price,
			  'map_price'       => $request->map_price,
			  'video_price'     => $request->video_price,
			  'profile_images'  => $request->profile_images,
		  ]);

		Session::flash('message', 'Payment Information Updated Successfully.');

		return redirect('admin/settings');
	}

	function about(Request $request) {
		DB::table('settings')
		  ->where('id', 1)
		  ->update(['about' => $request->about]);
		Session::flash('message', 'Despre Noi Text Updated Successfully.');

		return redirect('admin/settings');
	}

	function config(Request $request) {
		DB::table('settings')
		  ->where('id', 1)
		  ->update(['profile_images' => $request->profile_images]);
		Session::flash('message', 'General Configuration Updated Successfully.');

		return redirect('admin/settings');
	}

	function favicon(Request $request) {
		$logo = $request->file('favicon');
		$name = $logo->getClientOriginalName();
		$logo->move('assets/images/', $name);
		DB::table('settings')
		  ->where('id', 1)
		  ->update(['favicon' => $name]);
		Session::flash('message', 'Website Favicon Updated Successfully.');

		return redirect('admin/settings');
	}

	function address(Request $request) {
		//return $request->all();
		DB::table('settings')
		  ->where('id', 1)
		  ->update([
			  'address' => $request->address,
			  'phone'   => $request->phone,
			  'fax'     => $request->fax,
			  'email'   => $request->email,
		  ]);
		Session::flash('message', 'Adresa Updated Successfully.');

		return redirect('admin/settings');
	}

	function footer(Request $request) {
		//return $request->all();
		DB::table('settings')
		  ->where('id', 1)
		  ->update(['footer' => $request->footer]);
		Session::flash('message', 'Footer Updated Successfully.');

		return redirect('admin/settings');
	}

	function logo(Request $request) {
		$logo = $request->file('logo');
		$name = $logo->getClientOriginalName();
		$logo->move('assets/images/logo', $name);
		DB::table('settings')
		  ->where('id', 1)
		  ->update(['logo' => $name]);
		Session::flash('message', 'Website Logo Updated Successfully.');

		return redirect('admin/settings');
	}

	function background(Request $request) {
		//return $request->all();

		///return redirect('admin/settings');
		$logo = $request->file('background');
		$name = $logo->getClientOriginalName();
		$logo->move('assets/images', $name);
		DB::table('settings')
		  ->where('id', 1)
		  ->update(['background' => $name]);
		Session::flash('message', 'Background Image Updated Successfully.');

		return redirect('admin/settings');
	}
}
