<?php

use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;

define('GMAPS_KEY', 'AIzaSyAAtWVTo07CRSuT1_2U5fLpWqDxxqETvsw');
define('FB_APPID', '273680106445144');

function pre($var, $exit = FALSE) {
	echo '<pre style="font-size:11px;">';

	if (is_array($var) || is_object($var)) {
		echo htmlentities(print_r($var, TRUE));
	} elseif (is_string($var)) {
		echo "string(" . strlen($var) . ") \"" . htmlentities($var) . "\"\n";
	} else {
		var_dump($var);
	}

	echo "\n</pre>";

	if ($exit) {
		exit;
	}
}

function _pre($data) {
	include_once("Controllers/helpers/dBug.php");
	new dBug($data);
}

function slug($string, $space = "-") {
	$string = removeAccents($string);
	//$string = normalizeString($string);

	$string = str_replace("&", "si", $string);

	// Stergem spatiile de la capete
	$string = trim($string);

	$string = preg_replace("/[^a-zA-Z0-9 -]/", "", $string);
	$string = strtolower($string);

	$string = str_replace(" ", $space, $string);

	// Stergem spatiile duble
	$string = preg_replace("/$space+/", "$space", $string);

	return $string;
}

function removeUnwantedSpace($string) {
	$string = preg_replace('#\v+#', '', $string);
	$string = preg_replace('#\t#', '', $string);
	$string = preg_replace('#&nbsp;#', ' ', $string);
	$string = preg_replace("#\s\s+#", " ", $string);
	$string = trim(ltrim(rtrim($string)));

	return $string;
}

function normalizeName($name, $glue = '-') {
	return ucwords(implode($glue, array_map('ucfirst', explode($glue, strtolower(removeUnwantedSpace($name))))));
}

function lastQuery() {
	$query = DB::getQueryLog();

	return end($query);
}

function removeAccents($string) {
	$replace = [
		'ъ' => '-',
		'Ь' => '-',
		'Ъ' => '-',
		'ь' => '-',
		'Ă' => 'A',
		'Ą' => 'A',
		'À' => 'A',
		'Ã' => 'A',
		'Á' => 'A',
		'Æ' => 'A',
		'Â' => 'A',
		'Å' => 'A',
		'Ä' => 'Ae',
		'Þ' => 'B',
		'Ć' => 'C',
		'ץ' => 'C',
		'Ç' => 'C',
		'È' => 'E',
		'Ę' => 'E',
		'É' => 'E',
		'Ë' => 'E',
		'Ê' => 'E',
		'Ğ' => 'G',
		'İ' => 'I',
		'Ï' => 'I',
		'Î' => 'I',
		'Í' => 'I',
		'Ì' => 'I',
		'Ł' => 'L',
		'Ñ' => 'N',
		'Ń' => 'N',
		'Ø' => 'O',
		'Ó' => 'O',
		'Ò' => 'O',
		'Ô' => 'O',
		'Õ' => 'O',
		'Ö' => 'Oe',
		'Ş' => 'S',
		'Ś' => 'S',
		'Ș' => 'S',
		'Š' => 'S',
		'Ț' => 'T',
		'Ù' => 'U',
		'Û' => 'U',
		'Ú' => 'U',
		'Ü' => 'Ue',
		'Ý' => 'Y',
		'Ź' => 'Z',
		'Ž' => 'Z',
		'Ż' => 'Z',
		'â' => 'a',
		'ǎ' => 'a',
		'ą' => 'a',
		'á' => 'a',
		'ă' => 'a',
		'ã' => 'a',
		'Ǎ' => 'a',
		'а' => 'a',
		'А' => 'a',
		'å' => 'a',
		'à' => 'a',
		'א' => 'a',
		'Ǻ' => 'a',
		'Ā' => 'a',
		'ǻ' => 'a',
		'ā' => 'a',
		'ä' => 'ae',
		'æ' => 'ae',
		'Ǽ' => 'ae',
		'ǽ' => 'ae',
		'б' => 'b',
		'ב' => 'b',
		'Б' => 'b',
		'þ' => 'b',
		'ĉ' => 'c',
		'Ĉ' => 'c',
		'Ċ' => 'c',
		'ć' => 'c',
		'ç' => 'c',
		'ц' => 'c',
		'צ' => 'c',
		'ċ' => 'c',
		'Ц' => 'c',
		'Č' => 'c',
		'č' => 'c',
		'Ч' => 'ch',
		'ч' => 'ch',
		'ד' => 'd',
		'ď' => 'd',
		'Đ' => 'd',
		'Ď' => 'd',
		'đ' => 'd',
		'д' => 'd',
		'Д' => 'd',
		'ð' => 'd',
		'є' => 'e',
		'ע' => 'e',
		'е' => 'e',
		'Е' => 'e',
		'Ə' => 'e',
		'ę' => 'e',
		'ĕ' => 'e',
		'ē' => 'e',
		'Ē' => 'e',
		'Ė' => 'e',
		'ė' => 'e',
		'ě' => 'e',
		'Ě' => 'e',
		'Є' => 'e',
		'Ĕ' => 'e',
		'ê' => 'e',
		'ə' => 'e',
		'è' => 'e',
		'ë' => 'e',
		'é' => 'e',
		'ф' => 'f',
		'ƒ' => 'f',
		'Ф' => 'f',
		'ġ' => 'g',
		'Ģ' => 'g',
		'Ġ' => 'g',
		'Ĝ' => 'g',
		'Г' => 'g',
		'г' => 'g',
		'ĝ' => 'g',
		'ğ' => 'g',
		'ג' => 'g',
		'Ґ' => 'g',
		'ґ' => 'g',
		'ģ' => 'g',
		'ח' => 'h',
		'ħ' => 'h',
		'Х' => 'h',
		'Ħ' => 'h',
		'Ĥ' => 'h',
		'ĥ' => 'h',
		'х' => 'h',
		'ה' => 'h',
		'î' => 'i',
		'ï' => 'i',
		'í' => 'i',
		'ì' => 'i',
		'į' => 'i',
		'ĭ' => 'i',
		'ı' => 'i',
		'Ĭ' => 'i',
		'И' => 'i',
		'ĩ' => 'i',
		'ǐ' => 'i',
		'Ĩ' => 'i',
		'Ǐ' => 'i',
		'и' => 'i',
		'Į' => 'i',
		'י' => 'i',
		'Ї' => 'i',
		'Ī' => 'i',
		'І' => 'i',
		'ї' => 'i',
		'і' => 'i',
		'ī' => 'i',
		'ĳ' => 'ij',
		'Ĳ' => 'ij',
		'й' => 'j',
		'Й' => 'j',
		'Ĵ' => 'j',
		'ĵ' => 'j',
		'я' => 'ja',
		'Я' => 'ja',
		'Э' => 'je',
		'э' => 'je',
		'ё' => 'jo',
		'Ё' => 'jo',
		'ю' => 'ju',
		'Ю' => 'ju',
		'ĸ' => 'k',
		'כ' => 'k',
		'Ķ' => 'k',
		'К' => 'k',
		'к' => 'k',
		'ķ' => 'k',
		'ך' => 'k',
		'Ŀ' => 'l',
		'ŀ' => 'l',
		'Л' => 'l',
		'ł' => 'l',
		'ļ' => 'l',
		'ĺ' => 'l',
		'Ĺ' => 'l',
		'Ļ' => 'l',
		'л' => 'l',
		'Ľ' => 'l',
		'ľ' => 'l',
		'ל' => 'l',
		'מ' => 'm',
		'М' => 'm',
		'ם' => 'm',
		'м' => 'm',
		'ñ' => 'n',
		'н' => 'n',
		'Ņ' => 'n',
		'ן' => 'n',
		'ŋ' => 'n',
		'נ' => 'n',
		'Н' => 'n',
		'ń' => 'n',
		'Ŋ' => 'n',
		'ņ' => 'n',
		'ŉ' => 'n',
		'Ň' => 'n',
		'ň' => 'n',
		'о' => 'o',
		'О' => 'o',
		'ő' => 'o',
		'õ' => 'o',
		'ô' => 'o',
		'Ő' => 'o',
		'ŏ' => 'o',
		'Ŏ' => 'o',
		'Ō' => 'o',
		'ō' => 'o',
		'ø' => 'o',
		'ǿ' => 'o',
		'ǒ' => 'o',
		'ò' => 'o',
		'Ǿ' => 'o',
		'Ǒ' => 'o',
		'ơ' => 'o',
		'ó' => 'o',
		'Ơ' => 'o',
		'œ' => 'oe',
		'Œ' => 'oe',
		'ö' => 'oe',
		'פ' => 'p',
		'ף' => 'p',
		'п' => 'p',
		'П' => 'p',
		'ק' => 'q',
		'ŕ' => 'r',
		'ř' => 'r',
		'Ř' => 'r',
		'ŗ' => 'r',
		'Ŗ' => 'r',
		'ר' => 'r',
		'Ŕ' => 'r',
		'Р' => 'r',
		'р' => 'r',
		'ș' => 's',
		'с' => 's',
		'Ŝ' => 's',
		'š' => 's',
		'ś' => 's',
		'ס' => 's',
		'ş' => 's',
		'С' => 's',
		'ŝ' => 's',
		'Щ' => 'sch',
		'щ' => 'sch',
		'ш' => 'sh',
		'Ш' => 'sh',
		'ß' => 'ss',
		'т' => 't',
		'ט' => 't',
		'ŧ' => 't',
		'ת' => 't',
		'ť' => 't',
		'ţ' => 't',
		'Ţ' => 't',
		'Т' => 't',
		'ț' => 't',
		'Ŧ' => 't',
		'Ť' => 't',
		'™' => 'tm',
		'ū' => 'u',
		'у' => 'u',
		'Ũ' => 'u',
		'ũ' => 'u',
		'Ư' => 'u',
		'ư' => 'u',
		'Ū' => 'u',
		'Ǔ' => 'u',
		'ų' => 'u',
		'Ų' => 'u',
		'ŭ' => 'u',
		'Ŭ' => 'u',
		'Ů' => 'u',
		'ů' => 'u',
		'ű' => 'u',
		'Ű' => 'u',
		'Ǖ' => 'u',
		'ǔ' => 'u',
		'Ǜ' => 'u',
		'ù' => 'u',
		'ú' => 'u',
		'û' => 'u',
		'У' => 'u',
		'ǚ' => 'u',
		'ǜ' => 'u',
		'Ǚ' => 'u',
		'Ǘ' => 'u',
		'ǖ' => 'u',
		'ǘ' => 'u',
		'ü' => 'ue',
		'в' => 'v',
		'ו' => 'v',
		'В' => 'v',
		'ש' => 'w',
		'ŵ' => 'w',
		'Ŵ' => 'w',
		'ы' => 'y',
		'ŷ' => 'y',
		'ý' => 'y',
		'ÿ' => 'y',
		'Ÿ' => 'y',
		'Ŷ' => 'y',
		'Ы' => 'y',
		'ž' => 'z',
		'З' => 'z',
		'з' => 'z',
		'ź' => 'z',
		'ז' => 'z',
		'ż' => 'z',
		'ſ' => 'z',
		'Ж' => 'zh',
		'ж' => 'zh',
	];

	return strtr($string, $replace);
}

function imageUrl($path, $width = NULL, $height = NULL, $quality = 75, $crop = 1) {
	if ( ! $width && ! $height) {
		$url = env('IMAGE_URL') . $path;
	} else {
		$url = url('/') . '/timthumb.php?src=' . env('IMAGE_URL') . $path;
		if (isset($width)) {
			$url .= '&w=' . $width;
		}
		if (isset($height) && $height > 0) {
			$url .= '&h=' . $height;
		}
		if (isset($crop)) {
			$url .= "&zc=" . $crop;
		}
		if (isset($quality)) {
			$url .= '&q=' . $quality . '&s=1';
		} else {
			$url .= '&q=95&s=1';
		}
	}

	return $url;
}

function getObfuscatedEmailAddress($email) {
	$alwaysEncode = ['.', ':', '@'];

	$result = '';

	// Encode string using oct and hex character codes
	for ($i = 0; $i < strlen($email); $i ++) {
		// Encode 25% of characters including several that always should be encoded
		if (in_array($email[ $i ], $alwaysEncode) || mt_rand(1, 100) < 25) {
			if (mt_rand(0, 1)) {
				$result .= '&#' . ord($email[ $i ]) . ';';
			} else {
				$result .= '&#x' . dechex(ord($email[ $i ])) . ';';
			}
		} else {
			$result .= $email[ $i ];
		}
	}

	return $result;
}

function obfuscateEmail($email, $params = []) {
	if ( ! is_array($params)) {
		$params = [];
	}

	// Tell search engines to ignore obfuscated uri
	if ( ! isset($params['rel'])) {
		$params['rel'] = 'nofollow';
	}

	$neverEncode = ['.', '@', '+']; // Don't encode those as not fully supported by IE & Chrome

	$urlEncodedEmail = '';
	for ($i = 0; $i < strlen($email); $i ++) {
		// Encode 25% of characters
		if ( ! in_array($email[ $i ], $neverEncode) && mt_rand(1, 100) < 25) {
			$charCode        = ord($email[ $i ]);
			$urlEncodedEmail .= '%';
			$urlEncodedEmail .= dechex(($charCode >> 4) & 0xF);
			$urlEncodedEmail .= dechex($charCode & 0xF);
		} else {
			$urlEncodedEmail .= $email[ $i ];
		}
	}

	$obfuscatedEmail    = getObfuscatedEmailAddress($email);
	$obfuscatedEmailUrl = getObfuscatedEmailAddress('mailto:' . $urlEncodedEmail);

	$link = '<a href="' . $obfuscatedEmailUrl . '"';
	foreach ($params as $param => $value) {
		$link .= ' ' . $param . '="' . htmlspecialchars($value) . '"';
	}
	$link .= '>' . $obfuscatedEmail . '</a>';

	return $link;
}