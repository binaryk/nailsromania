

//Preloader

$(window).load(function() {
    $("#status").fadeOut();
    $("#preloader").delay(350).fadeOut("slow");
})




//Home fit screen


/*global $:false */
$(function(){"use strict";
    $('#home').css({'height':($(window).height())+'px'});
    $(window).resize(function(){
        $('#home').css({'height':($(window).height())+'px'});
    });

//Home typer

    $('[data-typer-targets]').typer();

//Home Background Slider

    $.mbBgndGallery.buildGallery({
        containment:"#home",
        timer:2000,
        effTimer:4000,
        controls:"#controls",
        grayScale:false,
        shuffle:true,
        preserveWidth:false,
        effect:"zoom",

        images:[
            "/assets/css/images/1.jpg",
            "/assets/css/images/2.jpg",
            "/assets/css/images/3.jpg"
        ],

        onStart:function(){},
        onPause:function(){},
        onPlay:function(opt){},
        onChange:function(opt,idx){},
        onNext:function(opt){},
        onPrev:function(opt){}
    });



});

//Scrolling

$(document).ready(function(){
    $(".main").onepage_scroll({
        sectionContainer: "section",
        responsiveFallback: 600
    });
});
