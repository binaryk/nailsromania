<?php

namespace App\Http\Controllers\Auth;

use App\Category;
use App\Http\Controllers\Controller;
use App\Profile;
use App\Settings;
use App\UsersModel;
use Illuminate\Auth\Events\Registered;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class ProfileRegistrationController extends Controller {
	protected $redirectTo = '/dashboard';

	public function __construct() {
		$this->middleware('guest:profile');
	}

    function step0() {
        $detect = new \Mobile_Detect();
        $mobile = $detect->isMobile();

        return view('registration.step-0', compact('mobile'));
    }

    function step1() {
        $detect = new \Mobile_Detect();
        $mobile = $detect->isMobile();

        return view('registration.step-1', compact('mobile'));
    }

	function showStep1() {
		$detect = new \Mobile_Detect();
		$mobile = $detect->isMobile();

		return view('registration.registration', compact('mobile'));
	}

	function register(Request $request) {
		$data      = $request->all();
		$validator = Validator::make($data,
			[
				'name'     => 'required|min:5|max:255',
				'email'    => 'required|email|max:255|unique:users_profiles',
				'phone'    => 'required|min:9|max:15',
				'password' => 'required|min:6',
			]);
		if ($validator->fails()) {
			return redirect(route('user.registration'))->withInput()->withErrors($validator);
		} else {
			$user = Profile::create([
				'name'      => normalizeName($data['name']),
				'name_slug' => slug($data['name']),
				'email'     => $data['email'],
				'city'     => $data['city'],
				'phone'     => $data['phone'],
				'password'  => Hash::make($request->password),
				'status'    => 1,
			]);

			//Send email to user
			$website = Settings::find(1)->value('title');
			$subject = 'Bun venit pe ' . $website;
			$to      = $user->name . ' <' . $user->email . '>';

			$headers[] = 'MIME-Version: 1.0';
			$headers[] = 'Content-type: text/html; charset=iso-8859-1';
			$headers[] = 'From: Makeup Romania <no-reply@makeupromania.ro>';
			$headers[] = 'To: ' . $to;
			$headers[] = 'Bcc: catalin@dinbacau.net, support@makeupromania.ro';

			$msg = 'Salut <b>' . $user->name . '</b>,<br /> Bine ai venit pe ' . $website . '. In noul profil iti poti configura portofoliul si datele personale usor si rapid. Grabeste-te, clientii asteapta sa te vada 
pe harta.<br /><br /> Acceseaza profilul tau personal <a href="http://makeupromania.ro/user/login">aici</a>.<br />';
			$msg .= '<b>Date conectare:</b> <br />';
			$msg .= '<b>Utilizator:</b> ' . $user->email . '<br />';
			$msg .= '<b>Parola:</b> ' . $request->password;

			mail($to, $subject, $msg, implode("\r\n", $headers));

			event(new Registered($user));

			$this->guard()->login($user);

			return $this->registered($request, $user) ?: redirect()->route('user.dashboard', ['new_user' => $user]);
		}
	}

	function showRegistrationForm($id) {
		$categories = Category::all();
		$user       = UsersModel::findOrFail($id);
		$user       = $user->id;

		$detect = new \Mobile_Detect();
		$mobile = $detect->isMobile();

		return view('registeruser', compact('categories', 'mobile', 'user'));
	}

	/**
	 * Get the guard to be used during registration.
	 *
	 * @return \Illuminate\Contracts\Auth\StatefulGuard
	 */
	protected function guard() {
		return Auth::guard('profile');
	}

	protected function registered(Request $request, $user) {
		//
	}
}
