<?php

namespace App\Http\Controllers\Auth;

use App\Profile;
use Exception;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Laravel\Socialite\Facades\Socialite;

class SocialAuthController extends Controller {
	function __construct() {
		$this->middleware('guest:profile');
	}

	/**
	 * List of providers configured in config/services acts as whitelist
	 *
	 * @var array
	 */
	protected $providers
		= [
			'facebook',
			'google',
			'twitter',
		];

	/**
	 * Show the social login page
	 *
	 * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
	 */
	function show() {
		$detect = new \Mobile_Detect();
		$mobile = $detect->isMobile();

		return view('auth.social', compact('mobile'));
	}

	/**
	 * Redirect to provider for authentication
	 *
	 * @param $driver
	 *
	 * @return mixed
	 */
	function redirectToProvider($driver) {
		if ( ! $this->isProviderAllowed($driver)) {
			return $this->sendFailedResponse("{$driver} is not currently supported");
		}

		try {
			return Socialite::driver($driver)->redirect();
		} catch (Exception $e) {
			// You should show something simple fail message
			return $this->sendFailedResponse($e->getMessage());
		}
	}

	function redirectToProviderMember($driver) {
	    // settings
        $this->updateConfig();
        if ( ! $this->isProviderAllowed($driver)) {
            return $this->sendFailedResponse("{$driver} is not currently supported");
        }

        try {
            return Socialite::driver($driver)->redirect();
        } catch (Exception $e) {
            // You should show something simple fail message
            return $this->sendFailedResponse($e->getMessage());
        }
    }

	/**
	 * Handle response of authentication redirect callback
	 *
	 * @param $driver
	 *
	 * @return \Illuminate\Http\RedirectResponse
	 */
	function handleProviderCallback($driver) {
		try {
			$user = Socialite::driver($driver)->user();
		} catch (Exception $e) {
			return $this->sendFailedResponse($e->getMessage());
		}

		// check for email in returned user
		return empty($user->email)
			? $this->sendFailedResponse("No email id returned from {$driver} provider.")
			: $this->loginOrCreateAccount($user, $driver, false);
	}

	function handleProviderCallbackMember($driver) {
	    $this->updateConfig();

        try {
            $user = Socialite::driver($driver)->user();
        } catch (Exception $e) {
            return $this->sendFailedResponse($e->getMessage());
        }

        // check for email in returned user
        return empty($user->email)
            ? $this->sendFailedResponse("No email id returned from {$driver} provider.")
            : $this->loginOrCreateAccount($user, $driver, true);
    }

	/**
	 * Send a successful response
	 *
	 * @return \Illuminate\Http\RedirectResponse
	 */
	protected function sendSuccessResponse() {
		return redirect()->intended(route('user.dashboard'));
	}

	/**
	 * Send a failed response with a msg
	 *
	 * @param null $msg
	 *
	 * @return \Illuminate\Http\RedirectResponse
	 */
	protected function sendFailedResponse($msg = NULL) {
		return redirect()->route('social.login')
		                 ->withErrors(['msg' => $msg ?: 'Unable to login, try with another provider to login.']);
	}

	protected function loginOrCreateAccount($providerUser, $driver, $isMember = false) {
		// check for already has account
		$user = Profile::where('email', $providerUser->getEmail())->first();

		// if user already found
		if ($user) {
			// update the avatar and provider that might have changed
			$user->update([
				'photo'        => $providerUser->avatar . '&type=large',
				'provider'     => $driver,
				'provider_id'  => $providerUser->id,
				'access_token' => $providerUser->token,
                'member' => $isMember ? 1 : 0
            ]);
		} else {
			// create a new user
			$user = Profile::create([
				'name'         => $providerUser->getName(),
				'name_slug'    => slug($providerUser->getName()),
				'email'        => $providerUser->getEmail(),
				'photo'        => $providerUser->getAvatar() . '&type=large',
				'provider'     => $driver,
				'provider_id'  => $providerUser->getId(),
				'access_token' => $providerUser->token,
				'password'     => '',
				'status'       => 1,
                'member' => $isMember ? 1 : 0
			]);
		}
		$user = Profile::where('provider', $driver)->where('provider_id', $providerUser->id)->first();
//		dd($user);

		// login the user
	    Auth::guard('profile')->login($user);
        $redirect = $isMember ? route('user.profile.edit.member') : route('user.dashboard');
        return redirect()->intended($redirect);
	}

	/**
	 * Check for provider allowed and services configured
	 *
	 * @param $driver
	 *
	 * @return bool
	 */
	private function isProviderAllowed($driver) {
		return in_array($driver, $this->providers) && config()->has("services.{$driver}");
	}

	protected function guard() {
		return Auth::guard('profile');
	}

    public function updateConfig()
    {
        $client_id = env('FB_ID_M');
        $client_secret = env('FB_SECRET_M');
        $redirect = env('APP_URL') . '/oauth/facebook/callback-member';
        \Config::set('services.facebook.client_id', $client_id );
        \Config::set('services.facebook.client_secret', $client_secret );
        \Config::set('services.facebook.redirect', $redirect );
	}
}