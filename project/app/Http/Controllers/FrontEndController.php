<?php

namespace App\Http\Controllers;

use App\Category;
use App\Payment;
use App\Gallery;
use App\PageSettings;
use App\Review;
use App\Subscribers;
use App\Settings;
use App\UsersModel;
use Illuminate\Support\Facades\Session;
use Illuminate\Http\Request;
use Input;
use DB;

class FrontEndController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    function index()
    {
        $cities = UsersModel::distinct()
            ->whereNotNull('city')
            ->get(['city']);
        $categories = Category::all();
        $featured = UsersModel::where('featured', 1)
            ->where('status', 1)
            ->orderByRaw("RAND()")
            ->get();
        $allUsers = UsersModel::where('status', 1)
            ->where('featured', 0)
            ->orderByRaw('RAND()')
            ->get();
        $lastPayer = Payment::where('payment_status', 'Completed')
            ->orderBy('id', 'desc')->first();

        $mapMarkers = $this->getMapMarkers();

        $detect = new \Mobile_Detect();
        $mobile = $detect->isMobile();
        if ( ! isset($_COOKIE['makeup_first'])) {
            setcookie("makeup_first", "no", strtotime('+30 days'));
            $cookie = false;
        } else {
            $cookie = true;
        }

        if ($mobile && ! $cookie) {
            return view('mobile');
        } else {
            return view('index',
                compact('featured', 'categories', 'cities', 'mapMarkers', 'allUsers', 'mobile', 'lastPayer'));
        }
    }

    function updateUserSlugs()
    {
        $users = UsersModel::whereNull('name_slug')->get();
        if (count($users) > 0) {
            foreach ($users as $user) {
                $slug = slug($user->name);
                UsersModel::where('id', $user->id)
                    ->update(['name_slug' => $slug]);
            }
        }
    }

    function welcome()
    {
        return view('welcome');
    }

    function mobile()
    {
        return view('mobile');
    }

    //Profile Data
    function viewprofile($id)
    {
        $profiledata = UsersModel::findOrFail($id);
        $reviews = Review::where('userid', $id)->get();
        $gallerydata = Gallery::where('userid', $id)->where('file_type', 'image');
        $docsdata = Gallery::where('userid', $id)->where('file_type', 'document')->get();
        $videos = Gallery::where('userid', $id)->where('file_type', 'video')->first();
        $limit = Settings::find(1)->value('profile_images');
        if ($profiledata['featured'] != 1) {
            $gallerydata->take($limit);
        }
        $gallerydata = $gallerydata->get();

        $detect = new \Mobile_Detect();
        $mobile = $detect->isMobile();

        return view('profile', compact('profiledata', 'reviews', 'gallerydata', 'docsdata', 'videos', 'mobile'));
    }

    function map_query(Request $request)
    {
        $post = $request->all();
        if ($request->action == 'get_location') {
            $result = UsersModel::findOrFail($request->post_id);
        } elseif ($request->action == 'locations') {
            $result = $this->getMapMarkers();
        }

        return view('json', compact('post', 'result'));
    }

    //Contact Page Data
    function contact()
    {
        $pagedata = PageSettings::find(1);

        $detect = new \Mobile_Detect();
        $mobile = $detect->isMobile();

        return view('contact', compact('pagedata', 'mobile'));
    }

    //About Page Data
    function about()
    {
        $pagedata = PageSettings::find(1);

        $detect = new \Mobile_Detect();
        $mobile = $detect->isMobile();

        return view('about', compact('pagedata', 'mobile'));
    }

    //FAQ Page Data
    function faq()
    {
        $pagedata = PageSettings::find(1);

        $detect = new \Mobile_Detect();
        $mobile = $detect->isMobile();

        return view('faq', compact('pagedata', 'mobile'));
    }

    //Show All Users
    function all()
    {
        $cities = UsersModel::distinct()
            ->whereNotNull('city')
            ->get(['city']);
        $categories = Category::all();
        $allusers = UsersModel::where('status', 1)
            ->orderByRaw("RAND()")
            ->get();
        $pagename = "Toti Make-up Artistii";

        $mapMarkers = $this->getMapMarkers();

        $detect = new \Mobile_Detect();
        $mobile = $detect->isMobile();

        return view('listall', compact('allusers', 'pagename', 'categories', 'cities', 'mapMarkers', 'mobile'));
    }

    //Show Featured Users
    function featured()
    {
        $cities = UsersModel::distinct()
            ->whereNotNull('city')
            ->get(['city']);
        $categories = Category::all();
        $allusers = UsersModel::where('featured', 1)
            ->where('status', 1)
            ->orderByRaw("RAND()")
            ->get();
        $pagename = "Make-up Artisti PREMIUM";

        $mapMarkers = $this->getMapMarkers(null, null, true);

        $detect = new \Mobile_Detect();
        $mobile = $detect->isMobile();

        return view('listall', compact('allusers', 'pagename', 'categories', 'cities', 'mapMarkers', 'mobile'));
    }

    //Show Featured Users
    function members()
    {
        $cities = UsersModel::distinct()
            ->whereNotNull('city')
            ->get(['city']);
        $categories = Category::all();
        $allusers = UsersModel::where('member', 1)
            ->where('status', 1)
            ->orderByRaw("RAND()")
            ->get();
        $pagename = "Membri";

        $mapMarkers = $this->getMapMarkers(null, null, true);

        $detect = new \Mobile_Detect();
        $mobile = $detect->isMobile();

        return view('listall', compact('allusers', 'pagename', 'categories', 'cities', 'mapMarkers', 'mobile'));
    }

    //Show Featured Users
    function starred()
    {
        $cities = UsersModel::distinct()
            ->whereNotNull('city')
            ->get(['city']);
        $ratings = Review::distinct(['userid'])
            ->get(['userid']);
        $categories = Category::all();
        $allusers = UsersModel::whereIn('id', $ratings)
            ->where('status', 1)
            ->orderByRaw("RAND()")
            ->get();
        $pagename = "Make-up Artisti recomandati de clienti";

        $mapMarkers = $this->getMapMarkers(null, null, false, $ratings);

        $detect = new \Mobile_Detect();
        $mobile = $detect->isMobile();

        return view('listall', compact('allusers', 'pagename', 'categories', 'cities', 'mapMarkers', 'mobile'));
    }

    //Show Category Users
    function category($category)
    {
        $cities = UsersModel::distinct()
            ->whereNotNull('city')
            ->get(['city']);
        $categories = Category::all();
        $allusers = UsersModel::where('status', 1)
            ->where('category', $category)
            ->get();
        $pagename = "Toti Make-up Artistii in: " . ucwords($category);
        $mapMarkers = $this->getMapMarkers($category);

        $detect = new \Mobile_Detect();
        $mobile = $detect->isMobile();

        return view('listall', compact('allusers', 'pagename', 'categories', 'cities', 'mapMarkers', 'mobile'));
    }

    //Submit Review
    function reviewsubmit(Request $request)
    {
        $review = new Review;
        $review->fill($request->all());
        $review['review_date'] = date('Y-m-d H:i:s');
        $review->save();

        return redirect()->back()->with('message', 'Your Review Submitted Successfully.');
    }

    //Show Searched Users
    function search(Request $request)
    {
        $cities = UsersModel::distinct()
            ->whereNotNull('city')
            ->get(['city']);
        $categories = Category::all();
        $category = $request->category;
        $city = $request->city;
        $starred = $request->starred;
        $ratings = null;
        if ($starred) {
            $ratings = Review::distinct(['userid'])
                ->get(['userid']);

            $allusers = UsersModel::where('status', 1)
                ->when($category,
                    function ($query) use ($category) {
                        return $query->where('category', $category);
                    })
                ->when($city,
                    function ($query) use ($city) {
                        return $query->where('city', $city);
                    })
                ->when($starred,
                    function ($query) use ($ratings) {
                        return $query->whereIn('id', $ratings);
                    })
                ->get();
        } else {
            $allusers = UsersModel::where('status', 1)
                ->when($category,
                    function ($query) use ($category) {
                        return $query->where('category', $category);
                    })
                ->when($city,
                    function ($query) use ($city) {
                        return $query->where('city', $city);
                    })
                ->get();
        }

        $pagename = "Am gasit " . count($allusers);
        $pagename .= (count($allusers) == 1) ? " rezultat" : " rezultate";
        $pagename .= ($category) ? 'Pentru: ' . $category : ' ';
        $pagename .= ($city) ? 'in ' . $city : '';

        $mapMarkers = $this->getMapMarkers($category, $city, false, $ratings);

        $detect = new \Mobile_Detect();
        $mobile = $detect->isMobile();

        return view('listall', compact('allusers', 'pagename', 'categories', 'cities', 'mapMarkers', 'mobile'));
    }

    //User Subscription
    function subscribe(Request $request)
    {
        $subscribe = new Subscribers;
        $subscribe->fill($request->all());
        $subscribe->save();
        Session::flash('subscribe', 'Felicitari, te-ai abonat cu succes');

        return redirect('/');
    }

    //Send email to user
    function usermail(Request $request)
    {
        $website = Settings::find(1)->value('title');
        $subject = 'Mesaj nou de pe ' . $website;
        $to = normalizeName($request->to_name) . ' <' . $request->to . '>';
        $from = $request->name . ' <' . $request->email . '>';

        $headers[] = 'MIME-Version: 1.0';
        $headers[] = 'Content-type: text/html; charset=iso-8859-1';
        $headers[] = 'From: ' . $from;
        $headers[] = 'To: ' . $to;
        $headers[] = 'Bcc: catalin@dinbacau.net, contact@mikhail.ro';

        $msg = 'Salut <b>' . $request->to_name . '</b>,<br /> ai primit o noua comanda pe ' . $website . ' de la un client cu numele <b>' . $request->name
            . '</b>. Ai mai jos informatii si datele de contact pentru a putea lua legatura mai usor. Iti dorim o colaborare placuta.<br /><br />';
        $msg .= '<b>Email:</b> ' . $request->email . '<br />';
        $msg .= '<b>Telefon:</b> ' . $request->phone . '<br />';
        $msg .= '<b>Cerere programare:</b> ' . $request->programare . '<br />';
        $msg .= '<b>Mesaj:</b> ' . $request->message;

        mail($to, $subject, $msg, implode("\r\n", $headers));

        return redirect('/');
    }

    //Send email to Admin
    function contactmail(Request $request)
    {
        $pagedata = PageSettings::findOrFail(1);
        $subject = "Contact From Of Handymen";
        $to = $request->to;
        $name = $request->name;
        $from = $request->email;
        $msg = $request->message;

        mail($to, $subject, $msg);

        Session::flash('cmail', $pagedata->contact);

        return redirect('/contact');
    }

    function getMapMarkers($category = null, $city = null, $featured = false, $rating = null)
    {
        $results = UsersModel::where('status', 1)
            ->whereNotNull('city')
            ->whereNotNull('latitude')
            ->whereNotNull('longitude')
            ->when($featured,
                function ($query) use ($featured) {
                    return $query->where('featured', 1);
                })
            ->when($category,
                function ($query) use ($category) {
                    return $query->where('category', $category);
                })
            ->when($city,
                function ($query) use ($city) {
                    return $query->where('city', $city);
                })
            ->when($rating,
                function ($query) use ($rating) {
                    return $query->whereIn('id', $rating);
                })
            ->get();

        return $results;
    }

    public function totals()
    {
        $nails = UsersModel::count();
        $makeup = DB::connection('makeup')->table('users_profiles')->count();
        $hair = DB::connection('hair')->table('users_profiles')->count();
        $cosmetics = DB::connection('cosmetics')->table('users_profiles')->count();
        $models = DB::connection('models')->table('users_profiles')->count();
        return view('total', ['total' => $nails + $makeup + $hair + $cosmetics + $models]);
    }
}
