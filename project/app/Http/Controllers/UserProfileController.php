<?php

namespace App\Http\Controllers;

use App\Category;
use App\Profile;
use App\Gallery;
use App\Settings;
use App\UsersModel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;

class UserProfileController extends Controller {
	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */

	function __construct() {
		$this->middleware('auth:profile');
	}

	/**
	 * Show the application dashboard.
	 *
	 * @return \Illuminate\Http\Response
	 */
	function index() {
		$user        = UsersModel::findOrFail(Auth::user()->id);
		$settings    = Settings::findOrFail(1);
		$gallerydata = Gallery::where('userid', $user['id'])->where('file_type', 'image')->get();
		$videodata   = Gallery::where('userid', $user['id'])->where('file_type', 'video')->get();

		$detect = new \Mobile_Detect();
		$mobile = $detect->isMobile();

		return view('user.dashboard', compact('user', 'settings', 'gallerydata', 'videodata', 'docdata', 'mobile'));
	}

	function image_gallery($type) {
		$user        = UsersModel::findOrFail(Auth::user()->id);
		$settings    = Settings::findOrFail(1);
		$gallerydata = Gallery::where('userid', $user['id'])->where('file_type', $type)->get();

		$detect = new \Mobile_Detect();
		$mobile = $detect->isMobile();

		return view('user.gallery', compact('user', 'settings', 'gallerydata', 'mobile'));
	}

	//Show Change Password Form
	function changePassform() {
		$detect = new \Mobile_Detect();
		$mobile = $detect->isMobile();

		return view('user.changepass', compact('mobile'));
	}

	//Edit Profile Form
	function edit() {
		UsersModel::$withoutAppends = TRUE;
		$categories                 = Category::all();
		$user                       = UsersModel::find(Auth::user()->id);
		$gallerydata                = Gallery::where('userid', $user['id'])->where('file_type', 'image')->get();
		$videos                     = Gallery::where('userid', $user['id'])->where('file_type', 'video')->get();
		$docs                       = Gallery::where('userid', $user['id'])->where('file_type', 'document')->get();

		$detect = new \Mobile_Detect();
		$mobile = $detect->isMobile();

		return view('user.editprofile', compact('user', 'categories', 'gallerydata', 'videos', 'docs', 'mobile'));
	}

	function editMember() {
		UsersModel::$withoutAppends = TRUE;
		$categories                 = Category::all();
		$user                       = UsersModel::find(Auth::user()->id);
		$gallerydata                = Gallery::where('userid', $user['id'])->where('file_type', 'image')->get();
		$videos                     = Gallery::where('userid', $user['id'])->where('file_type', 'video')->get();
		$docs                       = Gallery::where('userid', $user['id'])->where('file_type', 'document')->get();

		$detect = new \Mobile_Detect();
		$mobile = $detect->isMobile();
		$isMember = true;

		return view('user.editprofile', compact('user', 'categories', 'gallerydata', 'videos', 'docs', 'mobile', 'isMember'));
    }

	//Update Profile
	function update(Request $request, $id) {
		$user  = UsersModel::findOrFail($id);
		$input = $request->all();

		$input['name_slug'] = slug($request->name);

		if($request->q_titles != '' && $request->qualities != '') {
			$input['qtitles']        = implode("|", $request->q_titles);
			$input['qualifications'] = implode("|", $request->qualities);
		}

		if ($file = $request->file('photo')) {
			$photo_name = time() . $request->file('photo')->getClientOriginalName();
			$file->move('assets/images/profile_photo', $photo_name);
			$input['photo'] = $photo_name;
		}

		$user->update($input);
		Session::flash('message', 'Profil actualizat cu succes.');

		return redirect('user/edit');
	}

    function updateMember(Request $request, $id)
    {
        $user  = UsersModel::findOrFail($id);
        $input = $request->all();

        $input['name_slug'] = slug($request->name);

        if($request->q_titles != '' && $request->qualities != '') {
            $input['qtitles']        = implode("|", $request->q_titles);
            $input['qualifications'] = implode("|", $request->qualities);
        }

        if ($file = $request->file('photo')) {
            $photo_name = time() . $request->file('photo')->getClientOriginalName();
            $file->move('assets/images/profile_photo', $photo_name);
            $input['photo'] = $photo_name;
        }

        $user->update($input);
        Session::flash('message', 'Profil actualizat cu succes.');

        return redirect('user/edit');
	}

	//Update Profile
	function publish($id) {
		$user            = Profile::findOrFail($id);
		$input['status'] = 1;

		$user->update($input);
		Session::flash('message', 'Your Profile Published Successfully.');

		return redirect('user/dashboard');
	}

	//Change User Password
	function changepass(Request $request, $id) {
		$user              = Profile::findOrFail($id);
		$input['password'] = "";
		if ($request->cpass) {
			if (Hash::check($request->cpass, $user->password)) {
				if ($request->newpass == $request->renewpass) {
					$input['password'] = Hash::make($request->newpass);
				} else {
					Session::flash('error', 'Confirm Password Does not match.');

					return redirect('user/changepassword');
				}
			} else {
				Session::flash('error', 'Current Password Does not match');

				return redirect('user/changepassword');
			}
		}

		$user->update($input);
		Session::flash('message', 'Account Password Updated Successfully.');

		return redirect('user/changepassword');
	}
}
