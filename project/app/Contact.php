<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Contact extends Model {
	protected $table = 'contact';
	protected $fillable = ['userid', 'name', 'email', 'phone', 'apointment', 'contact'];
	public $timestamps = TRUE;
}
