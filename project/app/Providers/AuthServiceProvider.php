<?php

namespace App\Providers;

use Illuminate\Support\Facades\Gate;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class AuthServiceProvider extends ServiceProvider {
	/**
	 * The policy mappings for the application.
	 *
	 * @var array
	 */
	protected $policies
		= [
			'App\Model' => 'App\Policies\ModelPolicy',
		];

	/**
	 * Register any authentication / authorization services.
	 *
	 * @return void
	 */
	function boot() {
		$this->registerPolicies();
		//
	}

	/**
	 * Register any application services.
	 *
	 * @return void
	 */
	function register() {
		//
		require_once __DIR__ . '/../Http/helpers.php';
		require_once __DIR__ . '/../Http/mobile.php';
	}
}
