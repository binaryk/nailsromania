@extends('includes.master')



@section('content')

	<?php

	if ( ! $mobile) {

		$class = 'container';

	} else {

		$class = 'row';

	}

	?>

    <div class="{{$class}}">

    <nav class="navbar navbar-bootsnipp" style="{!! $mobile ? '' : 'display: none;' !!}" role="map-navigation" id="map-nav">
            <div class="container">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                    <button type="button" class="navbar-togglex" data-toggle="collapse" data-target="#map-navigation"
                    style="color: #fff;
                        background: inherit;
                        border: none;
                        width: 100%;
                        height: 40px;
                        line-height: 40px;
                        font-weight: 600;
                        font-size: 16px;
                        text-align: center;">Vezi harta
                    </button>
                </div>
                <div class="collapse navbar-collapse" id="map-navigation" style="width: 100%; height: 100%;">
                            @include('map.body', compact('cities', 'categories', 'mobile'))
                </div>
            </div>
        </nav>


        @if(! $mobile)
            @include('map.body')
        @endif
    </div>
	<?php

	if(count($mapMarkers) > 0) {

	foreach ($mapMarkers as $marker) {

		$json[ $marker->id ]['latitude']  = (float) $marker->latitude;

		$json[ $marker->id ]['longitude'] = (float) $marker->longitude;

		$json[ $marker->id ]['featured']  = (int) $marker->map;

		$json[ $marker->id ]['company']   = (int) $marker->company;

	}

	?>

    <script type="text/javascript">azl.locations = <?php echo json_encode($json); ?></script>

	<?php } ?>



    <section class="go-section" style="padding-bottom:0;">

        <div class="row">

            <div class="container">

                <h2 class="text-center">Toate categoriile</h2>

                <hr>

                @foreach($categories as $category)

                    <div class="col-md-4 cats" id="{{$category->slug}}">

                        <a href="category/{{$category->slug}}" class="btn btn-genius btn-block">

                            <strong> {{$category->name}}</strong>

                        </a>

                    </div>

                @endforeach

            </div>

            @if ( ! $mobile) {

            <div class="container">

              <a href="https://nailsromania.ro/user/registration">  <img class="img-responsive" src="{{url('/')}}/assets/images/banner_categorii.gif" style="margin:0 auto;"/></a>

            </div>

            @endif

        </div>

    </section>



    <div id="wrapper" class="go-section" style="padding-top:0;">

        <div class="container">

            <h2 class="text-center">Nail Artisti PREMIUM</h2>

            <hr>

			@if(count($featured) > 0)

            @foreach($featured as $feature)

				<?php

				if($lastPayer) {

					if ($feature->id == $lastPayer->user_id) {

						$class = 'lastPayer';

					} else {

						$class = '';

					}

				}

				?>

                <div class="col-md-3 col-xs-6 col-sm-6 single <?php echo $class; ?>">

                    <div class="group featured">

                        <a href="{{url('/')}}/profile/{{$feature->id}}/{{$feature->name_slug}}">

                            @if($feature->photo != "")
                               @if($feature->provider !== null)
                                <img src="{{ $feature->photo  }}"
                                     class="profile-image"
                                     alt="{{$feature->name}}"
                                     width="250"
                                     height="250">
                                @else
                                <img src="{{imageUrl(url('/').'/assets/images/profile_photo/' . $feature->photo, 250, 250)}}"
                                     class="profile-image"
                                     alt="{{$feature->name}}"
                                     width="250"
                                     height="250">
                                @endif
                            @else

                                <img src="{{url('/')}}/assets/images/profile_photo/avatar.jpg" style="max-width: 100%;" class="profile-image" alt="{{$feature->name}}">

                            @endif

                            @if($feature->company == 1)

                                <span class="company">COMPANIE</span>

                            @endif

                            <span class="premium">PREMIUM</span>

                            <div class="text-center listing">

                                <h3 class="no-margin go-bold">{{$feature->name}}</h3>

                                <p class="no-margin">{{$feature->category}} - {{$feature->city}}</p>

                            </div>

                        </a>

                    </div>

                </div>

            @endforeach

			@endif

        </div>

        <div class="container">

            <hr style="margin-top:15px;">

            <h2 class="text-center">Nail Artistii</h2>

            <hr>

			@if(count($allUsers) > 0)

            @foreach($allUsers as $user)

                <div class="col-md-3 col-xs-6 col-sm-6 single">

                    <div class="group featured">

                        <a href="{{url('/')}}/profile/{{$user->id}}/{{$user->name_slug}}">
                            @if($user->photo != "")
                                
                                <img src="{{ facebookOrLocal($user->photo, 250, 250) }}"
                                     class="profile-image"
                                     alt="{{$user->name}}"
                                     width="250"
                                     height="250">

                            @else
                                <img src="{{url('/')}}/assets/images/profile_photo/avatar.jpg" style="max-width: 100%;" class="profile-image" alt="{{$user->name}}">
                            @endif
                            @if($user->company == 1)
                                <span class="company">COMPANIE</span>
                            @endif
                            <div class="text-center listing">
                                <h3 class="no-margin go-bold">{{$user->name}}</h3>
                                <p class="no-margin">{{$user->category}} - {{$user->city}}</p>
                            </div>
                        </a>

                    </div>

                </div>

            @endforeach

			@endif

        </div>

    </div>

@stop



@section('footer')

<script>
	window.SGPMPopupLoader=window.SGPMPopupLoader||{ids:[],popups:{},call:function(w,d,s,l,id){
		w['sgp']=w['sgp']||function(){(w['sgp'].q=w['sgp'].q||[]).push(arguments[0]);}; 
		var sg1=d.createElement(s),sg0=d.getElementsByTagName(s)[0];
		if(SGPMPopupLoader && SGPMPopupLoader.ids && SGPMPopupLoader.ids.length > 0){SGPMPopupLoader.ids.push(id); return;}
		SGPMPopupLoader.ids.push(id);
		sg1.onload = function(){SGPMPopup.openSGPMPopup();}; sg1.async=true; sg1.src=l;
		sg0.parentNode.insertBefore(sg1,sg0);
		return {};
	}};
	SGPMPopupLoader.call(window,document,'script','https://popupmaker.com/assets/lib/SGPMPopup.min.js','dd00807f');
	
	    setTimeout(function () {
            map.setCenter({lat: 46.1841, lng: 25.2224});
            map.setZoom(7);    
        }, 1300);
        
</script>

<!-- Facebook Pixel Code -->
<script>
  !function(f,b,e,v,n,t,s)
  {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
  n.callMethod.apply(n,arguments):n.queue.push(arguments)};
  if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
  n.queue=[];t=b.createElement(e);t.async=!0;
  t.src=v;s=b.getElementsByTagName(e)[0];
  s.parentNode.insertBefore(t,s)}(window, document,'script',
  'https://connect.facebook.net/en_US/fbevents.js');
  fbq('init', '804899923028259');
  fbq('track', 'PageView');
</script>
<noscript><img height="1" width="1" style="display:none"
  src="https://www.facebook.com/tr?id=804899923028259&ev=PageView&noscript=1"
/></noscript>
<!-- End Facebook Pixel Code -->

<!-- begin olark code -->
<script type="text/javascript" async>
;(function(o,l,a,r,k,y){if(o.olark)return;
r="script";y=l.createElement(r);r=l.getElementsByTagName(r)[0];
y.async=1;y.src="//"+a;r.parentNode.insertBefore(y,r);
y=o.olark=function(){k.s.push(arguments);k.t.push(+new Date)};
y.extend=function(i,j){y("extend",i,j)};
y.identify=function(i){y("identify",k.i=i)};
y.configure=function(i,j){y("configure",i,j);k.c[i]=j};
k=y._={s:[],t:[+new Date],c:{},l:a};
})(window,document,"static.olark.com/jsclient/loader.js");
/* Add configuration calls below this comment */
olark.identify('6892-872-10-5248');</script>
<!-- end olark code -->


@stop