<?php $style = 'register'; ?>

@extends('includes.master')

@section('content')
    @if(!$mobile)
        @include('parts.header', ['background'=>'background_register.jpg'])
    @endif

    <div id="wrapper" class="go-section">
        <div class="row">
            <div class="container">
                <div class="row text-center">
                    <img src="{{url('/')}}/assets/images/PASUL2.png" alt="pasul2" class="img-responsive" style="margin:0 auto;"/>
                </div>
                <hr>
                <div id="response"></div>
                <form id="registerForm" action="{{route('user.reg.submit')}}" method="post">
                    {{csrf_field()}}
                    <input type="hidden" name="user_id" value="{{$user}}"/>
                    <div class="row">
                        <div class="col-md-6 col-md-offset-3">
                            <div class="form-group">
                                <select name="company" class="form-control" required>
                                    <option value="0">Individual</option>
                                    <option value="2">Societate Comerciala/I.I.</option>
                                    <option value="1">Salon</option>
                                </select>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-6 col-md-offset-3">
                            <div class="form-group">
                                <select name="category" class="form-control" required>
                                    <option value="">Selecteaza Categoria</option>
                                    @foreach($categories as $category)
                                        <option value="{{$category->name}}">{{$category->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-md-10 col-md-offset-3">

                        </div>

                        <label class="col-md-5 control-label"></label>
                        <div class="col-md-2">
                            <button id="RegButton" class="btn btn-ocean btn-block">
                                <strong>Inregistreaza-te</strong>
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@stop

@section('footer')
    <script type="text/javascript">
        //Google maps details
        var componentForm = {
            address: 'long_name',
            locality: 'short_name'
        };
        var input_load = document.getElementById('address_input');
        var options = {
            types: ['geocode']
        };
        address = new google.maps.places.Autocomplete(input_load, options);
        address.addListener('place_changed', complete_address_fields);

        function complete_address_fields() {
            var place = address.getPlace();
            document.getElementById('lat_input').value = place.geometry.location.lat();
            document.getElementById('lng_input').value = place.geometry.location.lng();

            for (var i = 0; i < place.address_components.length; i++) {
                var addressType = place.address_components[i].types[0];
                if (componentForm[addressType]) {
                    var val = place.address_components[i][componentForm[addressType]];
                    document.getElementById(addressType + '_input').value = val;
                }
            }
        }

        $('#RegButton').click(function (e) {
            e.preventDefault();
            gtag_report_conversion('{{url()->current()}}');
            var dataStr = $('#registerForm').serialize();

            $.ajax({
                type: "POST",
                url: "{{route('user.reg.submit')}}",
                data: dataStr,
                success: function (r) {
                    r = JSON.parse(r);
                    window.location.href = r.login_url;
                },
                error: function (e) {

                }
            });
        });
    </script>
@stop