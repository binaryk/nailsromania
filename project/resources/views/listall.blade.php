@extends('includes.master')
@section('header')
    @parent

    <style>
        @media (max-width: 768px) {
            .group {
                border: none;
            }
            .single {
                height: 220px;
            }
            .container-fluid>.navbar-collapse, .container-fluid>.navbar-header, .container>.navbar-collapse, .container>.navbar-header {
                margin: 0;
            }
            #map-nav, #map-nav > .container {
                padding: 0;
            }
            #map-navigation {
                padding: 0;
                margin: 0 auto;
            }

            .azl-map-wrapper .azl-map {
                height: 333px;
            }
        }
    </style>

@stop
@section('content')
    <nav class="navbar navbar-bootsnipp" style="{!! $mobile ? '' : 'display: none;' !!}" role="map-navigation" id="map-nav">
        <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <a href="{!! url('/') !!}" type="button" class="navbar-togglex" data-toggle="collapse" data-target="#map-navigation"
                        style="color: #fff;
                        text-align: center;
                        display: inline-block;
                        background: inherit;
                        border: none;
                        width: 100%;
                        height: 40px;
                        line-height: 40px;
                        font-weight: 600;
                        font-size: 16px;
                        text-align: center;">Inapoi
                </a>
            </div>
        </div>
    </nav>

    <div id="wrapper" class="go-section">
        <div class="row">
            <div class="container">
                <h2 class="text-center">{{$pagename}}</h2>
                <hr>
                <div class="gocover"></div>
                <div id="alldocs">
                    @foreach($allusers as $alluser)
                        <div class="col-md-3 col-xs-6 col-sm-6 single">
                            <div class="group<?php echo ($alluser['featured'] == 1) ? " featured" : ""; ?>">
                                <a href="{{url('/')}}/profile/{{$alluser->id}}/{{$alluser->name_slug}}">
                                    @if($alluser->photo != "")
                                        <img src="{{ facebookOrLocal($alluser->photo, 250, 250) }}"
                                             class="profile-image"
                                             alt="{{$alluser->name}}"
                                             width="250"
                                             height="250">
                                    @else
                                        <img src="{{url('/')}}/assets/images/profile_photo/avatar.jpg" style="max-width: 100%;" class="profile-image" alt="{{$alluser->name}}">
                                    @endif
                                    @if($alluser->company == 1)
                                        <span class="company">COMPANIE</span>
                                    @endif
                                    @if($alluser->featured ==1)
                                        <span class="premium">PREMIUM</span>
                                    @endif
                                    <div class="text-center listing">
                                        <h3 class="no-margin go-bold">{{$alluser->name}}</h3>
                                        <p class="no-margin">{{$alluser->category}} - {{$alluser->city}}</p>
                                    </div>
                                </a>
                            </div>
                        </div>
                    @endforeach
                    <div class='col-md-12 margintop'></div>
                </div>

            </div>
        </div>
    </div>

@stop

@section('footer')

@stop