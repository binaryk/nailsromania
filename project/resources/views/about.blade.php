@extends('includes.master')

@section('content')


    <section style="background: url({{url('/')}}/assets/images/{{$settings[0]->background}}) no-repeat center center; background-size: cover;">
        <div class="row" style="background-image: url(" http:
        //makeupromania.ro/assets/images/Fotolia_111751750_Subscription_Monthly_XXL.jpg");">

        <div style="margin: 3% 0px 3% 0px;">
            <div class="text-center" style="color: #FFF;padding: 20px;">
                <h1>Despre Noi</h1>
            </div>
        </div>

        </div>


    </section>


    <div id="wrapper" class="go-section">
        <div class="row">
            <div class="container">
                <div class="col-md-12">
                    {!! $pagedata->about !!}
                </div>
            </div>
        </div>
    </div>

@stop

@section('footer')

@stop