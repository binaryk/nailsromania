@extends('includes.master')

@section('header')
    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/bootstrap-social/5.0.0/bootstrap-social.min.css">
@stop

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-4 col-md-offset-4">
                @if ($errors->has('msg'))
                    <div class="alert alert-warning">
                        {{ $errors->first('msg') }}
                        <button type="button" data-dismiss="alert" aria-label="Close" class="close"><span aria-hidden="true">×</span></button>
                    </div>
                @endif

                <div class="panel panel-default">
                    <div class="panel-heading text-center">Social Login</div>

                    <div class="panel-body">
                        <p class="lead text-center">Authenticate using your social network account from one of following providers</p>
                        <a href="{{ route('social.oauth', 'facebook') }}" class="btn btn-block btn-lg btn-social btn-facebook social-button">
                            <span class="fa fa-facebook"></span> Login with Facebook
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop