<?php

$style = 'register';

?>



@extends('includes.master', ['mobile' => $mobile])


@section('header')
<style>
    .image-container,
    .info-container {
        padding: 15px;
    }
    .next-button {
        height: 35px;
        width: 100%;
        vertical-align: middle;
        padding: 8px;
        background: #7E4DC5 !important;
        border: #7E4DC5 !important;
        margin-bottom: 15px;
        font-size: 14px;
        font-weight: 600;
    }
    .go-section {
        padding: 15px 0;
    }
</style>
@endsection

@section('content')
    @include('parts.header', ['background'=>'background_register.jpg', 'mobile' => $mobile])

    <div id="wrapper step-0" class="go-section">

        <div class="row">

            <div class="container">

                <div class="col-md-12">
                    <div class="image-container text-center">
                        <img src="{{imageUrl(url('/').'/assets/images/PASUL1.png')}}"
                             class="step-image"
                             alt="Step - 1"
                             width="220">
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="info-container text-center">
                        Bun venit pe Nails Romania, singura platforma din Romania destinata Nail Artistilor. Urca-ti portofoliul, datele de contact, diplomele si companiile sau clientii te gasesc extrem de usor.
                        <br>
                        Apasa butonul de mai jos pentru a continua inregistrarea.
                    </div>
                </div>
                <div class="col-md-12 text-center">
                    <a class="btn btn-primary btn-xs next-button" href="{{ route('user.step1') }}">Continua</a>
                </div>

                <div id="resp" class="col-md-6 col-md-offset-3"></div>

        </div>

    </div>

@stop



@section('footer')

    <script type="text/javascript">

        $('#RegButton').click(function (e) {

            e.preventDefault();

            gtag_report_conversion('{{url()->current()}}');

            var dataStr = $('#registerForm').serialize();



            $.ajax({

                type: "POST",

                url: "{{route('user.registration.submit')}}",

                data: dataStr,

                success: function (r) {

                },

                error: function (e) {



                }

            });

        });

    </script>
    <!-- begin olark code -->
    <script type="text/javascript" async>
        ;(function(o,l,a,r,k,y){if(o.olark)return;
            r="script";y=l.createElement(r);r=l.getElementsByTagName(r)[0];
            y.async=1;y.src="//"+a;r.parentNode.insertBefore(y,r);
            y=o.olark=function(){k.s.push(arguments);k.t.push(+new Date)};
            y.extend=function(i,j){y("extend",i,j)};
            y.identify=function(i){y("identify",k.i=i)};
            y.configure=function(i,j){y("configure",i,j);k.c[i]=j};
            k=y._={s:[],t:[+new Date],c:{},l:a};
        })(window,document,"static.olark.com/jsclient/loader.js");
        /* Add configuration calls below this comment */
        olark.identify('6892-872-10-5248');</script>
    <!-- end olark code -->
@stop
