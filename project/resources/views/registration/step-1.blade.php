<?php

$style = 'register';

?>



@extends('includes.master', ['mobile' => $mobile])


@section('header')
    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/bootstrap-social/5.0.0/bootstrap-social.min.css">
    <style>
        .image-container,
        .info-container {
            padding: 15px;
        }
        .btn-facebook {
            width: 100%;
            border-radius: 0px;
            font-size: 11px;
            margin-bottom: 15px;
        }
        .btn-social {
            text-align: center;
            font-size: 14px;
            font-weight: 600;
        }
        .btn-social {
            text-align: center;
        }
        .btn-social.btn-lg>:first-child {
            line-height: 36px;
        }
        .register-button {
            height: 35px;
            width: 100%;
            vertical-align: middle;
            padding: 8px;
            background: #7E4DC5 !important;
            border: #7E4DC5 !important;
            margin-bottom: 15px;
            font-size: 14px;
            font-weight: 600;
        }
        .go-section {
            padding: 15px 0;
        }
    </style>
@endsection

@section('content')

    @include('parts.header', ['background'=>'background_register.jpg', 'mobile' => $mobile])

    <div id="wrapper step-0" class="go-section">

        <div class="row">

            <div class="container">

                <div class="col-md-12">
                    <div class="image-container text-center">
                        <img src="{{imageUrl(url('/').'/assets/images/PASUL2.png')}}"
                             class="step-image"
                             alt="Step - 1"
                             width="220">
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="info-container text-center">
                        Inregistreaza-te in cateva secunde prin facebook.
                    </div>
                </div>
                <div class="col-md-12 text-center">
                    <a href="{{ route('social.oauth', 'facebook') }}" class="btn btn-block btn-lg btn-social btn-facebook social-button">
                        <span class="fa fa-facebook"></span> Continua cu contul de Facebook
                    </a>
                </div>
                <div class="col-md-12 text-center">
                    <a href="{{ route('social.oauth.member', 'facebook') }}" class="btn btn-block btn-lg btn-social btn-facebook social-button">
                        <span class="fa fa-facebook"></span> Continua ca si memmbru cu contul de Facebook
                    </a>
                </div>

                <div class="col-md-12 text-center">
                    <p>*Nota : Daca doresti realizarea unui cont manual, apasa butonul de mai jos.</p>
                    <a class="btn btn-primary btn-xs register-button" href="{{ route('user.registration') }}">Fa-ti cont manual</a>
                </div>
                
                <div id="resp" class="col-md-6 col-md-offset-3"></div>

            </div>

        </div>

        @stop



        @section('footer')

            <script type="text/javascript">

                $('#RegButton').click(function (e) {

                    e.preventDefault();

                    gtag_report_conversion('{{url()->current()}}');

                    var dataStr = $('#registerForm').serialize();



                    $.ajax({

                        type: "POST",

                        url: "{{route('user.registration.submit')}}",

                        data: dataStr,

                        success: function (r) {

                        },

                        error: function (e) {



                        }

                    });

                });

            </script>
            <!-- begin olark code -->
            <script type="text/javascript" async>
                ;(function(o,l,a,r,k,y){if(o.olark)return;
                    r="script";y=l.createElement(r);r=l.getElementsByTagName(r)[0];
                    y.async=1;y.src="//"+a;r.parentNode.insertBefore(y,r);
                    y=o.olark=function(){k.s.push(arguments);k.t.push(+new Date)};
                    y.extend=function(i,j){y("extend",i,j)};
                    y.identify=function(i){y("identify",k.i=i)};
                    y.configure=function(i,j){y("configure",i,j);k.c[i]=j};
                    k=y._={s:[],t:[+new Date],c:{},l:a};
                })(window,document,"static.olark.com/jsclient/loader.js");
                /* Add configuration calls below this comment */
                olark.identify('6892-872-10-5248');</script>
            <!-- end olark code -->
@stop