@extends('includes.master')

@section('content')

    @include('parts.profile.rating')
    <div id="wrapper" class="go-section">
        <div class="row">
            <div class="container">
                <div class="col-md-8">
                    @if ( $mobile)
                        @include('parts.profile.sidebar')
                    @endif
                    @if($profiledata->video == 1 && count($videos) > 0)
                        @include('parts.profile.videos')
                    @endif
                    @if(count($gallerydata) > 0)
                        @include('parts.profile.gallery')
                    @endif
                    <div class="profile-section">
                        <h3 class="no-margin">Descriere Artist</h3>
                        <hr>
                        {!! $profiledata->description !!}
                    </div>
                    <div class="profile-section">
                        <div class="row">
                            <h3 class="no-margin">Produse folosite</h3>
                            <hr>
                            <ul class="col-md-12 specialities">
                                @foreach($profiledata->specialities as $specialitie)
                                    <li class="col-md-6 specialitie">{{$specialitie}}</li>
                                @endforeach
                            </ul>
                        </div>
                    </div>

                    <div class="profile-section">
                        <div class="row">
                            <h3 class="no-margin">Specializari</h3>
                            <hr>
                            <ul class="col-md-12 qualifications">
                                <li class="col-md-12 qualification">

                                </li>
                                @if(!empty($profiledata->qtitles) && !empty($profiledata->qualifications))
                                    @foreach(array_combine($profiledata->qtitles, $profiledata->qualifications) as $title => $quality)
                                        <li class="col-md-12 qualification">
                                            <strong>{{$title}}</strong>
                                            <span>{{$quality}}</span>
                                        </li>
                                    @endforeach
                                @endif
                            </ul>
                        </div>
                        @if(count($docsdata) > 0)
                            <div class="row">
                                @foreach($docsdata as $document)
                                    <div class="col-sm-4 col-md-3 col-xs-6">
                                        <div class="thumbnail">
                                            <a data-fancybox="diplome" href="{{url('/')}}/assets/images/gallery/{{$document->image}}">
                                                <img src="{{imageUrl(url('/').'/assets/images/gallery/' . $document->image, 300, 300)}}" alt="{{$profiledata->name}}" class="img-responsive image-blur"/>
                                            </a>
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                        @endif
                    </div>
                    {{-- Advertisements Size: 728x90 --}}
                    <div style="margin-bottom:20px;">
                        <div class="desktop-advert">
                            @if(!empty($ads728x90))
                                @if($ads728x90->type == "banner")
                                    <a class="ads" href="{{$ads728x90->redirect_url}}" target="_blank">
                                        <img class="banner-728x90" src="{{url('/')}}/assets/images/ads/{{$ads728x90->banner_file}}" alt="Advertisement">
                                    </a>
                                @else
                                    {!! $ads728x90->script !!}
                                @endif
                            @endif
                        </div>
                    </div>

                    <div class="row">
                        <div id="profileTab">
                            <ul class="nav nav-tabs">
                                <li class="active">
                                    <a href="#contact" data-toggle="tab">Contact</a>
                                </li>
                                <li>
                                    <a href="#reviews" data-toggle="tab">Review-uri</a>
                                </li>
                            </ul>

                            <div class="tab-content ">
                                @include('parts.profile.contact')
                                <div class="tab-pane fade" id="reviews">
                                    <h3>Scrie o recenzie</h3>
                                    <hr>
                                    <div class="row" style="margin-bottom: 20px">
                                        <div class="col-md-6">
                                            <div class='starrr' id='star1'></div>
                                            <div>
                                        <span class='your-choice-was' style='display: none;'>
                                            Your rating is: <span class='choice'></span>.
                                        </span>
                                            </div>
                                        </div>
                                    </div>
                                    <form method="POST" action="{{route('review.submit')}}">
                                        {{ csrf_field() }}
                                        <input type="hidden" name="rating" id="rate" value="5">
                                        <input type="hidden" name="userid" value="{{$profiledata->id}}">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <input name="name" placeholder="Nume Complet" class="form-control" type="text" required>

                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <input name="email" placeholder="Email" class="form-control" type="email" required>

                                                </div>
                                            </div>
                                        </div>
                                        <!-- Text input-->
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <textarea name="review" rows="6" placeholder="Descrie experienta cu acest Nail Artist" class="form-control"></textarea>
                                                </div>
                                            </div>
                                        </div>

                                        <div id="resp" class="col-md-6">
                                            @if ($errors->has('error'))
                                                <span class="help-block">
                                                    <strong>{{ $errors->first('password') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                        <!-- Button -->
                                        <div class="row">
                                            <div class="form-group">
                                                <label class="col-md-3 control-label"></label>
                                                <div class="col-md-4 col-md-offset-1">
                                                    <button type="submit" class="btn btn-ocean btn-block" id="LoginButton"><strong>Publica recenzia</strong></button>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                    <hr>
                                    <h3>Reviews:</h3>
                                    <hr>
                                    @forelse($reviews as $review)
                                        <div class="row rating-row">
                                            <div class="col-md-3">
                                                <strong>{{$review->name}}</strong>
                                                <div class="rating-box">
                                                    @for($i=1;$i<=5;$i++)
                                                        @if($i <= $review->rating)
                                                            <div class="star"><i class="fa fa-star"></i></div>
                                                        @else
                                                            <div class="star"><i class="fa fa-star-o"></i></div>
                                                        @endif
                                                    @endfor
                                                </div>
                                                <div class="rating-date">{{$review->review_date}}</div>
                                            </div>
                                            <div class="col-md-8">
                                                {{$review->review}}
                                            </div>
                                        </div>
                                    @empty
                                        <h4>Nu s-a alocat niciun review acestui Nail Artist</h4>
                                    @endforelse
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                @if ( ! $mobile)
                    @include('parts.profile.sidebar')
                @endif
            </div>
        </div>
    </div>

@stop

@section('footer')
    <script type="text/javascript">
        $('#star1').starrr({
            rating: 5,
            change: function (e, value) {
                if (value) {
                    $('.your-choice-was').show();
                    $('.choice').text(value);
                    $('#rate').val(value);
                } else {
                    $('.your-choice-was').hide();
                }
            }
        });
        @if(count($gallerydata) > 0)
        $('a[data-fancybox="gallery"]').fancybox({
            idleTime: false,
            margin: 0,
            loop: false,
            protect: true,
            infobar: false,
            zoomOpacity: false,
            thumbs: {
                hideOnClose: false
            },
            touch: {
                vertical: 'auto'
            },
            buttons: [
                'close'
            ],
            closeClickOutside: false,
            helpers: {
                title: {
                    type: 'inside'
                }
            }
        });
        @endif
        @if(count($docsdata) > 0)
        $('a[data-fancybox="diplome"]').fancybox({
            idleTime: false,
            margin: 0,
            loop: false,
            protect: true,
            infobar: false,
            zoomOpacity: false,
            thumbs: {
                hideOnClose: false
            },
            touch: {
                vertical: 'auto'
            },
            buttons: [
                'close'
            ],
            closeClickOutside: false,
            helpers: {
                title: {
                    type: 'inside'
                }
            }
        });
        @endif
    </script>
@stop