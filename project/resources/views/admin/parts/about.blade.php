<div class="tab-pane" id="about">
    <p class="lead">Despre Noi</p>
    <div class="ln_solid"></div>
    <form method="POST" action="settings/about" class="form-horizontal form-label-left" id="about_form">
        {{csrf_field()}}
        <div class="item form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="about"> Despre Noi Text <span class="required">*</span>
            </label>
            <div class="col-md-6 col-sm-6 col-xs-12">
                <textarea rows="10" cols="60" id="aboutpnael" class="form-control" name="about">{{$setting[0]->about}}</textarea>
            </div>
        </div>

        <div class="ln_solid"></div>
        <div class="form-group">
            <div class="col-md-6 col-md-offset-3">
                <!--  <button type="submit" class="btn btn-primary">Cancel</button> -->
                <button type="submit" id="about_update" class="btn btn-success btn-block">Update Settings</button>
            </div>
        </div>
    </form>
</div>