<div class="tab-pane" id="address">
    <p class="lead">Office Adresa</p>
    <div class="ln_solid"></div>
    <form method="POST" action="settings/address" class="form-horizontal form-label-left" id="about_form">
        {{csrf_field()}}
        <div class="item form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="address"> Street Adresa <span class="required">*</span>
            </label>
            <div class="col-md-6 col-sm-6 col-xs-12">
                <textarea rows="3" cols="60" class="form-control col-md-7 col-xs-12" name="address">{{$setting[0]->address}}</textarea>
            </div>
        </div>
        <div class="item form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="phone">Phone <span class="required">*</span>
            </label>
            <div class="col-md-6 col-sm-6 col-xs-12">
                <input id="name" class="form-control col-md-7 col-xs-12" data-validate-length-range="6" name="phone" placeholder="Phone Number" type="text" value="{{$setting[0]->phone}}">
            </div>
        </div>
        <div class="item form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="fax"> Fax <span class="required">*</span>
            </label>
            <div class="col-md-6 col-sm-6 col-xs-12">
                <input id="name" class="form-control col-md-7 col-xs-12" data-validate-length-range="6" name="fax" placeholder="Fax" type="text" value="{{$setting[0]->fax}}">
            </div>
        </div>
        <div class="item form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="email"> Email <span class="required">*</span>
            </label>
            <div class="col-md-6 col-sm-6 col-xs-12">
                <input id="name" class="form-control col-md-7 col-xs-12" data-validate-length-range="6" name="email" placeholder="Email Adresa" type="text" value="{{$setting[0]->email}}">
            </div>
        </div>
        <div class="ln_solid"></div>
        <div class="form-group">
            <div class="col-md-6 col-md-offset-3">
                <!--  <button type="submit" class="btn btn-primary">Cancel</button> -->
                <button id="office_update" type="submit" class="btn btn-success btn-block">Update Settings</button>
            </div>
        </div>
    </form>
</div>