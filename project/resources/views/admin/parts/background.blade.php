<div class="tab-pane" id="background">
    <p class="lead">Background Image</p>
    <div class="ln_solid"></div>
    <form method="POST" action="settings/background" class="form-horizontal form-label-left" enctype="multipart/form-data">
        {{csrf_field()}}
        <div class="item form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Current Background Image
            </label>
            <div class="col-md-6 col-sm-6 col-xs-12">
                <img class="col-md-10" src="../assets/images/{{$setting[0]->background}}">
            </div>

        </div>
        <br>
        <!-- <input type="hidden" name="id" value="1"> -->
        <div class="item form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="name">Setup New Background <span class="required">*</span>
            </label>
            <div class="col-md-6 col-sm-6 col-xs-12">
                <input type="file" name="background" required="required"/>
            </div>
        </div>

        <div class="ln_solid"></div>
        <div class="form-group">
            <div class="col-md-6 col-md-offset-3">
                <!--  <button type="submit" class="btn btn-primary">Cancel</button> -->
                <button type="submit" class="btn btn-success btn-block">Update Settings</button>
            </div>
        </div>
    </form>
</div>