<div class="tab-pane" id="payment">
    <p class="lead">Informatii plata</p>

    <div class="ln_solid"></div>
    <form method="POST" action="{{action('SettingsController@paymentinfo')}}" class="form-horizontal form-label-left">
        {{csrf_field()}}
        <div class="item form-group">
            <label class="control-label col-md-4 col-sm-4 col-xs-12" for="title"> Paypal Business Email <span class="required">*</span>
            </label>
            <div class="col-md-6 col-sm-6 col-xs-9">
                <input class="form-control col-md-6 col-xs-9" name="paypal_business" placeholder="Website Title" required="required" type="text" value="{{$setting[0]->paypal_business}}">
                <select class="form-control col-md-6 col-xs-3" name="paypal_sandbox">
                    <option value="1" {{($setting[0]->paypal_sandbox == 1) ? 'selected' : ''}}>Sandbox</option>
                    <option value="0" {{($setting[0]->paypal_sandbox == 0) ? 'selected' : ''}}>Live</option>
                </select>
            </div>
        </div>
        <div class="item form-group">
            <label class="control-label col-md-4 col-sm-4 col-xs-12" for="title"> Normal Profile Price <span class="required">*</span>
            </label>
            <div class="col-md-6 col-sm-6 col-xs-12">
                <input class="form-control col-md-7 col-xs-12" name="normal_price" placeholder="Normal Profile Price" required="required" type="text" value="{{$setting[0]->normal_price}}">
            </div>
        </div>
        <div class="item form-group">
            <label class="control-label col-md-4 col-sm-4 col-xs-12" for="title"> Featured Profile Price <span class="required">*</span>
            </label>
            <div class="col-md-6 col-sm-6 col-xs-12">
                <input class="form-control col-md-7 col-xs-12" name="featured_price" placeholder="Featured Profile Price" required="required" type="text" value="{{$setting[0]->featured_price}}">
            </div>
        </div>
        <div class="item form-group">
            <label class="control-label col-md-4 col-sm-4 col-xs-12" for="title"> Monthly Profile Price <span class="required">*</span>
            </label>
            <div class="col-md-6 col-sm-6 col-xs-12">
                <input class="form-control col-md-7 col-xs-12" name="monthly_price" placeholder="Monthly Profile Price" required="required" type="text" value="{{$setting[0]->monthly_price}}">
            </div>
        </div>
        <div class="item form-group">
            <label class="control-label col-md-4 col-sm-4 col-xs-12" for="title"> Anual Profile Price <span class="required">*</span>
            </label>
            <div class="col-md-6 col-sm-6 col-xs-12">
                <input class="form-control col-md-7 col-xs-12" name="anual_price" placeholder="Anual Profile Price" required="required" type="text" value="{{$setting[0]->anual_price}}">
            </div>
        </div>
        <div class="item form-group">
            <label class="control-label col-md-4 col-sm-4 col-xs-12" for="title"> Different Map Marker Price <span class="required">*</span>
            </label>
            <div class="col-md-6 col-sm-6 col-xs-12">
                <input class="form-control col-md-7 col-xs-12" name="map_price" placeholder="Map Marker Price" required="required" type="text" value="{{$setting[0]->map_price}}">
            </div>
        </div>
        <div class="item form-group">
            <label class="control-label col-md-4 col-sm-4 col-xs-12" for="title"> Enable Video Price <span class="required">*</span>
            </label>
            <div class="col-md-6 col-sm-6 col-xs-12">
                <input class="form-control col-md-7 col-xs-12" name="video_price" placeholder="Enable Video  Price" required="required" type="text" value="{{$setting[0]->video_price}}">
            </div>
        </div>

        <div class="ln_solid"></div>
        <div class="form-group">
            <div class="col-md-6 col-md-offset-3">
                <!--  <button type="submit" class="btn btn-primary">Cancel</button> -->
                <button type="submit" id="website_update" class="btn btn-success btn-block">Update Settings</button>
            </div>
        </div>
    </form>
</div>