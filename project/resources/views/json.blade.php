<?php
$json = '';
if ($post['action'] == 'get_location') {
	$json['id']          = $result['id'];
	$json['url']         = '/profile/' . $result['id'] . '/' . $result['name'];
	$json['title']       = ($result['company'] == 1) ? 'Companie: ' . $result['name'] : $result['name'];
	$json['image']       = '<a href="/profile/' . $result['id'] . '/' . $result['name'] . '"><img class="image lazy" src="/assets/images/profile_photo/' . $result['photo'] . '" alt=""></a>';
	$json['description'] = $result['category'] . ' in ' . $result['city'];
} elseif ($post['action'] == 'locations') {
	foreach ($result as $location) {
		$json[ $location['id'] ]['latitude']  = $location['latitude'];
		$json[ $location['id'] ]['longitude'] = $location['longitude'];
	}
} else {
	$json['message']   = $post['message'];
	$json['login_url'] = $post['url'];
	$json['success']   = 'success';
}

echo json_encode($json);