<?php

$style = 'register';

?>



@extends('includes.master')



@section('content')
  <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/bootstrap-social/5.0.0/bootstrap-social.min.css">

    @if(!$mobile)

        @include('parts.header', ['background'=>'background_register.jpg'])

    @endif

    <div id="wrapper" class="go-section">

        <div class="row">

            <div class="container">

                <hr>

                <div id="resp" class="col-md-6 col-md-offset-3">

                    @if ($errors->has('name'))

                        <span class="help-block">

							<strong>* {{ $errors->first('name') }}</strong>

						</span>

                    @endif



                    @if ($errors->has('email'))

                        <span class="help-block">

							<strong>* {{ $errors->first('email') }}</strong>

						</span>

                    @endif



                    @if ($errors->has('phone'))

                        <span class="help-block">

							<strong>* {{ $errors->first('phone') }}</strong>

						</span>

                    @endif



                    @if ($errors->has('password'))

                        <span class="help-block">

							<strong>* {{ $errors->first('password') }}</strong>

						</span>

                    @endif

                </div>

                <form id="registerForm" action="{{route('user.registration.submit')}}" method="post">

                    {{csrf_field()}}
                    <div class="row">

                        <div class="col-md-6 col-md-offset-3">

                            <div class="form-group">

                                <input @if(old('name') != '') value="{{old('name')}}" @endif name="name" placeholder="Numele complet" class="form-control" type="text" required>

                                <p id="nameError" class="errorMsg"></p>

                            </div>

                        </div>

                    </div>



                    <div class="row">

                        <div class="col-md-6 col-md-offset-3">

                            <div class="form-group">

                                <input @if(old('email') != '') value="{{old('email')}}" @endif name="email" placeholder="Email" class="form-control" type="email" required>

                                <p id="emailError" class="errorMsg"></p>

                            </div>

                        </div>

                    </div>



                    <div class="row">

                        <div class="col-md-6 col-md-offset-3">

                            <div class="form-group">

                                <input @if(old('phone') != '') value="{{old('phone')}}" @endif name="phone" placeholder="Telefon" class="form-control" type="text" required>

                                <p id="phoneError" class="errorMsg"></p>

                            </div>

                        </div>

                    </div>



                    <div class="row">

                        <div class="col-md-6 col-md-offset-3">

                            <div class="form-group">

                                <input name="password" placeholder="Parola" class="form-control" type="password" required>

                                <p id="passError" class="errorMsg"></p>

                            </div>

                        </div>

                    </div>



                    <div class="form-group">

                        <div class="col-md-10 col-md-offset-3"></div>



                        <label class="col-md-5 control-label"></label>

                        <div class="col-md-2">

                            <button type="submit" id="RegButton" class="btn btn-ocean btn-block">

                                <strong>Inregistreaza-te</strong>

                            </button>

                        </div>

                    </div>

                </form>

            </div>

        </div>

    </div>

@stop



@section('footer')

    <script type="text/javascript">

        $('#RegButton').click(function (e) {

            e.preventDefault();

            gtag_report_conversion('{{url()->current()}}');

            var dataStr = $('#registerForm').serialize();



            $.ajax({

                type: "POST",

                url: "{{route('user.registration.submit')}}",

                data: dataStr,

                success: function (r) {

                },

                error: function (e) {



                }

            });

        });

    </script>
<!-- begin olark code -->
<script type="text/javascript" async>
;(function(o,l,a,r,k,y){if(o.olark)return;
r="script";y=l.createElement(r);r=l.getElementsByTagName(r)[0];
y.async=1;y.src="//"+a;r.parentNode.insertBefore(y,r);
y=o.olark=function(){k.s.push(arguments);k.t.push(+new Date)};
y.extend=function(i,j){y("extend",i,j)};
y.identify=function(i){y("identify",k.i=i)};
y.configure=function(i,j){y("configure",i,j);k.c[i]=j};
k=y._={s:[],t:[+new Date],c:{},l:a};
})(window,document,"static.olark.com/jsclient/loader.js");
/* Add configuration calls below this comment */
olark.identify('6892-872-10-5248');</script>
<!-- end olark code -->
@stop
