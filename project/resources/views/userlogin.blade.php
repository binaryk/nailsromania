@extends('includes.master')
@section('header')
    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/bootstrap-social/5.0.0/bootstrap-social.min.css">
    <style>
        .image-container,
        .info-container {
            padding: 15px;
        }
        .btn-facebook {
            width: 100%;
            border-radius: 0px;
            font-size: 11px;
            margin-bottom: 15px;
        }
        .btn-social {
            text-align: center;
        }
        .btn-social.btn-lg>:first-child {
            line-height: 36px;
        }
        .register-button {
            height: 35px;
            width: 100%;
            vertical-align: middle;
            padding: 8px;
            background: #7E4DC5 !important;
            border: #7E4DC5 !important;
            margin-bottom: 15px;
        }
        .go-section {
            padding: 15px 0;
        }
    </style>
@endsection
@section('content')
    @if(!$mobile)
        @include('parts.header', ['background'=>'background_login.jpg'])
    @endif
    <div id="wrapper" class="go-section">
        <div class="row">
            <div class="container">
                <div class="container">
                    <!-- Form Name -->
                    <h2 class="text-center">Login</h2>
                    <hr>

                    <div class="col-md-12 text-center">
                        <a href="{{ route('social.oauth', 'facebook') }}" class="btn btn-block btn-lg btn-social btn-facebook social-button">
                            <span class="fa fa-facebook"></span> Continua cu contul de Facebook
                        </a>
                    </div>
                    
                    <form role="form" method="POST" action="{{ route('user.login.submit') }}" style="display: none;">
                        {{ csrf_field() }}
                        <div class="row">
                            <div class="col-md-6 col-md-offset-3">
                                <div class="form-group">
                                    <input name="email" value="{{ old('email') }}" placeholder="Email" class="form-control" type="email" required>
                                    <p id="emailError" class="errorMsg"></p>
                                </div>
                            </div>
                        </div>
                        <!-- Text input-->
                        <div class="row">
                            <div class="col-md-6 col-md-offset-3">
                                <div class="form-group">
                                    <input name="password" placeholder="Password" class="form-control" type="password" required>
                                    <p id="passError" class="errorMsg"></p>
                                </div>
                            </div>
                        </div>

                        <div id="resp" class="col-md-6 col-md-offset-3">
                            @if ($errors->has('password'))
                                <span class="help-block">
                                <strong>{{ $errors->first('password') }}</strong>
                            </span>
                            @endif
                            @if ($errors->has('email'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('email') }}</strong>
                                </span>
                            @endif
                        </div>
                        <!-- Button -->
                        <div class="form-group">
                            <label class="col-md-5 control-label"></label>
                            <div class="col-md-2">
                                <button type="submit" class="btn btn-ocean btn-block" id="LoginButton"><strong>Login</strong></button>
                            </div>
                            <div class="col-md-2" style="margin-top: 10px;">
                                <a href="{{route('user.forgotpass')}}" class="pull-right">Forgot Password?</a>
                            </div>

                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

@stop

@section('footer')

@stop