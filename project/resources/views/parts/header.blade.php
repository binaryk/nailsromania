<section style="background: url({{ url('/')}}/assets/images/{{$background}}) no-repeat center center; background-attachment:scroll;
        {{ $mobile ? 'background-size: contain;' : 'background-size: auto;' }}">
    <div class="row">
        <div style="margin: 0 auto;">
            <div class="text-center" style="color: #FFF; padding: 20px; {{ $mobile ? 'min-height: 120px;' : 'min-height: 300px;'  }}">
            </div>
        </div>
    </div>
</section>