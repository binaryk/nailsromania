<div class="profile-section">
    <h3 class="no-margin">Portofoliu</h3>
    <hr>
    <div id="gallery" class="row">
        @foreach($gallerydata as $image)
            <div class="col-sm-4 col-md-3 col-xs-6">
                <div class="thumbnail">
                    <a data-fancybox="gallery" href="{{url('/')}}/assets/images/gallery/{{$image->image}}">
                        <img src="{{imageUrl(url('/').'/assets/images/gallery/' . $image->image, 100, 100)}}" alt="{{$profiledata->name}}" class="img-responsive image-blur"/>
                        <div class="caption">
                            <div class="fb-like" data-href="{{url('/')}}/assets/images/gallery/{{$image->image}}" data-layout="button_count" data-action="like" data-size="small" data-show-faces="false" data-share="false"></div>
                        </div>
                    </a>
                </div>
            </div>
        @endforeach
    </div>
</div>