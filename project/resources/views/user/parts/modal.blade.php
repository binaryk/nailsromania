<!-- Modal -->
<div class="modal fade-scale" id="{{$modal_id}}" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">{{$modal_title}}</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12 text-center">
                        <p class="small-label" style="font-weight:700;">{!! $modal_subject !!}</p>
                        <h4 class="cost">{{$feature_price}}</h4>
                        <form class="paypal" action="{{route('payment.submit')}}" method="post" id="payment_form_{{$feature}}">
                            {{csrf_field()}}

                            <input type="hidden" name="method" value="Paypal">
                            <input type="hidden" name="cmd" value="_xclick"/>
                            <input type="hidden" name="no_note" value="1"/>
                            <input type="hidden" name="lc" value="UK"/>
                            <input type="hidden" name="currency_code" value="EUR"/>
                            <input type="hidden" name="bn" value="PP-BuyNowBF:btn_buynow_LG.gif:NonHostedGuest"/>
                            <input type="hidden" name="userid" value="{{$user->id}}"/>
                            <input type="hidden" name="custom" value="{{$feature}}"/>

                            <div class="form-group col-md-12">
                                <input type="submit" name="submit" class="btn btn-success" value="Plateste in siguranta"/><br/>
                            </div>
                            <img src="https://www.paypalobjects.com/webstatic/mktg/logo/AM_mc_vs_dc_ae.jpg" alt="Paypal"/>
                            <div class="form-group col-md-12">
                                <p class="small-label" style="font-weight: 900;">*Nota: Pentru plata folosim sistemul international PayPal. Este cel mai sigur sistem de tranzactii online.<br/>Daca nu aveti cont de PayPal, folositi butonul <font color="red">"Checkout as a guest"</font> folosind ulterior cardul dumneavoastra pentru efectuarea platii in siguranta.</p>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Inchide fereastra</button>
            </div>
        </div><!-- End Modal content-->
    </div>
</div><!-- End Modal -->