<div id="page-wrapper" @if($mobile) style="margin-top:110px;" @endif>
    <div @if($mobile) style="padding:0px;" @endif>
        <div class="container" id="main">
            <div class="col-md-12 text-center">
                <div class="main-content">
                    <p class="head">Completeaza-ti adresa pentru a fi vazut pe harta din prima
                        pagina alaturi de ceilalti Makeup-Artisti.<br/>Apasa pe butonul <b>Editeaza-ti profilul</b> de
                        mai jos. Succes.</p>
                    <div class="col-md-12 text-center">
                        <a href="{{route('user.profile.edit')}}" class="edit-profile f-button">Editeaza-ti profilul</a>
                        @if(count($gallerydata) < 20)
                            <a href="{!! url('/user/add_image') !!}" class="add-image f-button"
                               style="padding: 10px 15px;">Adauga fotografii</a>
                        @else
                            <a href="{!! url('/user/gallery', ['type'=>'image']) !!}" class="add-image f-button"
                               style="float:right; width:auto; padding: 10px 15px;margin-left:15px;">Editeaza fotografii</a>
                        @endif
                        @if(count($videodata) < 1)
                            <a href="{!! url('/user/add_video') !!}" class="add-image f-button">Adauga video</a>
                        @else
                            <a href="{!! url('/user/gallery', ['type'=>'video']) !!}" class="add-image f-button">Editeaza video</a>
                        @endif
                        @if(!$mobile)
                            <a href="javascript:" data-toggle="modal" data-target="#ModalFeatured"
                               class="edit-profile f-button">Promoveaza-ti profilul</a>
                            <a href="/premium" class="edit-profile f-button">Vrei banner cu tine pe site?</a>
                        @else
                            @if($user->featured != 1)
                                <a href="javascript:" data-toggle="modal" data-target="#ModalFeatured"
                                   class="edit-profile f-button">Promoveaza-ti profilul
                                </a>
                            @endif
                            <a href="/premium" class="edit-profile f-button">Vrei banner cu tine pe site?</a>
                        @endif
                        <a class="view-profile f-button"
                           href="{!! url('/profile/'. Auth::user()->id .'/'. Auth::user()->name_slug) !!}"
                           target="_blank">Vezi profilul tau</a>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
    </div>
</div>
<style>
    #page-wrapper {
        /*max-width: 900px;*/
        /*margin: 0 auto;*/
    }

    .main-content {
        display: block;
        margin: auto;
    }

    .main-content > .head {
        color: #000000;
        font-size: 14px;
        padding: 5px;
        font-style: italic;
    }

    .main-content .flex-row {
        width: 100%;
        display: flex;
        min-height: 30vh;
        justify-content: center;
    }

    .main-content .f-button {
        width: 100%;
        font-weight: 600;
        height: 40px;
        border-radius: 0;
        box-shadow: none;
        color: #000;
        border: 1px solid #000000;
        font-family: 'Quicksand', sans-serif !important;
        margin-top: 20px;
        margin-right: 10px;
        background: transparent;
        box-shadow: none;
    }

    .main-content .f-button:hover {
        transform: scale(1.01);
        cursor: pointer;
    }
</style>