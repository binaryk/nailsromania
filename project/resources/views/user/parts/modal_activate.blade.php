<!-- Modal -->
<div class="modal fade-scale" id="ModalAll" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Activeaza-ti Profilul</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12 text-center">
                        <h4 id="cost" class="cost"></h4>
                        <h4>Selecteaza perioada de abonament</h4>
                        <form class="paypal" action="{{route('payment.submit')}}" method="post" id="payment_form_active">
                            {{csrf_field()}}
                            <input type="hidden" name="cmd" value="_xclick"/>
                            <input type="hidden" name="no_note" value="1"/>
                            <input type="hidden" name="lc" value="UK"/>
                            <input type="hidden" name="currency_code" value="EUR"/>
                            <input type="hidden" name="method" value="Paypal">
                            <input type="hidden" name="bn" value="PP-BuyNowBF:btn_buynow_LG.gif:NonHostedGuest"/>
                            <input type="hidden" name="userid" value="{{$user->id}}"/>

                            <div class="form-group col-md-8 col-md-offset-2">
                                <select name="custom" class="form-control" id="opt" required>
                                    <option value="">Selecteaza tip abonament</option>
                                    <option value="monthly">Abonament lunar(1Euro)</option>
                                    <option value="anual">Abonament anual(10Euro)</option>
                                </select>
                            </div>
                            <div class="form-group col-md-12">
                                <input type="submit" name="submit" id="pay" class="btn btn-success" value="Plateste acum in siguranta"/>
                            </div>
                            <div class="form-group col-md-12">
                                <p class="small-label" style="font-weight: 900;">*Nota: Pentru plata folosim sistemul international PayPal. Este cel mai sigur sistem ce nu percepe comision de tranzactie.<br/>Daca nu aveti cont de PayPal, folositi butonul "Checkout as a guest" folosind ulterior cardul dumneavoastra pentru efectuarea platii in siguranta.</p>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Inchide</button>
            </div>
        </div>

    </div>
</div>