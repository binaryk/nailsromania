<!-- Page Heading -->
<div class="go-title">
    <div class="pull-right">
        <a href="{!! url('/user/dashboard') !!}" class="btn btn-default btn-back"><i class="fa fa-arrow-left"></i> Inapoi la pagina de profil</a>
    </div>
    <h3>Editeaza profilul</h3>
    <div class="go-line"></div>
</div>
<!-- Page Content -->
<div class="panel panel-default">
    <div class="panel-body">
        <div id="response">
            @if(Session::has('message'))
                <div class="alert alert-success alert-dismissable">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                    {{ Session::get('message') }}
                </div>
            @endif
        </div>
        <form method="POST" action="{{ action('UserProfileController@update', ['id' => $user->id]) }}" class="form-horizontal form-label-left" enctype="multipart/form-data">
            {{csrf_field()}}
            <div class="item form-group">
                <label class="control-label col-md-2 col-sm-2 col-xs-12" for="name">Categoria de Unghii<span class="required">*</span>
                </label>
                <div class="col-md-10 col-sm-10 col-xs-12">
                    <select name="category" class="form-control" required>
                        <option value="">Selecteaza Categoria de Unghii</option>
                        @foreach($categories as $category)
                            @if($category->name == $user->category)
                                <option value="{{$category->name}}" selected>{{$category->name}}</option>
                            @else
                                <option value="{{$category->name}}">{{$category->name}}</option>
                            @endif
                        @endforeach
                    </select>
                </div>
            </div>

            <div class="item form-group">
                <label class="control-label col-md-2 col-sm-2 col-xs-12" for="slug">Introdu adresa aici<span class="required">*</span>
                </label>
                <div class="col-md-10 col-sm-10 col-xs-12">
                    <input class="form-control col-md-7 col-xs-12" value="{{$user->address}}" id="address_input" name="address" placeholder="Adresa" required="required" type="text">
                    <input value="{{$user->city}}" id="locality_input" name="city" type="hidden">
                    <input type="hidden" name="latitude" id="lat_input" value="{{$user->latitude}}"/>
                    <input type="hidden" name="longitude" id="lng_input" value="{{$user->longitude}}"/>
                </div>
            </div>


            <div class="ln_solid"></div>
            <div class="form-group">
                <div class="col-md-6 col-md-offset-3">
                    <button id="add_ads" type="submit" class="btn btn-success btn-block">Actualizeaza profil</button>
                </div>
            </div>
        </form>
    </div>
</div>