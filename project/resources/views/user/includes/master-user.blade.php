<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, shrink-to-fit=no, initial-scale=1">
    <meta name="description" content="GeniusOcean Admin Panel.">
    <meta name="author" content="">
    <meta name="_token" content="{!! csrf_token() !!}"/>
    <link rel="icon" type="image/png" href="{{url('/')}}/assets/images/{{$settings[0]->favicon}}"/>
    <link href="//fonts.googleapis.com/css?family=Quicksand:300,400,500,700&amp;subset=latin-ext" rel="stylesheet">
    <title>{{$settings[0]->title}} - Admin Panel</title>

    <!-- Bootstrap Core CSS -->
    <link href="{{ URL::asset('assets/css/bootstrap.min.css')}}" rel="stylesheet">
    <link href="{{ URL::asset('assets/css/font-awesome.min.css')}}" rel="stylesheet">
    <link href="{{ URL::asset('assets/css/dataTables.bootstrap.min.css')}}" rel="stylesheet">
    <link href="{{ URL::asset('assets/css/bootstrap-toggle.min.css')}}" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="{{ URL::asset('assets/css/genius-admin.css')}}" rel="stylesheet">
    {{--<link rel="stylesheet" href="{{ URL::asset('assets/css/genius1.css')}}" type="text/css">--}}
    <link href="{{ URL::asset('assets/css/jquery.fileupload.css')}}" rel="stylesheet">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="//oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="//oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    @yield('header')
</head>
<body>
<div><!-- Navigation -->
    <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            @if(!$mobile)
                <a class="navbar-brand" href="{!! route('user.dashboard') !!}">
                    <img class="logo" src="{!! url('assets/images/logo') !!}/{{$settings[0]->logo}}" alt="LOGO">
                </a>
            @else
                <div class="animbrand">
                    <a class="navbar-brand" href="{{url('/')}}">NAILS Romania</a>
                </div>
            @endif
        </div>
        <!-- Top Menu Items -->
        <ul class="nav navbar-right top-nav">
            @if(!$mobile)
                <li><a href="{!! url('/') !!}"><b class="fa fa-home" target="_blank"></b> Mergi pe site</a></li>
            @endif
            <li><a href="{!! url('/profile/'. Auth::user()->id .'/'. Auth::user()->name) !!}" target="_blank"><b class="fa fa-user"></b> Vezi profilul tau</a></li>
            <li class="dropdown">
                <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown">{{ Auth::user()->name }} <b class="fa fa-angle-down"></b></a>
                <ul class="dropdown-menu">
                    <li><a href="{!! route('user.profile.edit') !!}"><i class="fa fa-fw fa-user"></i> Editeaza Profilul</a></li>
                    <li><a href="{!! route('user.changepassword') !!}"><i class="fa fa-fw fa-cog"></i> Schimba Parola</a></li>
                    <li class="divider"></li>
                    <li>
                        <a href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                            <i class="fa fa-fw fa-power-off"></i> Logout
                        </a>

                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                            {{ csrf_field() }}
                        </form>
                    </li>
                </ul>
            </li>
        </ul>

    </nav>

    @yield('content')

</div><!-- /#wrapper -->
<!-- /#wrapper -->
<script>
    var baseUrl = '{!! url('/') !!}';
</script>
<!-- jQuery -->
<script src="{{ URL::asset('assets/js/jquery.js')}}"></script>
<script src="{{ URL::asset('assets/js/jquery.smooth-scroll.js')}}"></script>
<!-- Bootstrap Core JavaScript -->
<script src="{{ URL::asset('assets/js/bootstrap.min.js')}}"></script>
<script src="{{ URL::asset('assets/js/jquery.dataTables.min.js')}}"></script>
<script src="{{ URL::asset('assets/js/dataTables.bootstrap.min.js')}}"></script>
<script type="text/javascript" src="//maps.google.com/maps/api/js?libraries=places&language=ro&key={{GMAPS_KEY}}"></script>
<!-- Switchery -->
<script src="{{ URL::asset('assets/js/bootstrap-toggle.min.js')}}"></script>
<script type="text/javascript" src="{{ URL::asset('assets/js/plugin/nicEdit.js')}}"></script>
<script type="text/javascript" src="{{ URL::asset('assets/js/admin-genius.js')}}"></script>
<script type="text/javascript" src="{{ URL::asset('assets/js/jquery.ui.widget.js')}}"></script>
<script type="text/javascript" src="{{ URL::asset('assets/js/jquery.iframe-transport.js')}}"></script>
<script type="text/javascript" src="{{ URL::asset('assets/js/jquery.fileupload.js')}}"></script>

@yield('footer')
<script type="text/javascript">$.ajaxSetup({headers: {'X-CSRF-Token': $('meta[name=_token]').attr('content')}});</script>
</body>
</html>

