@extends('user.includes.master-user')

@section('header')
    <script type="text/javascript">
        @if (Request::has('new_user'))
        fbq('track', 'CompleteRegistration');
        @endif
    </script>
    <!-- Facebook Pixel Code -->
@stop

@section('content')
    @if(!$mobile)
        @include('parts.header', ['background'=>'background_login.jpg'])
    @endif

   @include('user.parts.profile-buttons')
    <!-- /#page-wrapper -->

    @if($user->status != 1)
        @include('user.parts.modal_activate')
    @endif

	<?php
	$item_amount = $settings[0]->normal_price;

	if ($user->status == '1') {
		$item_amount = 0.00;
	}

	if ($user->featured != 1) {
	$item_amount = $item_amount + $settings[0]->featured_price;
	$feature_price_text = 'Cost abonament: ';
	$feature_price_text .= ($user->status != 1) ? $settings[0]->normal_price . ' + ' . $settings[0]->featured_price : $settings[0]->featured_price;
	$feature_price_text .= ' EUR';
	?>

    @include('user.parts.modal', [$modal_id = 'ModalFeatured', $modal_title = 'Activeaza profil Premium', $modal_subject = 'Vei fi afisat pe prima pagina la sectiunea Nail Artisti PREMIUM.<br />Vei avea un simbol distinct fata de ceilalti Nail Artisti pe harta din prima pagina.<br />Vei putea sa iti urci un material video pe pagina de profil, pe langa portofoliul foto.', $feature_price = $feature_price_text, $feature = 'featured'])

	<?php } ?>
@stop

@section('footer')
    <script type="text/javascript">
        $("#opt").change(function () {
            var opt = $("#opt").val();
            if (opt == "anual") {
                $("#cost").html("Cost abonament: {{$settings[0]->anual_price}} EUR");
            } else {
                $("#cost").html("Cost abonament: {{$settings[0]->monthly_price}} EUR");
            }
        });
    </script>


<script>
  !function(f,b,e,v,n,t,s)
  {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
  n.callMethod.apply(n,arguments):n.queue.push(arguments)};
  if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
  n.queue=[];t=b.createElement(e);t.async=!0;
  t.src=v;s=b.getElementsByTagName(e)[0];
  s.parentNode.insertBefore(t,s)}(window, document,'script',
  'https://connect.facebook.net/en_US/fbevents.js');
  fbq('init', '148015875838317');
  fbq('track', 'PageView');
  fbq('track', 'CompleteRegistration');
</script>
<noscript><img height="1" width="1" style="display:none"
  src="https://www.facebook.com/tr?id=148015875838317&ev=PageView&noscript=1"
/></noscript>
<!-- End Facebook Pixel Code -->
<script>
  fbq('track', 'CompleteRegistration');
</script>
<!-- end olark code -->
@stop