<footer>
    <div class="go-top">
        <a id="gtop" href="javascript:;"><i class="fa fa-angle-up"></i></a>
    </div>

    <div class="row">
        <div class="col-md-2">&nbsp;</div>

        <div class="col-md-3 about">
            <h4>Despre Noi</h4>
            <p>{!!$settings[0]->about!!}</p>
        </div>

        <div class="col-md-2">&nbsp;</div>

        <div class="col-md-3 address">
            <h4>Adresa</h4>
            <p>Street Adresa: {{$settings[0]->address}}</p>
            @if($settings[0]->phone != NULL)
                <p>Phone: {{$settings[0]->phone}}</p>
            @endif

            @if($settings[0]->fax != NULL)
                <p>Fax: {{$settings[0]->fax}}</p>
            @endif
            <p>Email: {{$settings[0]->email}}</p>
        </div>
    </div>

    <div class="c-line"></div>
    <div class="text-center">
        {!! $settings[0]->footer !!}
    </div>
</footer>