<section class="azl-map-wrapper all">
    @if(! $mobile)
        <input id="azl-map" type="checkbox" style="position: absolute; clip: rect(0, 0, 0, 0);">
        <div class="controls">
            <div class="toggle">
                <label for="azl-map"></label>
            </div>
            <div class="locate"></div>
            <div class="zoom-in"></div>
            <div class="zoom-out"></div>
        </div>
    @endif
    <div class="azl-map" data-query='{"post_type":"product", "nopaging":true, "posts_per_page":-1}'
         style="position: relative; overflow: hidden;"></div>

    <div class="row" style="padding: 10px;">
        <form action="{{action('FrontEndController@search')}}" method="post">
            {{csrf_field()}}
            <div class="form-group" style="padding-left: 15px;">
                <div class="col-md-4 opt-form">
                    <input id="color" name="city" class="form-control large-height" placeholder="Alege un oras"
                           list="cities" autocomplete="off">
                    <datalist id="cities">
                        @foreach($cities as $city)
                            <option value="{{$city->city}}">
                        @endforeach
                    </datalist>
                </div>
                <div class="col-md-8 opt-form">
                    <input type="text" placeholder="Alege o categorie" list="categories" name="category"
                           class="datalist form-control large-height" autocomplete="off">
                    <datalist id="categories">
                        @foreach($categories as $category)
                            <option value="{{$category->name}}">
                        @endforeach
                    </datalist>
                </div>
                @if(!$mobile && false)
                    <div class="col-md-2 opt-form">
                        <label>
                            <input type="checkbox" name="starred" class="form-control">
                            <div>Doar 5 stele</div>
                        </label>
                    </div>
                @endif
                <div class="submit" style="padding-right:15px;">
                    <input class="btn btn-ocean btn-block large-height" type="submit" value="Cauta"/>
                </div>
            </div>
        </form>
    </div>
</section>
@section('header')
    @parent

    <style>
        @media (max-width: 768px) {
            .single {
                height: 220px;
            }
            .container-fluid>.navbar-collapse, .container-fluid>.navbar-header, .container>.navbar-collapse, .container>.navbar-header {
                margin: 0;
            }
            #map-nav, #map-nav > .container {
                padding: 0;
            }
            #map-navigation {
                padding: 0;
                margin: 0 auto;
            }

            .azl-map-wrapper .azl-map {
                height: 333px;
            }
        }
    </style>

@stop
