<!DOCTYPE html>

<html lang="en">

<head>

    <!-- Required meta tags -->

    <meta charset="utf-8">

    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <title>Beauty Revolution Romania - Prima platforma Globala destinata exclusiv Beauty Artistilor Romani.</title>

    <!-- Meta Share -->

    <meta property="og:title" content="Beauty Revolution Romania"/>

    <meta property="og:type" content="website"/>

    <meta property="og:image" content="{{ URL::asset('assets/images/bg-one.jpg') }}"/>

    <!-- CSS Files -->

    <!-- build:css css/app.min.css -->

    <!-- Bootstrap CSS -->

    <link rel="stylesheet" href="{{ URL::asset('assets/css/bootstrap.min.css') }}">

    <!-- Web Fonts -->

    <link href="//fonts.googleapis.com/css?family=Playfair+Display:400,700|Dosis|Oswald:300" rel="stylesheet">

    <!-- Plugins -->

    <link rel="stylesheet" href="{{ URL::asset('assets/css/mobile/icon-font.css') }}">

    <link rel="stylesheet" href="{{ URL::asset('assets/css/mobile/aos.css') }}">

    <!-- Main CSS -->

    <link rel="stylesheet" href="{{ URL::asset('assets/css/mobile/style.css') }}">

    <!-- /build -->

</head>



<body class="light-theme bg-one body-padding">

<div class="animation anim-dark" data-aos="slide-effect-2">

    <div class="wrapper animation-inner">

        <div class="background bg-minimal">

        </div>

        <!-- // end .background -->

        <div class="content-section d-flex justify-content-center align-items-center">

            <div class="v-lines">

                <div class="vline-1"></div><!-- // end .vline-1 -->

                <div class="vline-2"></div><!-- // end .vline-1 -->

                <div class="vline-3"></div><!-- // end .vline-1 -->

                <div class="vline-4"></div><!-- // end .vline-1 -->

            </div><!-- // end .lines -->

            <div class="container">

                <div class="center-content align-self-center">

                    <h3 class="display-5 sans-serif-font font-bold" data-aos="fade" data-aos-delay="1800">

                        <img class="img-responsive" src="{{ URL::asset('assets/images/logo')}}/{{$settings[0]->logo}}" alt="">

                    </h3>
<center>Esti NAIL ARTIST?</center>
                    <div class="row mobile-menu">

                        <span class="animation anim-dark" data-aos="slide-effect-btn" data-aos-delay="3500">

                            <a href="http://nailsromania.ro/user/login" class="btn btn-lg btn-outline btn-block" data-toggle="modal" data-target="http://nailsromania.ro">LOGARE</a>

                            <a href="http://nailsromania.ro/user/registration" class="btn btn-lg btn-outline btn-block" data-toggle="modal" data-target="http://nailsromania.ro"> INREGISTRARE</a>

                            <a href="{{route('user.forgotpass')}}" class="btn btn-lg btn-outline btn-block" data-toggle="modal" data-target="http://nailsromania.ro">AI UITAT PAROLA?</a>

                            <a href="{{url('/')}}" class="btn btn-lg btn-outline btn-block" data-toggle="modal" data-target="http://nailsromania.ro">SUNT CLIENT</a>

                        </span>

                        <br>

                        <a href="http://nailsromania.ro/termenisiconditii.pdf">Termeni si conditii</a>

                    </div>

                </div>

                <!-- // end .center-content -->

            </div>

            <!-- // end .container -->

        </div>

        <!-- jQuery first, then Tether, then Bootstrap JS. -->

        <script src="{{ URL::asset('assets/js/mobile/jquery-3.2.1.min.js') }}"></script>

        <script src="{{ URL::asset('assets/js/mobile/tether.min.js') }}"></script>

        <!-- Bootstrap JS -->

        <script src="{{ URL::asset('assets/js/mobile/bootstrap.min.js') }}"></script>

        <!-- Plugins JS -->

        <script src="{{ URL::asset('assets/js/mobile/viewport.min.js') }}"></script>

        <script src="{{ URL::asset('assets/js/mobile/aos.js') }}"></script>

        <script src="{{ URL::asset('assets/js/mobile/countdown.js') }}"></script>

        <script src="{{ URL::asset('assets/js/mobile/smoothscroll.min.js') }}"></script>



        <!-- Main JS -->

        <script src="{{ URL::asset('assets/js/mobile/script.js') }}"></script>

        <!-- /build -->

</body>



</html>