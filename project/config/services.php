<?php

return [
	'mailgun' => [
		'domain' => env('MAILGUN_DOMAIN'),
		'secret' => env('MAILGUN_SECRET'),
	],

	'ses' => [
		'key'    => env('SES_KEY'),
		'secret' => env('SES_SECRET'),
		'region' => 'us-east-1',
	],

	'sparkpost' => [
		'secret' => env('SPARKPOST_SECRET'),
	],

	'facebook' => [
		'client_id'     => env('FB_ID'),
		'client_secret' => env('FB_SECRET'),
		'redirect'      => env('APP_URL') . '/oauth/facebook/callback',
	],
];
