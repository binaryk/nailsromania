<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/', 'FrontEndController@index');
Route::get('/launch', 'FrontEndController@welcome');
Route::get('/mobile', 'FrontEndController@mobile')->name('mobile.page');
Route::post('/map_query', 'FrontEndController@map_query');
Route::get('/about', 'FrontEndController@about');
Route::get('/faq', 'FrontEndController@faq');
Route::get('/contact', 'FrontEndController@contact');
Route::get('/listall', 'FrontEndController@all');
Route::get('/listfeatured', 'FrontEndController@featured');
Route::get('/members', 'FrontEndController@members');
Route::get('/liststarred', 'FrontEndController@starred');
Route::get('/category/{category}', 'FrontEndController@category');
Route::post('/search', 'FrontEndController@search');
Route::post('/subscribe', 'FrontEndController@subscribe');
Route::post('/profile/email', 'FrontEndController@usermail');
Route::get('/profile/{id}/{name_slug}', 'FrontEndController@viewprofile')->name('user.profile');
Route::post('/contact/email', 'FrontEndController@contactmail');
Route::get('/totals', 'FrontEndController@totals');

Route::get('auth/social', 'Auth\SocialAuthController@show')->name('social.login');
Route::get('oauth/{driver}', 'Auth\SocialAuthController@redirectToProvider')->name('social.oauth');
Route::get('oauth/{driver}/member', 'Auth\SocialAuthController@redirectToProviderMember')->name('social.oauth.member');
Route::get('oauth/{driver}/callback', 'Auth\SocialAuthController@handleProviderCallback')->name('social.callback');
Route::get('oauth/{driver}/callback-member', 'Auth\SocialAuthController@handleProviderCallbackMember')->name('social.callback.member');

Route::get('/login',
	function () {
		return view('admin.login');
	}
);

Auth::routes();

Route::group(['prefix' => 'admin'],
	function () {
		Route::get('/',
			function () {
				return view('admin.index');
			}
		);

		Route::get('/dashboard', 'HomeController@index');
		Route::resource('/users', 'UsersController');
		Route::resource('/category', 'CategoryController');
		Route::resource('/social', 'SocialLinkController');
		Route::resource('/tools', 'SeoToolsController');
		Route::resource('/adminprofile', 'AdminProfileController');

		Route::group(['prefix' => 'settings'],
			function () {
				Route::resource('/', 'SettingsController');
				Route::post('/title', 'SettingsController@title');
				Route::post('/paymentinfo', 'SettingsController@paymentinfo');
				Route::post('/about', 'SettingsController@about');
				Route::post('/config', 'SettingsController@config');
				Route::post('/address', 'SettingsController@address');
				Route::post('/footer', 'SettingsController@footer');
				Route::post('/logo', 'SettingsController@logo');
				Route::post('/favicon', 'SettingsController@favicon');
				Route::post('/background', 'SettingsController@background');
			});

		Route::group(['prefix' => 'pagesettings'],
			function () {
				Route::resource('/', 'PageSettingsController');
				Route::post('/about', 'PageSettingsController@about');
				Route::post('/faq', 'PageSettingsController@faq');
				Route::post('/contact', 'PageSettingsController@contact');
			});

		Route::group(['prefix' => 'ads'],
			function () {
				Route::resource('/', 'AdvertiseController');
				Route::get('/status/{id}/{status}', 'AdvertiseController@status');
			});

		Route::group(['prefix' => 'subscribers'],
			function () {
				Route::resource('/', 'SubscriberController');
				Route::get('/download', 'SubscriberController@download');
			});

		Route::group(['prefix' => 'adminpassword'],
			function () {
				Route::get('/', 'AdminProfileController@password');
				Route::post('/change/{id}', 'AdminProfileController@changepass');
			});
	}
);

Route::group(['prefix' => 'user'],
	function () {
		Route::get('/dashboard', 'UserProfileController@index')->name('user.dashboard');
		Route::get('/member', 'UserProfileController@editMember')->name('user.profile.edit.member');
		Route::get('/edit', 'UserProfileController@edit')->name('user.profile.edit');
		Route::get('/gallery/{type}', 'UserProfileController@image_gallery')->name('image.gallery');
		Route::get('/video_gallery', 'UserProfileController@video_gallery')->name('video.gallery');
		Route::get('/changepassword', 'UserProfileController@changePassform')->name('user.changepassword');
		Route::post('/changepass/{id}', 'UserProfileController@changepass')->name('user.changepassword.submit');
		Route::post('/update/{id}', 'UserProfileController@update')->name('user.update');
		Route::get('/publish/{id}', 'UserProfileController@publish')->name('user.publish');
		Route::post('/review', 'FrontEndController@reviewsubmit')->name('review.submit');

		Route::get('/delete/{id}/{type}', 'GalleryController@getDelete');
		Route::get('/add_image', 'GalleryController@addImageForm');
		Route::get('/add_video', 'GalleryController@addVideoForm');
		Route::get('/add_document', 'GalleryController@addDocForm');
		Route::post('/upload/{type}', 'GalleryController@postUpload');

		Route::get('/login', 'Auth\ProfileLoginController@showLoginFrom')->name('user.login');
		Route::post('/login', 'Auth\ProfileLoginController@login')->name('user.login.submit');

        Route::get('/step-0', 'Auth\ProfileRegistrationController@step0')->name('user.step0');
        Route::get('/step-1', 'Auth\ProfileRegistrationController@step1')->name('user.step1');
		Route::get('/registration', 'Auth\ProfileRegistrationController@showStep1')->name('user.registration');
		Route::post('/registration', 'Auth\ProfileRegistrationController@register')->name('user.registration.submit');

		Route::get('/forgot', 'Auth\ProfileResetPassController@showForgotForm')->name('user.forgotpass');
		Route::post('/forgot', 'Auth\ProfileResetPassController@resetPass')->name('user.forgotpass.submit');

		Route::group(['prefix' => 'payment'],
			function () {
				Route::post('/notify', 'PaymentController@notify')->name('payment.notify');

				Route::group(['middleware' => 'auth:profile'],
					function () {
						Route::post('/', 'PaymentController@store')->name('payment.submit');
						Route::get('/cancle', 'PaymentController@paycancle')->name('payment.cancle');
						Route::get('/return', 'PaymentController@payreturn')->name('payment.return');
					});
			});
	}
);

